package by.epam.newsmanagmentapp.service.impl;

import by.epam.newsmanagmentapp.domain.bean.Role;
import by.epam.newsmanagmentapp.domain.bean.User;
import by.epam.newsmanagmentapp.domain.to.UserTO;
import by.epam.newsmanagmentapp.exceptions.ServiceException;
import by.epam.newsmanagmentapp.service.AuthenticationService;
import by.epam.newsmanagmentapp.service.RoleService;
import by.epam.newsmanagmentapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class AuthenticationServiceImpl implements AuthenticationService {

  @Autowired
  private UserService userService;

  @Autowired
  private RoleService roleService;

  @Override
  public UserTO getUserInfo(String userName) throws ServiceException {
    UserTO userTO = new UserTO();
    User user = userService.getUserInfo(userName);
    Role role = roleService.getByKey(user.getRoleId());
    userTO.setUser(user);
    userTO.setRole(role);
    return userTO;
  }
}
