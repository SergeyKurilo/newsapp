<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="${pageContext.request.contextPath}/allnews?page=1">News App</a>
    </div>
    <p class="navbar-text navbar-right">
      <a href="${pageContext.request.contextPath}/logout">Sign out</a>
    </p>
  </div>
</nav>