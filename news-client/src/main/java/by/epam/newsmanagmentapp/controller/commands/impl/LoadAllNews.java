package by.epam.newsmanagmentapp.controller.commands.impl;

import by.epam.newsmanagmentapp.controller.commands.CommandException;
import by.epam.newsmanagmentapp.controller.commands.ICommand;
import by.epam.newsmanagmentapp.domain.bean.Author;
import by.epam.newsmanagmentapp.domain.bean.Tag;
import by.epam.newsmanagmentapp.domain.to.NewsTO;
import by.epam.newsmanagmentapp.exceptions.ServiceException;
import by.epam.newsmanagmentapp.service.AuthorService;
import by.epam.newsmanagmentapp.service.ComplexNewsService;
import by.epam.newsmanagmentapp.service.NewsService;
import by.epam.newsmanagmentapp.service.TagService;
import by.epam.newsmanagmentapp.utils.Action;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Component
public class LoadAllNews implements ICommand {

  @Autowired
  private AuthorService authorService;

  @Autowired
  private TagService tagService;

  @Autowired
  private ComplexNewsService complexNewsService;

  @Autowired
  private NewsService newsService;

  private static final String PAGE_NUMBER_VARIABLE = "page";
  private static final String TAGS_LIST = "tagsList";
  private static final String NEWS_TO = "newsTOs";
  private static final String NUMBER_OF_PAGES = "numberOfPages";
  private static final String AUTHORS_LIST = "authorsList";
  private static final String ACTION_PARAM = "action";
  private static final String SEARCH_CRITERIA = "searchCriteria";
  private static final String CURRENT_PAGE = "currentPage";


  @Override
  public String execute(HttpServletRequest request) throws CommandException {
    String PATH = "/allNews";
    List<Tag> tags;
    List<Author> authors;
    List<NewsTO> newsTOs;
    Long numberOfPages;
    HttpSession session = request.getSession();
    Long numberOfPage = 1L;

    if (request.getParameter(PAGE_NUMBER_VARIABLE) != null) {
      numberOfPage = Long.valueOf(request.getParameter(PAGE_NUMBER_VARIABLE));
    }

    if (session.getAttribute(SEARCH_CRITERIA) != null) {
      PATH = "/ClientController?command=searchResult&page=" + numberOfPage;
      request.setAttribute(ACTION_PARAM, Action.REDIRECT);
      return PATH;
    }

    try {
      tags = tagService.findAll();
      authors = authorService.getNotExpiredAuthors();
      newsTOs = complexNewsService.loadPage(numberOfPage);
      numberOfPages = newsService.numberOfPages();
    } catch (ServiceException e) {
      throw new CommandException(e);
    }

    session.setAttribute(CURRENT_PAGE, numberOfPage);
    fillInRequest(request, newsTOs, authors, tags, numberOfPages);
    return PATH;
  }

  private void fillInRequest(HttpServletRequest request, List<NewsTO> news, List<Author> authors,
                             List<Tag> tags, Long numberOfPages) {
    request.setAttribute(NEWS_TO, news);
    request.setAttribute(AUTHORS_LIST, authors);
    request.setAttribute(TAGS_LIST, tags);
    request.setAttribute(NUMBER_OF_PAGES, numberOfPages);
    request.setAttribute(ACTION_PARAM, Action.FORWARD);
  }
}
