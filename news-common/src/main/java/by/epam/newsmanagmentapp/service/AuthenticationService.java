package by.epam.newsmanagmentapp.service;

import by.epam.newsmanagmentapp.domain.to.UserTO;
import by.epam.newsmanagmentapp.exceptions.ServiceException;

public interface AuthenticationService {
  UserTO getUserInfo(String userName) throws ServiceException;
}
