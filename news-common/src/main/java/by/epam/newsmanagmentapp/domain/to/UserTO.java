package by.epam.newsmanagmentapp.domain.to;

import by.epam.newsmanagmentapp.domain.bean.Role;
import by.epam.newsmanagmentapp.domain.bean.User;

/**
 * Class to carry multiple data elements. Contains ${@link User} object and ${@link Role} object related to it.
 */
public class UserTO {
    private User user;
    private Role role;

    public UserTO() {
    }

    public UserTO(User user, Role role) {
        this.user = user;
        this.role = role;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserTO userTO = (UserTO) o;

        if (user != null ? !user.equals(userTO.user) : userTO.user != null) return false;
        return role != null ? role.equals(userTO.role) : userTO.role == null;

    }

    @Override
    public int hashCode() {
        int result = user != null ? user.hashCode() : 0;
        result = 31 * result + (role != null ? role.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserTO{");
        sb.append("user=").append(user);
        sb.append(", role=").append(role);
        sb.append('}');
        return sb.toString();
    }
}
