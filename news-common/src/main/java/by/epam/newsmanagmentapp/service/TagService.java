package by.epam.newsmanagmentapp.service;

import by.epam.newsmanagmentapp.domain.bean.Tag;
import by.epam.newsmanagmentapp.exceptions.ServiceException;

import java.util.List;

/**
 * Provides specific operations which can be performed with {@link Tag} objects.
 */
public interface TagService extends GenericService<Tag> {
    /**
     * Loads {@link Tag} objects with specified news identifier.
     *
     * @param id identifier of news.
     * @return List of ${@link Tag} objects.
     * @throws ServiceException if ${@link by.epam.newsmanagmentapp.exceptions.DAOException} was thrown.
     */
    List<Tag> getTagsByNewsId(Long id) throws ServiceException;
}
