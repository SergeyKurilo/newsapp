package by.epam.newsmanagmentapp.dao;

import by.epam.newsmanagmentapp.domain.bean.Author;
import by.epam.newsmanagmentapp.exceptions.DAOException;

import java.util.List;

/**
 * Describes specific operations which can be performed with {@link Author} objects in the database.
 */
public interface AuthorDAO extends GenericDAO<Author> {

    /**
     * Checks whether there is the author with specified name passed as parameter.
     *
     * @param authorName name of checking author.
     * @return true if author with such name is exist.
     * @throws DAOException if there were problems during reading records from database.
     */
    boolean checkAuthor(String authorName) throws DAOException;

    /**
     * Loads ${@link Author} object appropriate record in database with author name specified as parameter.
     *
     * @param authorName name of author.
     * @return {@link Author} objects from database which matches the search conditions.
     * @throws DAOException          if there were problems during reading records from database.
     */
    Author getAuthorByName(String authorName) throws DAOException;

    /**
     * Loads not expired authors from database.
     *
     * @return list of non-expired authors from database.
     * @throws DAOException if there were errors while reading {@link Author} objects from the database.
     */
    List<Author> getNotExpiredAuthors() throws DAOException;

    /**
     * Loads {@link Author} from database by news identifier.
     *
     * @param newsId identifier of news.
     * @return {@link Author} object with required newsId.
     * @throws DAOException          if there were errors while reading {@link Author} objects from the database.
     */
    Author getAuthorByNewsId(Long newsId) throws DAOException;

}
