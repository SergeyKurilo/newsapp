package by.epam.newsmanagmentapp.dao.impl;

import by.epam.newsmanagmentapp.dao.CommentDAO;
import by.epam.newsmanagmentapp.domain.bean.Comment;
import by.epam.newsmanagmentapp.exceptions.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-application-context.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
@DatabaseSetup(value = "classpath:by/epam/newsmanagmentapp/dao/CommentDAOTest.xml", type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = "classpath:by/epam/newsmanagmentapp/dao/CommentDAOTest.xml", type = DatabaseOperation.DELETE_ALL)
public class CommentDAOTest {

    @Autowired
    private CommentDAO commentDAO;

    @BeforeClass
    public static void setLocale() {
        Locale.setDefault(Locale.ENGLISH);
    }

    @Test
    public void testFindAll() throws DAOException {
        final int size = 4;
        List<Comment> commentList = commentDAO.findAll();
        assertEquals(size, commentList.size());
    }


    @Test
    public void testInsert() throws DAOException {
        final String COMMENT_TEXT = "Comment";
        final String CREATION_DATE_TEXT = "2016-04-16 08:58:03.628";
        Comment comment = new Comment();
        comment.setCommentText(COMMENT_TEXT);
        comment.setNewsId(1L);
        comment.setCreationDate(Timestamp.valueOf(CREATION_DATE_TEXT));
        Long id = commentDAO.insert(comment);
        comment.setCommentId(id);
        Comment actualComment = commentDAO.getByKey(id);
        assertEquals(comment, actualComment);
    }


    @Test
    public void testGetCommentsByNewsId() throws DAOException {
        final Long newsId = 1L;
        final int size = 2;
        List<Comment> comments = commentDAO.getCommentsByNewsId(newsId);
        assertEquals(size, comments.size());
    }

    @Test
    public void testDelete() throws DAOException {
        final Long id = 1L;
        commentDAO.delete(id);
        Comment comment = commentDAO.getByKey(id);
        assertNull(comment);
    }

    @Test
    public void testGetByKey() throws DAOException {
        final Long id = 1L;
        Comment comment = commentDAO.getByKey(id);
        assertEquals(id, comment.getCommentId());
    }

    @Test
    public void testUpdate() throws DAOException {
        final String NEW_TEXT = "Changed Text";
        final Long ID = 1L;
        Comment comment = commentDAO.getByKey(ID);
        comment.setCommentText(NEW_TEXT);
        commentDAO.update(comment);
        Comment actual = commentDAO.getByKey(ID);
        assertEquals(comment, actual);
    }


    @Test
    public void testDeleteByNewsId() throws DAOException {
        final Long newsId = 1L;
        commentDAO.deleteByNewsId(newsId);
        Comment comment = commentDAO.getByKey(newsId);
        assertNull(comment);
    }

}
