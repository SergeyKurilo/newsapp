package by.epam.newsmanagmentapp.dao.util;

import by.epam.newsmanagmentapp.exceptions.DAOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Util class for closing resultSet, statement and returning connection into connection pool.
 */
public class DBUtil {

  private static final Logger LOGGER = LogManager.getLogger(DBUtil.class);

  /**
   * Closes statement, resultSet and released connection into pool.
   *
   * @param connection Connection object that need to be released.
   * @param dataSource Data source object used by program.
   * @param statement  PreparedStatement object that is need to be closed.
   * @param resultSet  ResultSet object that is need to be closed.
   * @throws SQLException if there were errors during closed resources or released connection.
   */
  public static void closeConnection(Connection connection, DataSource dataSource, PreparedStatement statement, ResultSet resultSet) throws DAOException {
    try {
      if (resultSet != null) {
        resultSet.close();
      }
      if (statement != null) {
        statement.close();
      }
      if (connection != null) {
        DataSourceUtils.releaseConnection(connection, dataSource);
      }
    } catch (SQLException e) {
      LOGGER.error("Exception during connection closing", e);
      throw new DAOException(e);
    }


  }

  /**
   * Closes statement and released connection into pool.
   *
   * @param connection Connection object that need to be released.
   * @param dataSource Data source object used by program.
   * @param statement  PreparedStatement object that is need to be closed.
   * @throws SQLException if there were errors during closed resources or released connection.
   */
  public static void closeConnection(Connection connection, DataSource dataSource, PreparedStatement statement) throws DAOException {
    try {
      if (statement != null) {
        statement.close();
      }
      if (connection != null) {
        DataSourceUtils.releaseConnection(connection, dataSource);
      }
    } catch (SQLException e) {
      LOGGER.error("Exception during connection closing", e);
      throw new DAOException(e);
    }


  }
}
