package by.epam.newsmanagmentapp.service.impl;

import by.epam.newsmanagmentapp.dao.TagDAO;
import by.epam.newsmanagmentapp.domain.bean.Tag;
import by.epam.newsmanagmentapp.exceptions.DAOException;
import by.epam.newsmanagmentapp.exceptions.ServiceException;
import by.epam.newsmanagmentapp.service.TagService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Implementation of {@link TagService}.
 */
@Component
@Transactional
public class TagServiceImpl implements TagService {

    private static final Logger LOGGER = LogManager.getLogger(TagServiceImpl.class);

    @Autowired
    private TagDAO tagDAO;

    /**
     * Implementation of {@link TagService#getTagsByNewsId(Long)} method.
     */
    @Override
    public List<Tag> getTagsByNewsId(Long id) throws ServiceException {
        try {
            return tagDAO.getTagsByNewsId(id);
        } catch (DAOException e) {
            LOGGER.error("Load tags by news ID service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link TagService#findAll()} method.
     */
    @Override
    public List<Tag> findAll() throws ServiceException {
        try {
            return tagDAO.findAll();
        } catch (DAOException e) {
            LOGGER.error("Load all tags service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link TagService#insert(Object)} method.
     */
    @Override
    public Long insert(Tag tag) throws ServiceException {
        try {
            return tagDAO.insert(tag);
        } catch (DAOException e) {
            LOGGER.error("Create tag node service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link TagService#delete(Long)} method.
     */
    @Override
    public void delete(Long id) throws ServiceException {
        try {
            tagDAO.delete(id);
        } catch (DAOException e) {
            LOGGER.error("Removing tag service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link TagService#getByKey(Long)} method.
     */
    @Override
    public Tag getByKey(Long id) throws ServiceException {
        try {
            return tagDAO.getByKey(id);
        } catch (DAOException e) {
            LOGGER.error("Find tag node by ID service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link TagService#update(Object)} method.
     */
    @Override
    public void update(Tag tag) throws ServiceException {
        try {
            tagDAO.update(tag);
        } catch (DAOException e) {
            LOGGER.error("Update tag node service error");
            throw new ServiceException(e);
        }
    }
}

