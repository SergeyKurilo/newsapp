<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="col-sm-10">
  <div class="grid news_desc">
    <h3>${newsTO.news.title}
            <span class="glyphicons glyphicons-tags">
            <c:forEach items="${newsTO.tags}" var="tag">
              <c:out value="${tag.tagName}"/>
            </c:forEach>
            </span>
    </h3>
    <h4>Posted on <fmt:formatDate value="${newsTO.news.creationDate}" pattern="MM/dd/yyyy"/> by
      <span><a>${newsTO.author.authorName}</a></span></h4>
    <p>${newsTO.news.shortText}.</p>
    <p>${newsTO.news.fullText}.</p>
  </div>
  <div class="comments">
    <c:forEach items="${newsTO.comments}" var="commentVar">
      <div class="grid comments_desc">
        <h5><fmt:formatDate value="${commentVar.creationDate}" pattern="MM/dd/yyyy"/></h5>
        <form:form method="post" action="${pageContext.request.contextPath}/deletecomment">
          <input type="hidden" name="newsId" value="${commentVar.newsId}"/>
          <input type="hidden" name="commentId" value="${commentVar.commentId}"/>
          <input type="submit" value="Delete">
        </form:form>
        <p>${commentVar.commentText}</p>
      </div>
    </c:forEach>

    <div class="grid comments_desc">

      <form:form method="POST" modelAttribute="comment" action="${pageContext.request.contextPath}/addcomment">
        <form:textarea path="commentText" rows="5" cols="70"/>
        <c:forEach items="${errors}" var="error">
          ${error}
        </c:forEach>
        <form:input path="newsId" type="hidden" value="${newsTO.news.newsId}"/>
        <input type="submit" value="Comment">
      </form:form>

    </div>
  </div>
  <div class="pagination-custom">
    <ul class="pager">
      <c:if test="${previousId != 0}">
        <li class="previous"><a href="${pageContext.request.contextPath}/news/${previousId}">&larr; Previous</a></li>
      </c:if>
      <c:if test="${nextId != 0}">
        <li class="next"><a href="${pageContext.request.contextPath}/news/${nextId}">Next &rarr;</a></li>
      </c:if>
    </ul>
  </div>
</div>