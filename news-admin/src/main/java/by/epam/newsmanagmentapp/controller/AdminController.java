package by.epam.newsmanagmentapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AdminController {
  private static final String LOGIN_MAPPING = "/login";
  private static final String ROOT_MAPPING = "/";
  private static final String LOGIN_PAGE = "login";

  @RequestMapping({ROOT_MAPPING, LOGIN_MAPPING})
  public String getLoginPage() {
    return LOGIN_PAGE;
  }

}
