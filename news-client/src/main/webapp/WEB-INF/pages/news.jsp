<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>News</title>

  <c:url value="/resources/styles/bootstrap.css" var="bootstrapCss"/>
  <link href="${bootstrapCss}" rel="stylesheet"/>
  <c:url value="/resources/styles/app.css" var="customCss"/>
  <link href="${customCss}" rel="stylesheet"/>
  <script src="<c:url value="/resources/scripts/jquery.js"/>"></script>


</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="${pageContext.request.contextPath}/ClientController?page=1">News App</a>
    </div>
  </div>
</nav>

<div class="col-sm-12">
  <div class="grid news_desc">
    <h3>${newsTO.news.title}
            <span class="glyphicons glyphicons-tags">
            <c:forEach items="${newsTO.tags}" var="tag">
              <c:out value="${tag.tagName}"/>
            </c:forEach>
            </span>
    </h3>
    <h4>Posted on <fmt:formatDate value="${newsTO.news.creationDate}" pattern="MM/dd/yyyy"/> by
      <span><a>${newsTO.author.authorName}</a></span></h4>
    <p>${newsTO.news.shortText}.</p>
    <p>${newsTO.news.fullText}.</p>
  </div>
  <div class="comments">
    <c:forEach items="${newsTO.comments}" var="commentVar">
      <div class="grid comments_desc">
        <h5><fmt:formatDate value="${commentVar.creationDate}" pattern="MM/dd/yyyy"/></h5>
        <p>${commentVar.commentText}</p>
      </div>
    </c:forEach>

    <div class="grid comments_desc">

      <form method="POST" modelAttribute="comment" action="${pageContext.request.contextPath}/ClientController">
        <textarea name="commentText" rows="5" cols="70"></textarea>
        <c:forEach items="${errors}" var="error">
          ${error}
        </c:forEach>
        <input name="command" type="hidden" value="addComment"/>
        <input name="newsId" type="hidden" value="${newsTO.news.newsId}"/>
        <input type="submit" value="Comment">
      </form>

    </div>
  </div>

  <div class="pagination-custom">
    <ul class="pager">
      <c:if test="${previousId != 0}">
        <li class="previous"><a
                href="${pageContext.request.contextPath}/ClientController?command=singleNews&id=${previousId}">&larr; Previous</a>
        </li>
      </c:if>
      <c:if test="${nextId != 0}">
        <li class="next"><a
                href="${pageContext.request.contextPath}/ClientController?command=singleNews&id=${nextId}">Next &rarr;</a></li>
      </c:if>
    </ul>
  </div>

</div>
</body>
