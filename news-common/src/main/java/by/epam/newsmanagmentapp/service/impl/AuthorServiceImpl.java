package by.epam.newsmanagmentapp.service.impl;

import by.epam.newsmanagmentapp.dao.AuthorDAO;
import by.epam.newsmanagmentapp.domain.bean.Author;
import by.epam.newsmanagmentapp.exceptions.DAOException;
import by.epam.newsmanagmentapp.exceptions.ServiceException;
import by.epam.newsmanagmentapp.service.AuthorService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Implementation of {@link AuthorService}.
 */
@Component
@Transactional
public class AuthorServiceImpl implements AuthorService {

    private static final Logger LOGGER = LogManager.getLogger(AuthorServiceImpl.class);

    @Autowired
    private AuthorDAO authorDAO;

    /**
     * Implementation of {@link AuthorService#getNotExpiredAuthors()} method.
     */
    @Override
    public List<Author> getNotExpiredAuthors() throws ServiceException {
        try {
            return authorDAO.getNotExpiredAuthors();
        } catch (DAOException e) {
            LOGGER.error("Getting not expired authors service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link AuthorService#getAuthorByNewsId(Long)} method.
     */
    @Override
    public Author getAuthorByNewsId(Long newsId) throws ServiceException {
        Author author = null;
        if (newsId != null) {
            try {
                author = authorDAO.getAuthorByNewsId(newsId);
            } catch (DAOException e) {
                LOGGER.error("Find author node by news ID service error");
                throw new ServiceException(e);
            }
        }
        return author;
    }

    /**
     * Implementation of {@link AuthorService#findAll()} method.
     */
    @Override
    public List<Author> findAll() throws ServiceException {
        try {
            return authorDAO.findAll();
        } catch (DAOException e) {
            LOGGER.error("Load all authors service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link AuthorService#insert(Object)} method.
     */
    @Override
    public Long insert(Author author) throws ServiceException {
        try {
            return authorDAO.insert(author);
        } catch (DAOException e) {
            LOGGER.error("Create author node service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link AuthorService#delete(Long)} method.
     */
    @Override
    public void delete(Long id) throws ServiceException {
        if (id != null) {
            try {
                authorDAO.delete(id);
            } catch (DAOException e) {
                LOGGER.error("Removing author service error");
                throw new ServiceException(e);
            }
        }
    }

    /**
     * Implementation of {@link AuthorService#getByKey(Long)} method.
     */
    @Override
    public Author getByKey(Long id) throws ServiceException {
        try {
            return authorDAO.getByKey(id);
        } catch (DAOException e) {
            LOGGER.error("Find author node by ID " + id + " service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link AuthorService#update(Object)} method.
     */
    @Override
    public void update(Author author) throws ServiceException {
        try {
            authorDAO.update(author);
        } catch (DAOException e) {
            LOGGER.error("Update author node service error");
            throw new ServiceException(e);
        }
    }
}

