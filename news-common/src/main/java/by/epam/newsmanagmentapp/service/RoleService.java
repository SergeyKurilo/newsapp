package by.epam.newsmanagmentapp.service;

import by.epam.newsmanagmentapp.domain.bean.Role;

/**
 * Provides specific operations which can be performed with {@link Role} objects.
 */
public interface RoleService extends GenericService<Role> {
}

