package by.epam.newsmanagmentapp.domain.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Class that contains search parameters: author id and list of tag ides.
 */
public class SearchCriteria implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long authorId;
    private List<Long> tagsId;

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public List<Long> getTagsId() {
        return tagsId;
    }

    public void setTagsId(List<Long> tagsId) {
        this.tagsId = tagsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchCriteria that = (SearchCriteria) o;

        if (authorId != null ? !authorId.equals(that.authorId) : that.authorId != null) return false;
        return tagsId != null ? tagsId.equals(that.tagsId) : that.tagsId == null;

    }

    @Override
    public int hashCode() {
        int result = authorId != null ? authorId.hashCode() : 0;
        result = 31 * result + (tagsId != null ? tagsId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SearchCriteria{");
        sb.append("authorId=").append(authorId);
        sb.append(", tagsId=").append(tagsId);
        sb.append('}');
        return sb.toString();
    }
}

