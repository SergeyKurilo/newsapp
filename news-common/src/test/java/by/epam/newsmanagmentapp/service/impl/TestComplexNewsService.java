package by.epam.newsmanagmentapp.service.impl;

import by.epam.newsmanagmentapp.domain.bean.Author;
import by.epam.newsmanagmentapp.domain.bean.Comment;
import by.epam.newsmanagmentapp.domain.bean.News;
import by.epam.newsmanagmentapp.domain.bean.Tag;
import by.epam.newsmanagmentapp.domain.to.NewsTO;
import by.epam.newsmanagmentapp.exceptions.ServiceException;
import by.epam.newsmanagmentapp.service.AuthorService;
import by.epam.newsmanagmentapp.service.CommentService;
import by.epam.newsmanagmentapp.service.NewsService;
import by.epam.newsmanagmentapp.service.TagService;
import by.epam.newsmanagmentapp.service.impl.ComplexNewsServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestComplexNewsService {

    @Mock
    private NewsService newsService;

    @Mock
    private CommentService commentService;

    @Mock
    private TagService tagService;

    @Mock
    private AuthorService authorService;

    @InjectMocks
    private ComplexNewsServiceImpl complexNewsService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateNewsWithAuthorAndTags() throws ServiceException {
        News news = new News();
        Long authorId = 1L;
        Long tagIdOne = 1L;
        Long tagIdTwo = 2L;
        when(newsService.insert(news)).thenReturn(1L);
        complexNewsService.createNewsWithAuthorAndTags(news, authorId, tagIdOne, tagIdTwo);
        verify(newsService).insert(news);
        verify(newsService).createNewsAuthorLink(1L, authorId);
        verify(newsService).createNewsTagsLinks(1L, tagIdOne, tagIdTwo);
    }

    @Test
    public void testDeleteNewsWithAllLinks() throws ServiceException {
        Long newsId = 1L;
        complexNewsService.deleteNewsWithAllLinks(newsId);
        verify(newsService).deleteNewsAuthorLink(newsId);
        verify(newsService).deleteNewsTagLink(newsId);
        verify(commentService).deleteByNewsId(newsId);
        verify(newsService).delete(newsId);
    }

    @Test
    public void testLoadNewsTO() throws ServiceException {
        Long newsId = 1L;
        @SuppressWarnings("deprecation") News news = new News(1L, "sport", "Basketball hey hey", "Hello everybody", Timestamp.valueOf("2016-05-18 08:58:03.628") , new Date(2015, 10, 11));
        List<Tag> tags = new ArrayList<>();
        List<Comment> comments = new ArrayList<>();
        Author author = new Author();

        when(newsService.getByKey(newsId)).thenReturn(news);
        when(tagService.getTagsByNewsId(newsId)).thenReturn(tags);
        when(authorService.getAuthorByNewsId(newsId)).thenReturn(author);
        when(commentService.getCommentsByNewsId(newsId)).thenReturn(comments);

        NewsTO newsTO = complexNewsService.loadNewsTO(newsId);
        verify(newsService).getByKey(newsId);
        verify(tagService).getTagsByNewsId(newsId);
        verify(authorService).getAuthorByNewsId(newsId);
        verify(commentService).getCommentsByNewsId(newsId);

        assertEquals(newsTO.getNews().getCreationDate(), news.getCreationDate());
        assertEquals(newsTO.getComments(), comments);
    }
}
