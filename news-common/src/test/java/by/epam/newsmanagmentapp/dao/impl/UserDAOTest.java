package by.epam.newsmanagmentapp.dao.impl;

import by.epam.newsmanagmentapp.dao.UserDAO;
import by.epam.newsmanagmentapp.domain.bean.User;
import by.epam.newsmanagmentapp.exceptions.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import junit.framework.TestCase;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-application-context.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
@DatabaseSetup(value = "classpath:by/epam/newsmanagmentapp/dao/UserDAOTest.xml", type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = "classpath:by/epam/newsmanagmentapp/dao/UserDAOTest.xml", type = DatabaseOperation.DELETE_ALL)
public class UserDAOTest {

    @Autowired
    private UserDAO userDAO;

    @BeforeClass
    public static void setLocale() {
        Locale.setDefault(Locale.ENGLISH);
    }

    @Test
    public void testFindAll() throws DAOException {
        final int size = 3;
        List<User> userList = userDAO.findAll();
        assertEquals(size, userList.size());
    }

    @Test
    public void testInsert() throws DAOException {
        User user = new User();
        user.setUserName("username");
        user.setLogin("login");
        user.setPassword("password");
        user.setRoleId(1L);
        Long userId = userDAO.insert(user);
        User actualUser = userDAO.getByKey(userId);
        assertEquals(userId, actualUser.getUserId());
    }

    @Test
    public void testDelete() throws DAOException {
        final Long id = 3L;
        final int size = 2;
        userDAO.delete(id);
        List<User> users = userDAO.findAll();
        TestCase.assertEquals(size, users.size());
    }

    @Test
    public void testGetByKey() throws DAOException {
        final Long id = 1L;
        User user = userDAO.getByKey(id);
        TestCase.assertEquals(id, user.getUserId());
    }

    @Test
    public void testUpdate() throws DAOException {
        final String USER_LOGIN = "Sia";
        final Long ID = 1L;
        User user = userDAO.getByKey(ID);
        user.setLogin(USER_LOGIN);
        userDAO.update(user);
        User actual = userDAO.getByKey(ID);
        assertEquals(user, actual);
    }
}
