package by.epam.newsmanagmentapp.dao;

import by.epam.newsmanagmentapp.domain.bean.Role;

/**
 * Describes specific operations which can be performed with {@link Role} objects in the database.
 */
public interface RoleDAO extends GenericDAO<Role> {
}
