package by.epam.newsmanagmentapp.dao.impl;

import by.epam.newsmanagmentapp.dao.TagDAO;
import by.epam.newsmanagmentapp.dao.util.DBUtil;
import by.epam.newsmanagmentapp.domain.bean.Tag;
import by.epam.newsmanagmentapp.exceptions.DAOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of {@link TagDAO} using JDBC
 */
@Component
public class TagDAOImpl implements TagDAO {

    @Autowired
    DataSource dataSource;

    private static final String TAG_ID = "TAG_ID";
    private static final String TAG_NAME = "TAG_NAME";

    private static final String SQL_SELECT_ALL_TAGS = "SELECT TAG_ID, TAG_NAME FROM TAGS";
    private static final String SQL_INSERT_TAG = "INSERT INTO TAGS(TAG_NAME) VALUES (?)";
    private static final String SQL_DELETE_TAG_BY_TAG_ID = "DELETE FROM TAGS WHERE TAG_ID=?";
    private static final String SQL_SELECT_TAG_BY_TAG_ID = "SELECT TAG_ID,TAG_NAME FROM TAGS WHERE TAG_ID=?";
    private static final String SQL_UPDATE_TAG = "UPDATE TAGS SET TAG_NAME=? WHERE TAG_ID = ?";
    private static final String SQL_SELECT_TAGS_BY_NEWS_ID = "SELECT TAG_ID, TAG_NAME from TAGS INNER JOIN NEWS_TAGS ON TAGS.TAG_ID = NEWS_TAGS.NWS_TAG_ID and NEWS_TAGS.NWS_NEWS_ID = ?";


    private TagDAOImpl() {
    }

    /**
     * Implementation of {@link TagDAO#findAll()} method using simple JDBC.
     */
    @Override
    public List<Tag> findAll() throws DAOException {
        List<Tag> tags = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_TAGS);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Tag tag = new Tag();
                tag.setTagId(rs.getLong(TAG_ID));
                tag.setTagName(rs.getString(TAG_NAME));
                tags.add(tag);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
        return tags;
    }

    /**
     * Implementation of {@link TagDAO#insert(Object)} method using simple JDBC.
     */
    @Override
    public Long insert(Tag tag) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        Long id = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_INSERT_TAG, new String[]{TAG_ID});
            preparedStatement.setString(1, tag.getTagName());
            preparedStatement.executeUpdate();

            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    id = generatedKeys.getLong(1);
                } else {
                    throw new DAOException("Creating tag failed, no ID obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement);
        }
        return id;
    }

    /**
     * Implementation of {@link TagDAO#delete(Long)} method using simple JDBC.
     */
    @Override
    public void delete(Long id) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_DELETE_TAG_BY_TAG_ID);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement);
        }
    }

    /**
     * Implementation of {@link TagDAO#getByKey(Long)} method using simple JDBC.
     */
    @Override
    public Tag getByKey(Long id) throws DAOException {
        Tag tag = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_TAG_BY_TAG_ID);
            preparedStatement.setLong(1, id);
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                tag = new Tag();
                tag.setTagId(rs.getLong(TAG_ID));
                tag.setTagName(rs.getString(TAG_NAME));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
        return tag;
    }

    /**
     * Implementation of {@link TagDAO#update(Object)} method using simple JDBC.
     */
    @Override
    public void update(Tag tag) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_UPDATE_TAG);
            preparedStatement.setString(1, tag.getTagName());
            preparedStatement.setLong(2, tag.getTagId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement);
        }
    }

    /**
     * Implementation of {@link TagDAO#getTagsByNewsId(Long)} method using simple JDBC.
     */
    @Override
    public List<Tag> getTagsByNewsId(Long newsId) throws DAOException {
        List<Tag> tags = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_TAGS_BY_NEWS_ID);
            preparedStatement.setLong(1, newsId);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                buildListOfTagsFromResultSet(rs, tags);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
        return tags;
    }

    private void buildListOfTagsFromResultSet(final ResultSet rs, final List<Tag> tags) throws SQLException {
        Tag tag = new Tag();
        tag.setTagId(rs.getLong(TAG_ID));
        tag.setTagName(rs.getString(TAG_NAME));
        tags.add(tag);
    }
}

