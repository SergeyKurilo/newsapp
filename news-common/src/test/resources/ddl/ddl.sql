--------------------------------------------------------
--  File created - �����������-������-11-2016   
--------------------------------------------------------
 --------------------------------------------------------
  --  DROPING TABLES
  --------------------------------------------------------
  --------------------------------------------------------
  --DROP TABLE USERS
  --------------------------------------------------------
  DROP TABLE USERS;
   --------------------------------------------------------
  --DROP TABLE ROLES
  --------------------------------------------------------
  DROP TABLE ROLES;
  --------------------------------------------------------
  --DROP TABLE NEWS_TAGS
  --------------------------------------------------------
  DROP TABLE NEWS_TAGS;
  --------------------------------------------------------
  --DROP TABLE NEWS_AUTHORS
  --------------------------------------------------------
  DROP TABLE NEWS_AUTHORS;
  --------------------------------------------------------
  --DROP TABLE TAGS
  --------------------------------------------------------
  DROP TABLE TAGS;
  --------------------------------------------------------
  --DROP TABLE COMMENTS
  --------------------------------------------------------
  DROP TABLE COMMENTS;
  --DROP TABLE AUTHORS
  --------------------------------------------------------
  DROP TABLE AUTHORS;
  --DROP TABLE NEWS
  --------------------------------------------------------
  DROP TABLE NEWS;
  --------------------------------------------------------
  --------------------------------------------------------
  --------------------------------------------------------
  DROP SEQUENCE ATR_ID_SEQ;
  DROP SEQUENCE CMT_ID_SEQ;
  DROP SEQUENCE NWS_ID_SEQ;
  DROP SEQUENCE TAG_ID_SEQ;
  DROP SEQUENCE USR_ID_SEQ;
  DROP SEQUENCE ROLE_ID_SEQ;
--------------------------------------------------------
--  DDL for Sequence ATR_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ATR_ID_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence CMT_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "CMT_ID_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence NWS_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "NWS_ID_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence TAG_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "TAG_ID_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence USR_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "USR_ID_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;

 --------------------------------------------------------
--  DDL for Sequence ROLE_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ROLE_ID_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table AUTHORS
--------------------------------------------------------

  CREATE TABLE "AUTHORS" 
   (	"ATR_ID" NUMBER(20,0), 
	"ATR_NAME" NVARCHAR2(30), 
	"ATR_EXPIRED" TIMESTAMP (6)
   ); 
--------------------------------------------------------
--  DDL for Table COMMENTS
--------------------------------------------------------

  CREATE TABLE "COMMENTS" 
   (	"CMT_ID" NUMBER(20,0), 
	"CMT_TEXT" NVARCHAR2(100), 
	"CMT_CREATION_DATE" TIMESTAMP (6), 
	"CMT_NWS_ID" NUMBER(20,0)
   ); 
--------------------------------------------------------
--  DDL for Table NEWS
--------------------------------------------------------

  CREATE TABLE "NEWS" 
   (	"NWS_ID" NUMBER(20,0), 
	"NWS_TITLE" NVARCHAR2(200),
	"NWS_SHORT_TEXT" NVARCHAR2(200),
	"NWS_FULL_TEXT" NVARCHAR2(2000),
	"NWS_CREATION_DATE" TIMESTAMP (6), 
	"NWS_MODIFICATION_DATE" DATE
   ); 
--------------------------------------------------------
--  DDL for Table NEWS_AUTHORS
--------------------------------------------------------

  CREATE TABLE "NEWS_AUTHORS" 
   (	"NAT_NEWS_ID" NUMBER(20,0), 
	"NAT_ATR_ID" NUMBER(20,0)
   ); 
--------------------------------------------------------
--  DDL for Table NEWS_TAGS
--------------------------------------------------------

  CREATE TABLE "NEWS_TAGS" 
   (	"NWS_NEWS_ID" NUMBER(20,0), 
	"NWS_TAG_ID" NUMBER(20,0)
   );
--------------------------------------------------------
--  DDL for Table ROLES
--------------------------------------------------------

  CREATE TABLE "ROLES" 
   (	"ROLE_ID" NUMBER(20,0), 
	"ROLE_NAME" VARCHAR2(50 BYTE)
   );
--------------------------------------------------------
--  DDL for Table TAGS
--------------------------------------------------------

  CREATE TABLE "TAGS" 
   (	"TAG_ID" NUMBER(20,0), 
	"TAG_NAME" NVARCHAR2(30)
   );
--------------------------------------------------------
--  DDL for Table USERS
--------------------------------------------------------

  CREATE TABLE "USERS" 
   (	"USR_ID" NUMBER(20,0), 
	"USR_NAME" NVARCHAR2(50), 
	"USR_LOGIN" VARCHAR2(20 BYTE), 
	"USR_PASSWORD" VARCHAR2(32 BYTE),
	"USR_ROLE_ID" NUMBER(20,0)
   ) ;
--------------------------------------------------------
--  DDL for Index AUTHOR_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "AUTHOR_PK" ON "AUTHORS" ("ATR_ID");
--------------------------------------------------------
--  DDL for Index COMMENTS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "COMMENTS_PK" ON "COMMENTS" ("CMT_ID");
--------------------------------------------------------
--  DDL for Index NEWS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "NEWS_PK" ON "NEWS" ("NWS_ID");
--------------------------------------------------------
--  DDL for Index ROLES_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ROLES_PK" ON "ROLES" ("ROLE_ID");
--------------------------------------------------------
--  DDL for Index TAG_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "TAG_PK" ON "TAGS" ("TAG_ID");
--------------------------------------------------------
--  DDL for Index USERS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "USERS_PK" ON "USERS" ("USR_ID");
--------------------------------------------------------
--  Constraints for Table AUTHORS
--------------------------------------------------------

  ALTER TABLE "AUTHORS" MODIFY ("ATR_ID" NOT NULL ENABLE);
  ALTER TABLE "AUTHORS" MODIFY ("ATR_NAME" NOT NULL ENABLE);
  ALTER TABLE "AUTHORS" MODIFY ("ATR_EXPIRED" NOT NULL ENABLE);
  ALTER TABLE "AUTHORS" ADD CONSTRAINT "AUTHOR_PK" PRIMARY KEY ("ATR_ID");
--------------------------------------------------------
--  Constraints for Table COMMENTS
--------------------------------------------------------

  ALTER TABLE "COMMENTS" MODIFY ("CMT_ID" NOT NULL ENABLE);
  ALTER TABLE "COMMENTS" MODIFY ("CMT_TEXT" NOT NULL ENABLE);
  ALTER TABLE "COMMENTS" MODIFY ("CMT_CREATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "COMMENTS" ADD CONSTRAINT "COMMENTS_PK" PRIMARY KEY ("CMT_ID");
  ALTER TABLE "COMMENTS" MODIFY ("CMT_NWS_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table NEWS
--------------------------------------------------------

  ALTER TABLE "NEWS" MODIFY ("NWS_ID" NOT NULL ENABLE);
  ALTER TABLE "NEWS" MODIFY ("NWS_TITLE" NOT NULL ENABLE);
  ALTER TABLE "NEWS" MODIFY ("NWS_SHORT_TEXT" NOT NULL ENABLE);
  ALTER TABLE "NEWS" MODIFY ("NWS_FULL_TEXT" NOT NULL ENABLE);
  ALTER TABLE "NEWS" MODIFY ("NWS_CREATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "NEWS" MODIFY ("NWS_MODIFICATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "NEWS" ADD CONSTRAINT "NEWS_PK" PRIMARY KEY ("NWS_ID");
--------------------------------------------------------
--  Constraints for Table NEWS_AUTHORS
--------------------------------------------------------

  ALTER TABLE "NEWS_AUTHORS" MODIFY ("NAT_NEWS_ID" NOT NULL ENABLE);
  ALTER TABLE "NEWS_AUTHORS" MODIFY ("NAT_ATR_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table NEWS_TAGS
--------------------------------------------------------

  ALTER TABLE "NEWS_TAGS" MODIFY ("NWS_NEWS_ID" NOT NULL ENABLE);
  ALTER TABLE "NEWS_TAGS" MODIFY ("NWS_TAG_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ROLES
--------------------------------------------------------

  ALTER TABLE "ROLES" ADD CONSTRAINT "ROLES_PK" PRIMARY KEY ("ROLE_ID");
  ALTER TABLE "ROLES" MODIFY ("ROLE_ID" NOT NULL ENABLE);
  ALTER TABLE "ROLES" MODIFY ("ROLE_NAME" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TAGS
--------------------------------------------------------

  ALTER TABLE "TAGS" MODIFY ("TAG_ID" NOT NULL ENABLE);
  ALTER TABLE "TAGS" MODIFY ("TAG_NAME" NOT NULL ENABLE);
  ALTER TABLE "TAGS" ADD CONSTRAINT "TAG_PK" PRIMARY KEY ("TAG_ID");
--------------------------------------------------------
--  Constraints for Table USERS
--------------------------------------------------------

  ALTER TABLE "USERS" MODIFY ("USR_ROLE_ID" NOT NULL ENABLE);
  ALTER TABLE "USERS" MODIFY ("USR_ID" NOT NULL ENABLE);
  ALTER TABLE "USERS" MODIFY ("USR_NAME" NOT NULL ENABLE);
  ALTER TABLE "USERS" MODIFY ("USR_LOGIN" NOT NULL ENABLE);
  ALTER TABLE "USERS" MODIFY ("USR_PASSWORD" NOT NULL ENABLE);
  ALTER TABLE "USERS" ADD CONSTRAINT "USERS_PK" PRIMARY KEY ("USR_ID");
--------------------------------------------------------
--  Ref Constraints for Table COMMENTS
--------------------------------------------------------

  ALTER TABLE "COMMENTS" ADD CONSTRAINT "CMT_NWS_FK" FOREIGN KEY ("CMT_NWS_ID")
	  REFERENCES "NEWS" ("NWS_ID") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table NEWS_AUTHORS
--------------------------------------------------------

  ALTER TABLE "NEWS_AUTHORS" ADD CONSTRAINT "NAT_ATR_FK" FOREIGN KEY ("NAT_ATR_ID")
	  REFERENCES "AUTHORS" ("ATR_ID") ON DELETE CASCADE ENABLE;
  ALTER TABLE "NEWS_AUTHORS" ADD CONSTRAINT "NAT_NEWS_FK" FOREIGN KEY ("NAT_NEWS_ID")
	  REFERENCES "NEWS" ("NWS_ID") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table NEWS_TAGS
--------------------------------------------------------

  ALTER TABLE"NEWS_TAGS" ADD CONSTRAINT "NWS_NEWS_FK" FOREIGN KEY ("NWS_NEWS_ID")
	  REFERENCES "NEWS" ("NWS_ID") ON DELETE CASCADE ENABLE;
  ALTER TABLE "NEWS_TAGS" ADD CONSTRAINT "NWS_TAG_FK" FOREIGN KEY ("NWS_TAG_ID")
	  REFERENCES "TAGS" ("TAG_ID") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table USERS
--------------------------------------------------------

  ALTER TABLE "USERS" ADD CONSTRAINT "USR_ROLE_FK" FOREIGN KEY ("USR_ROLE_ID")
	  REFERENCES "ROLES" ("ROLE_ID") ENABLE;
--------------------------------------------------------
--  DDL for Trigger AUT_TRIG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "AUT_TRIG" 
before insert on AUTHORS
for each row
  begin
    select ATR_ID_SEQ.NEXTVAL INTO :NEW.ATR_ID FROM DUAL;
  end;
/
ALTER TRIGGER "AUT_TRIG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger COMMENTS_TRIG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "COMMENTS_TRIG" 
before insert on COMMENTS
for each row
  begin
    select CMT_ID_SEQ.NEXTVAL INTO :NEW.CMT_ID FROM DUAL;
  end;
/
ALTER TRIGGER "COMMENTS_TRIG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger NEWS_TRIG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "NEWS_TRIG" 
before insert on NEWS
for each row
  begin
    select NWS_ID_SEQ.NEXTVAL INTO :NEW.NWS_ID FROM DUAL;
  end;
/
ALTER TRIGGER "NEWS_TRIG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TAG_TRIG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "TAG_TRIG" 
before insert on TAGS
for each row
  begin
    select TAG_ID_SEQ.NEXTVAL INTO :NEW.TAG_ID FROM DUAL;
  end;
/
ALTER TRIGGER "TAG_TRIG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger USERS_TRIG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "USERS_TRIG" 
before insert on USERS
for each row
  begin
    select USR_ID_SEQ.NEXTVAL INTO :NEW.USR_ID FROM DUAL;
  end;
/
ALTER TRIGGER "USERS_TRIG" ENABLE;
