<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="col-sm-10">
  <form:form class="form" method="post" modelAttribute="searchCriteria"
             action="${pageContext.request.contextPath}/searchresult?page=1">
    <div class="col-md-5">
      <form:select path="authorId">
        <form:option value="" label="Select author"/>
        <form:options items="${authorsList}" itemValue="authorId" itemLabel="authorName"/>
      </form:select>
    </div>
    <div class="col-md-5">
      <form:select path="tagsId" multiple="true">
        <form:options items="${tagsList}" itemValue="tagId" itemLabel="tagName"/>
      </form:select>
    </div>
    <input class="btn btn-info" type="submit" value="Filter"/>
  </form:form>
  <form:form method="post" action="${pageContext.request.contextPath}/deleteCriteria">
    <input class="btn btn-info" type="submit" value="Reset"/>
  </form:form>
  <form:form method="post" modelAttribute="newsToDelete" action="${pageContext.request.contextPath}/deleteNews">
    <c:forEach items="${newsTOs}" var="newsTO">
      <div class="grid news_desc">
        <h3><a href="${pageContext.request.contextPath}/news/${newsTO.news.newsId}"
               title="More">${newsTO.news.title} </a></h3>
        <form:checkbox path="newsIds" value="${newsTO.news.newsId}"/>
        <h4>Posted on <fmt:formatDate value="${newsTO.news.creationDate}" pattern="MM/dd/yyyy"/> by
          <span><a>${newsTO.author.authorName}</a></span></h4>
        <p>${newsTO.news.shortText}<a href="${pageContext.request.contextPath}/news/${newsTO.news.newsId}" title="More">[....]</a>
        </p>
      </div>
      <a href="${pageContext.request.contextPath}/editNews/${newsTO.news.newsId}" title="Edit">Edit</a>
    </c:forEach>
    <input class="btn btn-info" type="submit" value="Delete"/>
  </form:form>
  <div class="pagination-custom">
    <ul class="pagination">
      <c:forEach begin="1" end="${numberOfPages}" step="1" var="page">
        <li><a href="${pageContext.request.contextPath}/allnews?page=${page}">${page}</a></li>
      </c:forEach>
    </ul>
  </div>
</div>