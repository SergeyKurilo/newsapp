package by.epam.newsmanagmentapp.domain.to;

import by.epam.newsmanagmentapp.domain.bean.News;

import java.util.List;

public class NewsToDeleteTO {
  private List<Long> newsIds;

  public NewsToDeleteTO() {
  }

  public NewsToDeleteTO(List<Long> newsIds) {
    this.newsIds = newsIds;
  }

  public List<Long> getNewsIds() {
    return newsIds;
  }

  public void setNewsIds(List<Long> newsIds) {
    this.newsIds = newsIds;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    NewsToDeleteTO that = (NewsToDeleteTO) o;

    return newsIds != null ? newsIds.equals(that.newsIds) : that.newsIds == null;

  }

  @Override
  public int hashCode() {
    return newsIds != null ? newsIds.hashCode() : 0;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("NewsToDeleteTO{");
    sb.append("newsIds=").append(newsIds);
    sb.append('}');
    return sb.toString();
  }
}
