package by.epam.newsmanagmentapp.controller.commands.impl;

import by.epam.newsmanagmentapp.controller.commands.ICommand;
import by.epam.newsmanagmentapp.utils.Action;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Component
public class BackPage implements ICommand {

  private static final String CURRENT_PAGE = "currentPage";
  private static final String ACTION_PARAM = "action";
  private static final String PATH = "/ClientController?page=";

  @Override
  public String execute(HttpServletRequest request) {
    HttpSession session = request.getSession();
    Long page = (Long) session.getAttribute(CURRENT_PAGE);
    request.setAttribute(ACTION_PARAM, Action.REDIRECT);
    return PATH + page;
  }
}
