package by.epam.newsmanagmentapp.utils;

import by.epam.newsmanagmentapp.exceptions.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice("by.epam.newsmanagmentapp.controller")
public class GlobalExceptionHandler {

  private static final Logger LOGGER = LogManager.getLogger(GlobalExceptionHandler.class);

  @ExceptionHandler(ServiceException.class)
  public ModelAndView handleServiceException(Exception exception) {
    ModelAndView modelAndView = new ModelAndView();
    LOGGER.error("ServiceException Occured: ", exception);
    modelAndView.setViewName("serviceException");
    return modelAndView;
  }

  @ExceptionHandler(Throwable.class)
  public ModelAndView handleThrowableException(Exception exception) {
    ModelAndView modelAndView = new ModelAndView();
    LOGGER.error("Exception Occured: ", exception);
    modelAndView.setViewName("throwableException");
    return modelAndView;
  }

}
