package by.epam.newsmanagmentapp.service.impl;

import by.epam.newsmanagmentapp.dao.RoleDAO;
import by.epam.newsmanagmentapp.domain.bean.Role;
import by.epam.newsmanagmentapp.exceptions.DAOException;
import by.epam.newsmanagmentapp.exceptions.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TestRoleService {

    @InjectMocks
    private RoleServiceImpl roleService;

    @Mock
    private RoleDAO roleDAO;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateRole() throws ServiceException, DAOException {
        Role role = new Role();
        when(roleDAO.insert(role)).thenReturn(1L);
        Long tempId = roleService.insert(role);
        assertNotNull(tempId);
        verify(roleDAO).insert(role);
    }

    @Test(expected = ServiceException.class)
    public void testCreateRoleExceptionSituation() throws ServiceException, DAOException {
        Role role = new Role();
        when(roleDAO.insert(role)).thenThrow(DAOException.class);
        Long tempId = roleService.insert(role);
    }

    @Test
    public void testUpdateRole() throws ServiceException, DAOException {
        Role role = new Role();
        roleService.update(role);
        verify(roleDAO, times(1)).update(role);
    }

    @Test
    public void testGetRoleById() throws ServiceException, DAOException {
        Role role = new Role();
        Long authorId = 1L;
        when(roleDAO.getByKey(authorId)).thenReturn(role);
        Role tempRole = roleService.getByKey(authorId);
        assertEquals(tempRole, role);
        verify(roleDAO).getByKey(authorId);
    }


    @Test
    public void testDeleteRole() throws ServiceException, DAOException {
        Long roleId = 1L;
        roleService.delete(roleId);
        verify(roleDAO).delete(roleId);
    }


    @Test
    public void testFindAllTags() throws ServiceException, DAOException {
        when(roleDAO.findAll()).thenReturn(new ArrayList<>());
        List<Role> result = roleService.findAll();
        assertNotNull(result);
        verify(roleDAO).findAll();
    }
}
