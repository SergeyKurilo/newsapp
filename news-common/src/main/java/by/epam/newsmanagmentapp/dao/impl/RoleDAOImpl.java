package by.epam.newsmanagmentapp.dao.impl;

import by.epam.newsmanagmentapp.dao.RoleDAO;
import by.epam.newsmanagmentapp.dao.util.DBUtil;
import by.epam.newsmanagmentapp.domain.bean.Role;
import by.epam.newsmanagmentapp.exceptions.DAOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of {@link RoleDAO} using JDBC
 */
@Component
public class RoleDAOImpl implements RoleDAO {

    @Autowired
    DataSource dataSource;

    private static final String ROLE_ID = "ROLE_ID";
    private static final String ROLE_NAME = "ROLE_NAME";

    private static final String SQL_SELECT_ALL_ROLES = "SELECT ROLE_ID, ROLE_NAME FROM ROLES";
    private static final String SQL_SELECT_ROLE_BY_ROLE_ID = "SELECT ROLE_ID,ROLE_NAME FROM ROLES WHERE ROLE_ID=?";
    private static final String SQL_UPDATE_ROLE = "UPDATE ROLES SET ROLE_NAME=? WHERE ROLE_ID = ?";
    private static final String SQL_INSERT_ROLE = "INSERT INTO ROLES(ROLE_NAME) VALUES (?)";
    private static final String SQL_DELETE_ROLE_BY_ROLE_ID = "DELETE FROM ROLES WHERE ROLE_ID=?";


    private RoleDAOImpl() {

    }

    /**
     * Implementation of {@link RoleDAO#findAll()} method using simple JDBC.
     */
    @Override
    public List<Role> findAll() throws DAOException {
        List<Role> roles = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_ROLES);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Role role = new Role();
                role.setRoleId(rs.getLong(ROLE_ID));
                role.setRoleName(rs.getString(ROLE_NAME));
                roles.add(role);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
        return roles;
    }

    /**
     * Implementation of {@link RoleDAO#getByKey(Long)} method using simple JDBC.
     */
    @Override
    public Role getByKey(Long id) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        Role role = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_ROLE_BY_ROLE_ID);
            preparedStatement.setLong(1, id);
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                role = new Role();
                role.setRoleId(rs.getLong(ROLE_ID));
                role.setRoleName(rs.getString(ROLE_NAME));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
        return role;
    }

    /**
     * Implementation of {@link RoleDAO#update(Object)} method using simple JDBC.
     */
    @Override
    public void update(Role role) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_UPDATE_ROLE);
            preparedStatement.setString(1, role.getRoleName());
            preparedStatement.setLong(2, role.getRoleId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement);
        }
    }

    /**
     * Implementation of {@link RoleDAO#insert(Object)} method using simple JDBC.
     */
    @Override
    public Long insert(Role role) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        Long id = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_INSERT_ROLE, new String[]{ROLE_ID});
            preparedStatement.setString(1, role.getRoleName());
            preparedStatement.executeUpdate();

            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    id = generatedKeys.getLong(1);
                } else {
                    throw new DAOException("Creating role failed, no ID obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement);
        }
        return id;
    }

    /**
     * Implementation of {@link RoleDAO#delete(Long)} method using simple JDBC.
     */
    @Override
    public void delete(Long id) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_DELETE_ROLE_BY_ROLE_ID);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement);
        }
    }

}
