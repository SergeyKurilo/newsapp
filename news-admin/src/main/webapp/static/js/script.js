$(document).ready(function () {
    setNavigation();
});

function setNavigation() {
    var path = window.location.pathname;
    var urlRegExp = new RegExp(path == '/' ? window.location.origin + '/?$' : path.replace(/\/$/, ''));

    $(".nav a").each(function () {
        if (urlRegExp.test(this.href.replace(/\/$/, ''))) {
            $(this).closest('li').addClass('active');
            return false;
        }
    });
}


var HIDDEN_CLASS = "hidden";
var UPDATE = "#update";
var DELETE = "#delete";
var EDIT_ID = "#edit";
var EXPIRED_ID = "#expired";
var CANCEL_ID = "#cancel";
var AUTHOR_ID = "#author";
var TAG_ID = "#tag";
var DISABLED = 'disabled';
var defaultData = null;

function showAuthorButtons(object) {

    var tempId = object.id;
    var buttonId = parseInt(tempId.replace(/\D+/g, ""));
    var dataId = AUTHOR_ID + buttonId;
    var defaultData = $(dataId).val();

    $(object).attr(HIDDEN_CLASS);

    var saveId = UPDATE + buttonId;
    $(saveId).removeClass(HIDDEN_CLASS);

    var expiredId = EXPIRED_ID + buttonId;
    $(expiredId).removeClass(HIDDEN_CLASS);

    var cancelId = CANCEL_ID + buttonId;
    $(cancelId).removeClass(HIDDEN_CLASS);

    var editId = EDIT_ID + buttonId;
    $(editId).addClass(HIDDEN_CLASS)

    var nameId = AUTHOR_ID + buttonId;
    $(nameId).removeAttr(DISABLED);
}

function hideAuthorButtons(object) {
    var tempId = object.id;
    var buttonId = parseInt(tempId.replace(/\D+/g, ""));
    var dataId = AUTHOR_ID + buttonId;
    $(dataId).val(defaultData);

    $(object).addClass(HIDDEN_CLASS);

    var saveId = UPDATE + buttonId;
    $(saveId).addClass(HIDDEN_CLASS);

    var expiredId = EXPIRED_ID + buttonId;
    $(expiredId).addClass(HIDDEN_CLASS);

    var editId = EDIT_ID + buttonId;
    $(editId).removeClass(HIDDEN_CLASS);

    var cancelId = CANCEL_ID + buttonId;
    $(cancelId).addClass(HIDDEN_CLASS);

    var nameId = AUTHOR_ID + buttonId;
    $(nameId).attr(DISABLED, true);
}

function showTagButtons(object) {

    var tempId = object.id;
    var buttonId = parseInt(tempId.replace(/\D+/g, ""));
    var dataId = TAG_ID + buttonId;
    var defaultData = $(dataId).val();

    $(object).attr(HIDDEN_CLASS);

    var saveId = UPDATE + buttonId;
    $(saveId).removeClass(HIDDEN_CLASS);

    var expiredId = DELETE + buttonId;
    $(expiredId).removeClass(HIDDEN_CLASS);

    var cancelId = CANCEL_ID + buttonId;
    $(cancelId).removeClass(HIDDEN_CLASS);

    var editId = EDIT_ID + buttonId;
    $(editId).addClass(HIDDEN_CLASS)

    var nameId = TAG_ID + buttonId;
    $(nameId).removeAttr(DISABLED);
}

function hideTagButtons(object) {
    var tempId = object.id;
    var buttonId = parseInt(tempId.replace(/\D+/g, ""));
    var dataId = TAG_ID + buttonId;
    $(dataId).val(defaultData);

    $(object).addClass(HIDDEN_CLASS);

    var saveId = UPDATE + buttonId;
    $(saveId).addClass(HIDDEN_CLASS);

    var expiredId = DELETE + buttonId;
    $(expiredId).addClass(HIDDEN_CLASS);

    var editId = EDIT_ID + buttonId;
    $(editId).removeClass(HIDDEN_CLASS);

    var cancelId = CANCEL_ID + buttonId;
    $(cancelId).addClass(HIDDEN_CLASS);

    var nameId = TAG_ID + buttonId;
    $(nameId).attr(DISABLED, true);
}
