package by.epam.newsmanagmentapp.service.impl;

import by.epam.newsmanagmentapp.dao.AuthorDAO;
import by.epam.newsmanagmentapp.domain.bean.Author;
import by.epam.newsmanagmentapp.exceptions.DAOException;
import by.epam.newsmanagmentapp.exceptions.ServiceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {

    @InjectMocks
    private AuthorServiceImpl authorService;

    @Mock
    private AuthorDAO authorDAO;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateAuthor() throws ServiceException, DAOException {
        Author author = new Author();
        when(authorDAO.insert(author)).thenReturn(1L);
        Long tempId = authorService.insert(author);
        Assert.assertNotNull(tempId);
        verify(authorDAO).insert(author);
    }

    @Test(expected = ServiceException.class)
    public void testCreateAuthorExceptionSituation() throws ServiceException, DAOException {
        Author author = new Author();
        when(authorDAO.insert(author)).thenThrow(DAOException.class);
        authorService.insert(author);
    }

    @Test
    public void testUpdateAuthor() throws ServiceException, DAOException {
        Author author = new Author();
        authorService.update(author);
        verify(authorDAO, times(1)).update(author);
    }

    @Test
    public void testGetAuthorById() throws ServiceException, DAOException{
        Author author = new Author();
        Long authorId = 1L;
        when(authorDAO.getByKey(authorId)).thenReturn(author);
        Author tempAuthor = authorService.getByKey(authorId);
        Assert.assertEquals(tempAuthor, author);
        verify(authorDAO).getByKey(authorId);
    }

    @Test
    public void testDeleteAuthor() throws ServiceException, DAOException {
        Long authorId = 1L;
        authorService.delete(authorId);
        verify(authorDAO).delete(authorId);
    }

    @Test
    public void testGetNoExpiredAuthors() throws ServiceException, DAOException {
        when(authorDAO.getNotExpiredAuthors()).thenReturn(new ArrayList<>());
        List<Author> result = authorService.getNotExpiredAuthors();
        Assert.assertNotNull(result);
        verify(authorDAO).getNotExpiredAuthors();
    }

    @Test
    public void testFindAll() throws ServiceException, DAOException {
        when(authorDAO.findAll()).thenReturn(new ArrayList<>());
        List<Author> result = authorService.findAll();
        Assert.assertNotNull(result);
        verify(authorDAO).findAll();
    }
}
