package by.epam.newsmanagmentapp.dao.impl;

import by.epam.newsmanagmentapp.dao.RoleDAO;
import by.epam.newsmanagmentapp.domain.bean.Role;
import by.epam.newsmanagmentapp.exceptions.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-application-context.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
@DatabaseSetup(value = "classpath:by/epam/newsmanagmentapp/dao/RoleDAOTest.xml", type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = "classpath:by/epam/newsmanagmentapp/dao/RoleDAOTest.xml", type = DatabaseOperation.DELETE_ALL)
public class RoleDAOTest {

    @Autowired
    private RoleDAO roleDAO;

    @BeforeClass
    public static void setLocale() {
        Locale.setDefault(Locale.ENGLISH);
    }

    @Test
    public void testFindAll() throws DAOException {
        final int size = 2;
        List<Role> roleList = roleDAO.findAll();
        assertEquals(size, roleList.size());
    }

    @Test
    public void testInsert() throws DAOException {
        final String NEW_ROLE_NAME = "NEW_ROLE";
        Role role = new Role();
        role.setRoleName(NEW_ROLE_NAME);
        Long roleId = roleDAO.insert(role);
        Role actualRole = roleDAO.getByKey(roleId);
        assertEquals(roleId, actualRole.getRoleId());
    }

    @Test
    public void testDelete() throws DAOException {
        final Long id = 2L;
        final int size = 1;
        roleDAO.delete(id);
        List<Role> roles = roleDAO.findAll();
        assertEquals(size, roles.size());
    }

    @Test
    public void testGetByKey() throws DAOException {
        final Long id = 1L;
        Role role = roleDAO.getByKey(id);
        assertEquals(id, role.getRoleId());
    }

    @Test
    public void testUpdate() throws DAOException {
        final String ROLE_NAME = "ROLE_USER";
        final Long ID = 1L;
        Role role = roleDAO.getByKey(ID);
        role.setRoleName(ROLE_NAME);
        roleDAO.update(role);
        Role actual = roleDAO.getByKey(ID);
        assertEquals(role, actual);
    }
}
