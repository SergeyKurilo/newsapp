package by.epam.newsmanagmentapp.service;

import by.epam.newsmanagmentapp.domain.bean.User;
import by.epam.newsmanagmentapp.exceptions.ServiceException;

/**
 * Provides specific operations which can be performed with {@link User} objects.
 */
public interface UserService extends GenericService<User> {
  User getUserInfo(String userName) throws ServiceException;
}

