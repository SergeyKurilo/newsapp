package by.epam.newsmanagmentapp.dao;

import by.epam.newsmanagmentapp.domain.bean.News;
import by.epam.newsmanagmentapp.domain.bean.SearchCriteria;
import by.epam.newsmanagmentapp.domain.to.NextAndPreviousNewsIdTO;
import by.epam.newsmanagmentapp.exceptions.DAOException;

import java.util.List;

/**
 * Describes specific operations which can be performed with {@link News} objects in the database.
 */
public interface NewsDAO extends GenericDAO<News> {

    /**
     * Load all ${@link News} appropriate criteria specified in passed ${@link SearchCriteria} object.
     *
     * @param searchCriteria ${@link SearchCriteria} object.
     * @return List of ${@link News} objects.
     * @throws DAOException if there were errors during reading from database.
     */
    List<News> findBySearchCriteria(SearchCriteria searchCriteria) throws DAOException;

    /**
     * Create link between ${@link News} and ${@link by.epam.newsmanagmentapp.domain.bean.Author}.
     *
     * @param newsId   identifier of news.
     * @param authorId identifier of author.
     * @throws DAOException if there were errors during creating link.
     */
    void createNewsAuthorLink(Long newsId, Long authorId) throws DAOException;

    /**
     * Delete link between ${@link News} and ${@link by.epam.newsmanagmentapp.domain.bean.Author}.
     *
     * @param newsId identifier of news.
     * @throws DAOException if there were errors during deleting link from database.
     */
    void deleteNewsAuthorLink(Long newsId) throws DAOException;

    /**
     * Create links between ${@link News} and ${@link by.epam.newsmanagmentapp.domain.bean.Tag}.
     *
     * @param newsId identifier of news.
     * @param tags   identifiers of tags.
     * @throws DAOException if there were errors during creating link.
     */
    void createNewsTagsLinks(Long newsId, Long... tags) throws DAOException;

    /**
     * Delete links between ${@link News} and ${@link by.epam.newsmanagmentapp.domain.bean.Tag}.
     *
     * @param newsId identifier of news.
     * @throws DAOException if there were errors during deleting link from database.
     */
    void deleteNewsTagLink(Long newsId) throws DAOException;

    /**
     * Delete links between ${@link News} and ${@link by.epam.newsmanagmentapp.domain.bean.Tag} by id of tag.
     *
     * @param tagId identifier of tag.
     * @throws DAOException if there were errors during deleting link from database.
     */
    void deleteNewsTagLinkByTag(Long tagId) throws DAOException;

    /**
     * Count number of ${@link News} records in database.
     *
     * @return number of news.
     * @throws DAOException if there were errors during reading from database.
     */
    Long getNumberOfNews() throws DAOException;

    Long getNumberOfNewsWithSearchCriteria(SearchCriteria searchCriteria) throws DAOException;

    List<News> loadPage(Long first, Long last) throws DAOException;

    List<News> loadPageWithSearchCriteria(SearchCriteria searchCriteria, Long first, Long last) throws DAOException;

    NextAndPreviousNewsIdTO getNextAndPreviousNewsId(Long newsId) throws DAOException;

    NextAndPreviousNewsIdTO getNextAndPreviousNewsIdWithSearchCriteria(SearchCriteria searchCriteria, Long newsId) throws DAOException;
}
