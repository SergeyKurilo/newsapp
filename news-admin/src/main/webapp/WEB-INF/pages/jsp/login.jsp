<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="container">
    <c:url var="loginUrl" value="${servletContext.contextPath}/j_spring_security_check"/>
    <form class="form-signin" action="${loginUrl}" method="post">
        <c:if test="${param.error != null}">
            <div class="alert alert-danger">
                <p>Invalid username and password.</p>
            </div>
        </c:if>
        <c:if test="${param.logout != null}">
            <div class="alert alert-success">
                <p>You have been logged out successfully.</p>
            </div>
        </c:if>
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="username" class="sr-only">Username</label>
        <input type="text" class="form-control" id="username" name="j_username" placeholder="Enter Username" required autofocus>
        <label for="password" class="sr-only">Password</label>
        <input type="password" class="form-control" id="password" name="j_password" placeholder="Enter Password"
               required>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>

</div>
