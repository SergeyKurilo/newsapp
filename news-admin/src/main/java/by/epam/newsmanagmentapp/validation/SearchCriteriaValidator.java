package by.epam.newsmanagmentapp.validation;

import by.epam.newsmanagmentapp.domain.bean.SearchCriteria;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class SearchCriteriaValidator implements Validator {
  @Override
  public boolean supports(Class<?> aClass) {
    return SearchCriteria.class.equals(aClass);
  }

  @Override
  public void validate(Object o, Errors errors) {
    SearchCriteria searchCriteria = (SearchCriteria) o;

    if (searchCriteria.getTagsId() == null && searchCriteria.getAuthorId() == null) {

    }
  }
}
