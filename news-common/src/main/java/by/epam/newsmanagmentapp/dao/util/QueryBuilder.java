package by.epam.newsmanagmentapp.dao.util;

import by.epam.newsmanagmentapp.domain.bean.SearchCriteria;

import java.util.List;

/**
 * Util class provides functionality for dynamic constructing SQL query according to ${@link SearchCriteria}.
 */
public class QueryBuilder {

  private static final String QUERY = "SELECT DISTINCT NWS_ID,NWS_TITLE,NWS_SHORT_TEXT,NWS_FULL_TEXT,NWS_CREATION_DATE,NWS_MODIFICATION_DATE FROM NEWS " +
          " INNER JOIN NEWS_AUTHORS ON NWS_ID=NAT_NEWS_ID INNER JOIN AUTHORS ON NAT_ATR_ID = ATR_ID" +
          " INNER JOIN NEWS_TAGS ON NWS_ID = NWS_NEWS_ID INNER JOIN TAGS ON NWS_TAG_ID = TAG_ID WHERE ";
  private static final String AUTHOR_ID = "ATR_ID =";
  private static final String AND = " AND ";
  private static final String IN = " IN ";
  private static final String TAG_ID = " TAG_ID ";
  private static final String COMMA = ",";
  private static final String BRACKET_CLOSE = ")";
  private static final String BRACKET_OPEN = "(";
  private static final String HAVING_COUNT = " HAVING COUNT (DISTINCT ";
  private static final String EQUALS = "=";
  private static final String GROUP_BY = " GROUP BY NWS_ID,NWS_TITLE,NWS_SHORT_TEXT,NWS_FULL_TEXT,NWS_CREATION_DATE,NWS_MODIFICATION_DATE";

  /**
   * Constructs SQL query according to criteria specified in input {@link SearchCriteria} object.
   *
   * @param searchCriteria {@link SearchCriteria} object which contains parameters of search.
   * @return String representation of SQL query.
   */
  public static String buildQuery(SearchCriteria searchCriteria) {
    StringBuilder query = new StringBuilder();
    query.append(QUERY);
    if (searchCriteria.getAuthorId() != null) {
      query.append(AUTHOR_ID);
      query.append(searchCriteria.getAuthorId());
    }
    if (searchCriteria.getTagsId() != null) {
      List<Long> tags = searchCriteria.getTagsId();
      if (!tags.isEmpty()) {
        if (searchCriteria.getAuthorId() != null) {
          query.append(AND);
        }
        query.append(TAG_ID);
        query.append(IN);
        query.append(BRACKET_OPEN);
        for (Long id : tags) {
          query.append(id);
          query.append(COMMA);
        }
        query.delete(query.length() - 1, query.length());
        query.append(BRACKET_CLOSE);
        query.append(HAVING_COUNT);
        query.append(TAG_ID);
        query.append(BRACKET_CLOSE);
        query.append(EQUALS);
        query.append(tags.size());
      }
    }
    query.append(GROUP_BY);
    return query.toString();

  }
}
