package by.epam.newsmanagmentapp.dao;

import by.epam.newsmanagmentapp.domain.bean.Comment;
import by.epam.newsmanagmentapp.exceptions.DAOException;

import java.util.List;

/**
 * Describes specific operations which can be performed with {@link Comment} objects in the database.
 */
public interface CommentDAO extends GenericDAO<Comment> {
    /**
     * Delete {@link Comment} records with news identifier specified as parameter.
     *
     * @param id identifier of news.
     * @throws DAOException if there were errors during deleting comment records with specified news identifier.
     */
    void deleteByNewsId(Long id) throws DAOException;

    /**
     * Loads all {@link Comment} records from database with specified news identifier.
     *
     * @param id identifier of news.
     * @return List of ${@link Comment} objects from database.
     * @throws DAOException if there were errors during reading records from database.
     */
    List<Comment> getCommentsByNewsId(Long id) throws DAOException;
}
