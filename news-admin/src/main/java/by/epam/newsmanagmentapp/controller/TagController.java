package by.epam.newsmanagmentapp.controller;

import by.epam.newsmanagmentapp.domain.bean.Tag;
import by.epam.newsmanagmentapp.exceptions.ServiceException;
import by.epam.newsmanagmentapp.service.TagService;
import by.epam.newsmanagmentapp.utils.ErrorsAttributeBuilder;
import by.epam.newsmanagmentapp.validation.TagValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@RestController
public class TagController {

  @Autowired
  private TagService tagService;

  @Autowired
  private TagValidator tagValidator;

  private static final String UPDATE_TAG_MAPPING = "/updatetag";
  private static final String ADD_TAG_MAPPING = "/addtag";
  private static final String TAGS_MAPPING = "/tags";
  private static final String DELETE_TAG_MAPPING = "/deletetag/{id}";
  private static final String TAG_EDIT_MODEL_ATTRIBUTE = "tag";
  private static final String TAG_ADD_MODEL_ATTRIBUTE = "addTag";
  private static final String REDIRECT_PREFIX = "redirect:";
  private static final String ID_VARIABLE = "id";
  private static final String ERRORS_MODEL_ATTRIBUTE = "errors";
  private static final String TAG_ID_ERROR_ATTRIBUTE = "tagIdError";

  @InitBinder({TAG_EDIT_MODEL_ATTRIBUTE, TAG_ADD_MODEL_ATTRIBUTE})
  protected void initBinder(WebDataBinder binder) {
    binder.setValidator(tagValidator);
  }

  @RequestMapping(value = TAGS_MAPPING, method = RequestMethod.GET)
  public ResponseEntity<List<Tag>> getAllTags(final RedirectAttributes redirectAttributes) throws ServiceException {
    List<Tag> tags = tagService.findAll();
    if (tags.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    return new ResponseEntity<>(tags, HttpStatus.OK);
  }

  @RequestMapping(value = ADD_TAG_MAPPING, method = RequestMethod.POST)
  public ModelAndView addTag(@ModelAttribute(TAG_ADD_MODEL_ATTRIBUTE) @Validated Tag tag, BindingResult bindingResult, final RedirectAttributes redirectAttributes) throws ServiceException {
    ModelAndView modelAndView = new ModelAndView(REDIRECT_PREFIX + TAGS_MAPPING);
    if (bindingResult.hasErrors()) {
      redirectAttributes.addFlashAttribute(TAG_ADD_MODEL_ATTRIBUTE, tag);
      redirectAttributes.addFlashAttribute(ERRORS_MODEL_ATTRIBUTE, ErrorsAttributeBuilder.getErrors(bindingResult));
      return modelAndView;
    }
    tagService.insert(tag);
    return modelAndView;
  }

  @RequestMapping(value = DELETE_TAG_MAPPING, method = RequestMethod.POST)
  public ModelAndView deleteTag(@PathVariable(ID_VARIABLE) Long tagId) throws ServiceException {
    tagService.delete(tagId);
    ModelAndView modelAndView = new ModelAndView(REDIRECT_PREFIX + TAGS_MAPPING);
    return modelAndView;
  }

  @RequestMapping(value = UPDATE_TAG_MAPPING, method = RequestMethod.POST)
  public ModelAndView updateTag(@ModelAttribute(TAG_EDIT_MODEL_ATTRIBUTE) @Validated Tag tag, BindingResult bindingResult, final RedirectAttributes redirectAttributes) throws ServiceException {
    ModelAndView modelAndView = new ModelAndView(REDIRECT_PREFIX + TAGS_MAPPING);
    if (bindingResult.hasErrors()) {
      redirectAttributes.addFlashAttribute(ERRORS_MODEL_ATTRIBUTE, ErrorsAttributeBuilder.getErrors(bindingResult));
      redirectAttributes.addFlashAttribute(TAG_ID_ERROR_ATTRIBUTE, tag.getTagId());
      return modelAndView;
    }
    tagService.update(tag);
    return modelAndView;
  }

}
