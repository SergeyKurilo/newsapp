package by.epam.newsmanagmentapp.dao.impl;

import by.epam.newsmanagmentapp.dao.NewsDAO;
import by.epam.newsmanagmentapp.domain.to.NextAndPreviousNewsIdTO;
import by.epam.newsmanagmentapp.dao.util.DBUtil;
import by.epam.newsmanagmentapp.dao.util.QueryBuilder;
import by.epam.newsmanagmentapp.domain.bean.News;
import by.epam.newsmanagmentapp.domain.bean.SearchCriteria;
import by.epam.newsmanagmentapp.exceptions.DAOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of {@link NewsDAO} using JDBC
 */
@Component
public class NewsDAOImpl implements NewsDAO {

    @Autowired
    DataSource dataSource;

    private static final String NEWS_ID = "NWS_ID";
    private static final String NEWS_TITLE = "NWS_TITLE";
    private static final String NEWS_SHORT_TEXT = "NWS_SHORT_TEXT";
    private static final String NEWS_FULL_TEXT = "NWS_FULL_TEXT";
    private static final String NEWS_CREATION_DATE = "NWS_CREATION_DATE";
    private static final String NEWS_MODIFICATION_DATE = "NWS_MODIFICATION_DATE";

    private static final String SQL_NEWS_PAGE_WITH_CRITERIA_LEFT_PART = "SELECT NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE " +
            "FROM (SELECT ROWNUM rn, N.* FROM (" +
            "SELECT NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE, COUNT(CMT_ID) AS c FROM (";

    private static final String SQL_NEWS_PAGE_WITH_CRITERIA_RIGHT_PART = ") LEFT OUTER JOIN COMMENTS ON COMMENTS.CMT_NWS_ID = NWS_ID " +
            "GROUP BY NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE " +
            "ORDER BY c DESC, NWS_MODIFICATION_DATE DESC) N) WHERE rn>=? AND rn<=?";

    private static final String SQL_PREVIOUS_NEXT_ID_SEARCH_CRITERIA_LEFT = "Select prev_news, next_news from (SELECT nws_id,LAG(nws_id,1) OVER( ORDER BY count DESC, nws_modification_date DESC) as prev_news, " +
            "LEAD(nws_id,1) OVER ( ORDER BY count DESC, nws_modification_date DESC) as next_news " +
            "FROM (SELECT nws_id, nws_title, nws_short_text, nws_full_text, nws_creation_date, nws_modification_date,COUNT(cmt_id) " +
            "AS count FROM (";
    private static final String SQL_PREVIOUS_NEXT_ID_SEARCH_CRITERIA_RIGHT = " ) LEFT OUTER JOIN COMMENTS ON COMMENTS.cmt_nws_id = NWS_ID GROUP BY NWS_ID,NWS_TITLE,NWS_SHORT_TEXT,NWS_FULL_TEXT,NWS_CREATION_DATE,NWS_MODIFICATION_DATE )) WHERE nws_id = ?";

    private static final String SQL_SELECT_FROM_NEWS = "SELECT NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE, COUNT(CMT_ID) AS c FROM NEWS " +
            "FULL OUTER JOIN COMMENTS ON COMMENTS.CMT_NWS_ID = NEWS.NWS_ID " +
            "GROUP BY NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE " +
            "ORDER BY c DESC, NWS_MODIFICATION_DATE DESC";

    private static final String SQL_INSERT_NEWS = "INSERT INTO NEWS(NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE) VALUES (?, ?, ?, ?, ?)";

    private static final String SQL_DELETE_NEWS_BY_NEWS_ID = "DELETE FROM NEWS WHERE NWS_ID=?";

    private static final String SQL_SELECT_NEWS_BY_NEWS_ID = "SELECT NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE FROM NEWS WHERE NWS_ID=?";

    private static final String SQL_UPDATE_NEWS = "UPDATE NEWS SET NWS_TITLE=?, NWS_SHORT_TEXT=?, NWS_FULL_TEXT=?,  NWS_MODIFICATION_DATE=? WHERE NWS_ID = ?";

    private static final String SQL_CREATE_NEWS_AUTHOR_LINK = "INSERT INTO NEWS_AUTHORS(NAT_NEWS_ID, NAT_ATR_ID) VALUES (?, ?)";

    private static final String SQL_CREATE_NEWS_TAG_LINK = "INSERT INTO NEWS_TAGS(NWS_NEWS_ID, NWS_TAG_ID) VALUES (?, ?)";

    private static final String SQL_DELETE_NEWS_AUTHOR_LINK = "DELETE FROM NEWS_AUTHORS WHERE NAT_NEWS_ID=?";

    private static final String SQL_DELETE_NEWS_TAG_LINK = "DELETE FROM NEWS_TAGS WHERE NWS_NEWS_ID=?";

    private static final String SQL_DELETE_NEWS_TAG_LINK_BY_TAG = "DELETE FROM NEWS_TAGS WHERE NWS_TAG_ID=?";

    private static final String SQL_COUNT_NUMBER_OF_NEWS = "SELECT COUNT(NWS_ID) FROM NEWS";

    private static final String SQL_LEFT_PART_SEARCH_CRITERIA = "SELECT COUNT(NWS_ID) FROM (";

    private static final String SQL_RIGHT_PART_SEARCH_CRITERIA = ")";

    private static final String SQL_SELECT_FROM_NEWS_IN = "SELECT nws_id, nws_title, nws_short_text, nws_full_text, nws_creation_date, nws_modification_date " +
            "FROM (SELECT T.*,ROWNUM RN FROM (SELECT nws_id, nws_title, nws_short_text, nws_full_text, nws_creation_date, nws_modification_date,COUNT(cmt_id) " +
            "AS count FROM NEWS FULL OUTER JOIN COMMENTS ON COMMENTS.cmt_nws_id = nws_id " +
            "GROUP BY nws_id, nws_title, nws_short_text, nws_full_text, nws_creation_date,nws_modification_date " +
            "ORDER BY count DESC, nws_modification_date DESC) T) WHERE RN >= ? AND RN <= ?";

    private static final String SQL_SELECT_PREV_AND_NEXT_NEWS_ID = "SELECT PREV_NEWS, NEXT_NEWS FROM (SELECT LAG(NWS_ID, 1) " +
            "OVER (ORDER BY c DESC, NWS_MODIFICATION_DATE DESC) PREV_NEWS, NWS_ID CURRENT_ID, LEAD(NWS_ID, 1) " +
            "OVER (ORDER BY c DESC, NWS_MODIFICATION_DATE DESC) NEXT_NEWS, c, NWS_MODIFICATION_DATE FROM (SELECT NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, " +
            "NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE, COUNT(CMT_ID) AS c FROM NEWS FULL OUTER JOIN COMMENTS ON COMMENTS.CMT_NWS_ID = NEWS.NWS_ID " +
            "GROUP BY NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE " +
            "ORDER BY c DESC, NWS_MODIFICATION_DATE DESC)) WHERE CURRENT_ID=?";

    private NewsDAOImpl() {
    }

    /**
     * Implementation of {@link  NewsDAO#findAll()} method using simple JDBC.
     */
    @Override
    public List<News> findAll() throws DAOException {
        List<News> newses = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_FROM_NEWS);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                News news = new News();
                news.setNewsId(rs.getLong(NEWS_ID));
                news.setTitle(rs.getString(NEWS_TITLE));
                news.setShortText(rs.getString(NEWS_SHORT_TEXT));
                news.setFullText(rs.getString(NEWS_FULL_TEXT));
                news.setCreationDate(rs.getTimestamp(NEWS_CREATION_DATE));
                news.setModificationDate(rs.getDate(NEWS_MODIFICATION_DATE));
                newses.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
        return newses;
    }

    /**
     * Implementation of {@link NewsDAO#insert(Object)} method using simple JDBC.
     */
    @Override
    public Long insert(News news) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        Long id = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_INSERT_NEWS, new String[]{NEWS_ID});
            preparedStatement.setString(1, news.getTitle());
            preparedStatement.setString(2, news.getShortText());
            preparedStatement.setString(3, news.getFullText());
            preparedStatement.setTimestamp(4, news.getCreationDate());
            preparedStatement.setDate(5, news.getModificationDate());
            preparedStatement.executeUpdate();

            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    id = generatedKeys.getLong(1);
                } else {
                    throw new DAOException("Creating news failed, no ID obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement);
        }
        return id;
    }

    /**
     * Implementation of {@link NewsDAO#delete(Long)} method using simple JDBC.
     */
    @Override
    public void delete(Long id) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_BY_NEWS_ID);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement);
        }
    }

    /**
     * Implementation of {@link NewsDAO#getByKey(Long)} method using simple JDBC.
     */
    @Override
    public News getByKey(Long id) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        News news = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_NEWS_BY_NEWS_ID);
            preparedStatement.setLong(1, id);
            rs = preparedStatement.executeQuery();
            news = new News();
            news.setNewsId(rs.getLong(NEWS_ID));
            news.setTitle(rs.getString(NEWS_TITLE));
            news.setShortText(rs.getString(NEWS_SHORT_TEXT));
            news.setFullText(rs.getString(NEWS_FULL_TEXT));
            news.setCreationDate(rs.getTimestamp(NEWS_CREATION_DATE));
            news.setModificationDate(rs.getDate(NEWS_MODIFICATION_DATE));
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
        return news;
    }

    /**
     * Implementation of {@link NewsDAO#update(Object)} method using simple JDBC.
     */
    @Override
    public void update(News news) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_UPDATE_NEWS);
            preparedStatement.setString(1, news.getTitle());
            preparedStatement.setString(2, news.getShortText());
            preparedStatement.setString(3, news.getFullText());
            preparedStatement.setDate(4, news.getModificationDate());
            preparedStatement.setLong(5, news.getNewsId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement);
        }
    }

    /**
     * Implementation of {@link NewsDAO#findBySearchCriteria(SearchCriteria)} method using simple JDBC.
     */
    @Override
    public List<News> findBySearchCriteria(SearchCriteria searchCriteria) throws DAOException {
        List<News> newses = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        String query = QueryBuilder.buildQuery(searchCriteria);
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(query);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                News news = new News();
                news.setNewsId(rs.getLong(NEWS_ID));
                news.setTitle(rs.getString(NEWS_TITLE));
                news.setShortText(rs.getString(NEWS_SHORT_TEXT));
                news.setFullText(rs.getString(NEWS_FULL_TEXT));
                news.setCreationDate(rs.getTimestamp(NEWS_CREATION_DATE));
                news.setModificationDate(rs.getDate(NEWS_MODIFICATION_DATE));
                newses.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
        return newses;
    }

    /**
     * Implementation of {@link NewsDAO#createNewsAuthorLink(Long, Long)} method using simple JDBC.
     */
    @Override
    public void createNewsAuthorLink(Long newsId, Long authorId) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_CREATE_NEWS_AUTHOR_LINK);
            preparedStatement.setLong(1, newsId);
            preparedStatement.setLong(2, authorId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement);
        }
    }

    /**
     * Implementation of {@link NewsDAO#deleteNewsAuthorLink(Long)} method using simple JDBC.
     */
    @Override
    public void deleteNewsAuthorLink(Long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_AUTHOR_LINK);
            preparedStatement.setLong(1, newsId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement);
        }
    }

    /**
     * Implementation of {@link NewsDAO#createNewsTagsLinks(Long, Long...)} method using simple JDBC.
     */
    @Override
    public void createNewsTagsLinks(Long newsId, Long... tags) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_CREATE_NEWS_TAG_LINK);
            for (Long tagId : tags) {
                preparedStatement.setLong(1, newsId);
                preparedStatement.setLong(2, tagId);
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement);
        }
    }

    /**
     * Implementation of {@link NewsDAO#deleteNewsTagLink(Long)} method using simple JDBC.
     */
    @Override
    public void deleteNewsTagLink(Long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_TAG_LINK);
            preparedStatement.setLong(1, newsId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement);
        }
    }

    @Override
    public void deleteNewsTagLinkByTag(Long tagId) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_TAG_LINK_BY_TAG);
            preparedStatement.setLong(1, tagId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement);
        }
    }

    /**
     * Implementation of {@link NewsDAO#getNumberOfNews()} method using simple JDBC.
     */
    @Override
    public Long getNumberOfNews() throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        Long number = 0L;
        ResultSet rs;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_COUNT_NUMBER_OF_NEWS);
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                number = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement);
        }
        return number;
    }

    @Override
    public Long getNumberOfNewsWithSearchCriteria(SearchCriteria searchCriteria) throws DAOException {
        String query = SQL_LEFT_PART_SEARCH_CRITERIA + QueryBuilder.buildQuery(searchCriteria) + SQL_RIGHT_PART_SEARCH_CRITERIA;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        Long number = 0L;
        ResultSet rs;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(query);
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                number = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement);
        }
        return number;
    }


    @Override
    public List<News> loadPage(Long first, Long last) throws DAOException {
        List<News> newses = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_FROM_NEWS_IN);
            preparedStatement.setLong(1, first);
            preparedStatement.setLong(2, last);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                News news = new News();
                news.setNewsId(rs.getLong(NEWS_ID));
                news.setTitle(rs.getString(NEWS_TITLE));
                news.setShortText(rs.getString(NEWS_SHORT_TEXT));
                news.setFullText(rs.getString(NEWS_FULL_TEXT));
                news.setCreationDate(rs.getTimestamp(NEWS_CREATION_DATE));
                news.setModificationDate(rs.getDate(NEWS_MODIFICATION_DATE));
                newses.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
        return newses;
    }

    @Override
    public List<News> loadPageWithSearchCriteria(SearchCriteria searchCriteria, Long first, Long last) throws DAOException {
        List<News> newses = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        String query = SQL_NEWS_PAGE_WITH_CRITERIA_LEFT_PART + QueryBuilder.buildQuery(searchCriteria) + SQL_NEWS_PAGE_WITH_CRITERIA_RIGHT_PART;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, first);
            preparedStatement.setLong(2, last);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                News news = new News();
                news.setNewsId(rs.getLong(NEWS_ID));
                news.setTitle(rs.getString(NEWS_TITLE));
                news.setShortText(rs.getString(NEWS_SHORT_TEXT));
                news.setFullText(rs.getString(NEWS_FULL_TEXT));
                news.setCreationDate(rs.getTimestamp(NEWS_CREATION_DATE));
                news.setModificationDate(rs.getDate(NEWS_MODIFICATION_DATE));
                newses.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
        return newses;
    }

    @Override
    public NextAndPreviousNewsIdTO getNextAndPreviousNewsId(Long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        NextAndPreviousNewsIdTO nextAndPreviousNewsIdTO = new NextAndPreviousNewsIdTO();
        try {
            buildNextAndPreviousNewsTO(preparedStatement, SQL_SELECT_PREV_AND_NEXT_NEWS_ID, connection, rs, nextAndPreviousNewsIdTO, newsId);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
        return nextAndPreviousNewsIdTO;
    }

    @Override
    public NextAndPreviousNewsIdTO getNextAndPreviousNewsIdWithSearchCriteria(SearchCriteria searchCriteria, Long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        NextAndPreviousNewsIdTO nextAndPreviousNewsIdTO = new NextAndPreviousNewsIdTO();
        String query = SQL_PREVIOUS_NEXT_ID_SEARCH_CRITERIA_LEFT + QueryBuilder.buildQuery(searchCriteria) + SQL_PREVIOUS_NEXT_ID_SEARCH_CRITERIA_RIGHT;
        try {
            buildNextAndPreviousNewsTO(preparedStatement, query, connection, rs, nextAndPreviousNewsIdTO, newsId);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
        return nextAndPreviousNewsIdTO;
    }

    private void buildNextAndPreviousNewsTO(PreparedStatement preparedStatement, String query, Connection connection, ResultSet rs, NextAndPreviousNewsIdTO nextAndPreviousNewsIdTO, Long newsId) throws SQLException {
        connection = DataSourceUtils.doGetConnection(dataSource);
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setLong(1, newsId);
        rs = preparedStatement.executeQuery();
        if (rs.next()) {
            nextAndPreviousNewsIdTO.setPreviousId(rs.getLong(1));
            nextAndPreviousNewsIdTO.setNextId(rs.getLong(2));
        }
    }

}
