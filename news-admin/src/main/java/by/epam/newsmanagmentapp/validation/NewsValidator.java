package by.epam.newsmanagmentapp.validation;

import by.epam.newsmanagmentapp.domain.to.AddNewsTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class NewsValidator implements Validator {
  @Override
  public boolean supports(Class<?> aClass) {
    return AddNewsTO.class.equals(aClass);
  }

  @Override
  public void validate(Object o, Errors errors) {

    AddNewsTO addNewsTO = (AddNewsTO) o;

    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "news.title", "NotEmpty.newsForm.news.title");
    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "news.shortText", "NotEmpty.newsForm.news.shortText");
    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "news.fullText", "NotEmpty.newsForm.news.fullText");

    if (addNewsTO.getTags() == null || addNewsTO.getTags().length < 1) {
      errors.rejectValue("tags", "Valid.newsForm.tags");
    }

    if (addNewsTO.getAuthorId() == null) {
      errors.rejectValue("authorId", "Valid.newsForm.authorId");
    }
  }
}
