<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<spring:url value="authorName" var="authorName"/>
<spring:url value="${pageContext.request.contextPath}/updateauthor" var="updateAuthor"/>
<spring:url value="${pageContext.request.contextPath}/expireauthor" var="expireAuthor"/>

<div class="col-sm-10">

  <c:forEach items="${authors}" var="item">
    <form:form id="form${item.authorId}" class="form-inline" role="form" method="post" modelAttribute="editAuthor">
      <label>Author </label>
      <div class="form-group">
        <form:input path="authorId" type="hidden" value="${item.authorId}"/>
        <form:input id="author${item.authorId}" class="form-control" value="${item.authorName}"
                    path="${authorName}" disabled="true"/>
        <c:if test="${authorIdError eq item.authorId}">
          <c:forEach items="${errors}" var="error">
            <p>${error}</p>
          </c:forEach>
        </c:if>
      </div>
      <button id="update${item.authorId}" class="btn btn-primary hidden" formaction="${updateAuthor}">Update</button>
      <button id="expired${item.authorId}" class="btn btn-danger hidden" formaction="${expireAuthor}">Expire</button>
      <button id="cancel${item.authorId}" class="btn btn-danger hidden" type="button" onclick="hideAuthorButtons(this)">Cancel</button>
      <button id="edit${item.authorId}" class="btn btn-primary" type="button" onclick="showAuthorButtons(this)">Edit</button>
    </form:form>
  </c:forEach>
  <form:form class="form-inline" role="form" action="${pageContext.request.contextPath}/addauthor" method="post"
             modelAttribute="addAuthor">
    <div class="form-group">
      <label>Add author</label>
      <form:input class="form-control" path="authorName"/>
      <c:forEach items="${errors}" var="error">
        ${error}
      </c:forEach>
      <button id="edit-button" class="btn btn-primary">Add author</button>
    </div>
  </form:form>
</div>