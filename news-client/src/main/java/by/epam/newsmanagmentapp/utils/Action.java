package by.epam.newsmanagmentapp.utils;

public enum Action {
    REDIRECT, FORWARD
}
