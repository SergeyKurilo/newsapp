package by.epam.newsmanagmentapp.service.impl;

import by.epam.newsmanagmentapp.dao.RoleDAO;
import by.epam.newsmanagmentapp.domain.bean.Role;
import by.epam.newsmanagmentapp.exceptions.DAOException;
import by.epam.newsmanagmentapp.exceptions.ServiceException;
import by.epam.newsmanagmentapp.service.RoleService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Implementation of {@link RoleService}.
 */
@Component
@Transactional
public class RoleServiceImpl implements RoleService {

    private static final Logger LOGGER = LogManager.getLogger(RoleServiceImpl.class);

    @Autowired
    private RoleDAO roleDAO;

    /**
     * Implementation of {@link RoleService#findAll()} method.
     */
    @Override
    public List<Role> findAll() throws ServiceException {
        try {
            return roleDAO.findAll();
        } catch (DAOException e) {
            LOGGER.error("Load all roles service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link RoleService#insert(Object)} method.
     */
    @Override
    public Long insert(Role role) throws ServiceException {
        try {
            return roleDAO.insert(role);
        } catch (DAOException e) {
            LOGGER.error("Create role node service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link RoleService#delete(Long)} method.
     */
    @Override
    public void delete(Long id) throws ServiceException {
        try {
            roleDAO.delete(id);
        } catch (DAOException e) {
            LOGGER.error("Removing role service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link RoleService#getByKey(Long)} method.
     */
    @Override
    public Role getByKey(Long id) throws ServiceException {
        try {
            return roleDAO.getByKey(id);
        } catch (DAOException e) {
            LOGGER.error("Find role node by ID service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link RoleService#update(Object)} method.
     */
    @Override
    public void update(Role role) throws ServiceException {
        try {
            roleDAO.update(role);
        } catch (DAOException e) {
            LOGGER.error("Update role node service error");
            throw new ServiceException(e);
        }
    }
}

