
INSERT INTO roles VALUES (1, 'ROLE_ADMIN');
INSERT INTO roles VALUES (2, 'ROLE_USER');

INSERT INTO USERS(USR_ROLE_ID, USR_NAME, USR_LOGIN, USR_PASSWORD) VALUES (1, 'Siarhei Kuryla', 'SKuryla', '3d2172418ce305c7d16d4b05597c6a59');
INSERT INTO USERS(USR_ROLE_ID, USR_NAME, USR_LOGIN, USR_PASSWORD) VALUES (1, 'Nikolai Yatsyna', 'NikY', '12345');
INSERT INTO USERS(USR_ROLE_ID, USR_NAME, USR_LOGIN, USR_PASSWORD) VALUES (2, 'Pavel Grinchenko', 'PavelGO', '54321');
INSERT INTO USERS(USR_ROLE_ID, USR_NAME, USR_LOGIN, USR_PASSWORD) VALUES (2, 'Yana Volchok', 'YaVo', '11111');
INSERT INTO USERS(USR_ROLE_ID, USR_NAME, USR_LOGIN, USR_PASSWORD) VALUES (1, 'Vlad Zhuk', 'Zhuk', '12345');


INSERT INTO NEWS(NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE) VALUES
  (1,
   'Deftones',
   'Deftones Talk New Album, Band Tension',
   'When the recorded track begins again with layers of guitar feedback and muscular beats,
   the band doesnt just walk through it. Abe Cunningham is really pounding those drums,
   and Stephen Carpenter is bent over his guitar, carefully playing every part, though none of them are even plugged in.
   Singer Chino Moreno is pacing, leaping and whipping his mic chord anxiously, overcome with the sensations of a cut he describes as perpetual motion,
   like the whole song is tumbling over itself.
   Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
   incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
   laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
   velit esse cillum dolore eu fugiat nulla pariatur.',
   current_timestamp,
   to_date('20160405','YYYYMMDD'));

INSERT INTO NEWS(NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE) VALUES
  (2,
   'Miami Open',
   'Victoria Azarenka beats Svetlana Kuznetsova in final',
   'Victoria Azarenka secured back-to-back WTA titles with a straight-set victory over Svetlana Kuznetsova in the Miami Open final.
   The Belarusian won 6-3 6-2 in 77 minutes to add a third Miami title to her victories in 2009 and 2011.
   The 26-year-old, who won in Indian Wells two week ago, will return to the worlds top five next week.
   Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
   incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
   laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
   velit esse cillum dolore eu fugiat nulla pariatur.',
   current_timestamp,
   to_date('20160405','YYYYMMDD'));

INSERT INTO NEWS(NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE) VALUES
  (3,
   'Electric skateboards',
   'The Blink Board is the lightest and cheapest electric skateboard yet and its a lot of fun to ride',
   'Electric skateboards are an exciting new technology that is currently being hampered by cost.
    Few companies offer boards below the $1,000 mark, which is five to ten times as expensive as a traditional skateboard or longboard.
    Price is such an issue with electric skateboards right now that I was recently served a banner ad that prompted me to sign up for a monthly financing program just to buy one.
   Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
   incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
   laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
   velit esse cillum dolore eu fugiat nulla pariatur.',
   current_timestamp,
   to_date('20160405','YYYYMMDD'));

INSERT INTO NEWS(NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE) VALUES
  (4,
   'Goo Goo Dolls',
   'Goo Goo Dolls announce 11th album and reveal new song',
   'Following their announcement of an extensive North American tour for 2016, the multi-platinum,
   Grammy-nominated Goo Goo Dolls have unveiled details of their upcoming new album ‘Boxes’, via Warner Bros. Records.
   The CD and digital will arrive on May 6th, with the vinyl version following on June 10th.
    The first song from the album Over and Over is available now as an instant grat when you preorder Boxes on iTunes here.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
   incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
   laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
   velit esse cillum dolore eu fugiat nulla pariatur.',
   current_timestamp,
   to_date('20160405','YYYYMMDD'));

INSERT INTO NEWS(NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE) VALUES
  (5,
   'Algorithms',
   'New algorithm cracks graph problem. A new algorithm efficiently solves the graph isomorphism problem, computer scientist László Babai announced November 10.',
   'A new algorithm efficiently solves the graph isomorphism problem, computer scientist László Babai announced November 10 at a Combinatorics and Theoretical Computer Science
   seminar at the University of Chicago. The problem requires determining whether two separate sets of interconnected points, known as graphs, are linked in the same way
   even if the graphs look very different. In practice, existing algorithms can do the job in reasonable time,
    but it was possible that extremely complex graphs would make the problem intractable.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
   incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
   laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
   velit esse cillum dolore eu fugiat nulla pariatur.',
   current_timestamp,
   to_date('20160405','YYYYMMDD'));



INSERT INTO NEWS(NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE) VALUES
  (6,
   'French nuclear workers to join strike',
   'Workers at French nuclear power stations are due to join a strike over labour reforms that has already paralysed oil refineries and triggered fuel shortages.',
   'Nuclear power provides about 75% of the country''s electricity.
   Grid operator RTE said French nuclear power capacity was being cut by at least four gigawatts - equivalent to 6% of the country''s total production capacity - Reuters news agency reports.
   Clashes broke out at one refinery on Tuesday when police broke up a blockade at Fos-sur-Mer in Marseille.
   Workers at a large oil terminal in the port of Le Havre were due to go on strike on Thursday to block imports.
   Industrial action also spread to France''s railways on Wednesday, with a strike by train drivers cutting some high-speed TGV services as well as regional and commuter trains. More transport disruption was expected on Thursday.
   The CGT has also called for protest rallies in cities across France.
   The government provoked union outrage when it resorted to a constitutional device to force its watered-down labour reforms through parliament without a vote.
   The government says the reforms, which make it easier for companies to hire and fire staff, are needed to bring down unemployment.',
   current_timestamp,
   to_date('20160405','YYYYMMDD'));

INSERT INTO NEWS(NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE) VALUES
  (7,
   'Johnny Depp and Amber Heard in marriage split',
   'Film star Johnny Depp and his actress wife Amber Heard are to divorce, US court documents reveal.',
   'Film star Johnny Depp and his actress wife Amber Heard are to divorce, US court documents have revealed.
   Heard, 30, filed for divorce in Los Angeles Superior Court citing irreconcilable differences.
   They married 15 months ago and have no children.
   They have recently been embroiled in a legal case in Australia after Heard took two dogs into the country illegally. She pleaded guilty in April to a charge of falsifying documents.
   Depp, 52, and Heard met while co-starring in the 2011 film The Rum Diary.
   In the court papers, Texan-born Heard listed the separation date as Sunday.
   Celebrity news website TMZ reported that Depp had asked a judge to reject Heard''s claim for spousal support.
   The split comes days after the death of Depp''s mother. His latest film, Alice Through the Looking Glass, is due to be released on Friday.',
   current_timestamp,
   to_date('20160405','YYYYMMDD'));

INSERT INTO NEWS(NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE) VALUES
  (8,
   'Oil hits $50 a barrel for first time this year',
   'Oil continues its recovery,
    with prices rising above $50 a barrel for the first time in 2016 as a result of supply disruptions and increased global demand.',
   'The benchmark Brent crude price hit $50.07 a barrel in Asian trade.
 The rise followed US data on Thursday showing that oil inventories had fallen, largely due to supply disruptions following fires in Canada.
 Brent crude has now risen 80% since it hit 13-year lows of below $28 a barrel at the start of the year.
 US crude oil inventories fell by 4.2 million barrels to 537.1 million barrels in the week to May 20, according to US Department of Energy data.
 Canada is the biggest supplier to the US and wildfires in the western provinces have knocked out about a million barrels a day.
 Talks in recent months between Opec and Russia about freezing oil production had already helped prices recover.
 Short-term disruptions to oil supplies have also lifted the price, as they have offset higher production from Iran and Saudi Arabia.
 As well as the disruption to key oil production facilities in Canada, attacks by militant groups continue to restrict oil pipelines in Nigeria.
 Demand has also been better than expected from major economies such as China, India and Russia.',
   current_timestamp,
   to_date('20160405','YYYYMMDD'));

INSERT INTO NEWS(NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE) VALUES
  (9,
   'G7 Japan: World leaders visit Shinto religion''s holiest shrine',
   'Japanese PM Shinzo Abe has taken world leaders to the Shinto religion''s holiest site, as the Group of Seven (G7) summit begins in the country.',
   'Mr Abe said the visit was so that they could "understand the spirituality of Japanese people".
   The two-day G7 meeting in Ise-Shima brings together industrialised nations.
   On Friday, US President Barack Obama will visit Hiroshima - the site of the first atomic bomb - the first sitting US president to do so.
   The visit to the shrine is controversial because critics say Mr Abe is catering to his conservative supporters who want to revive traditional values.
   Top of the agenda for the G7 nations - the US, Canada, Britain, Italy, Germany, France and Japan - will be concerns over the health of the global economy.
   Europe''s refugee crisis will also feature prominently at the meeting. European Council President Donald Tusk said on Thursday he would ask the G7''s support for more global aid for refugees.',
   current_timestamp,
   to_date('20160405','YYYYMMDD'));

INSERT INTO NEWS(NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE) VALUES
  (10,
   'Indonesia approves death penalty for child rapists',
   'Indonesia has toughened its punishments for child rapists to include the death penalty and chemical castration.',
   'It follows outrage over several recent violent crimes, including the gang-rape and murder of a 14-year-old girl.
   President Joko Widodo said the regulation was "intended to overcome the crisis caused by sexual violence against children".
   Previously, the maximum sentence for rape, of either an adult or a child, was 14 years in jail.
   People jailed for sexual offences against children may also now be made to wear electronic monitoring devices after their release.
   The gang rape of 14-year-old Yuyun on her way home from school and the rape and brutal murder of a 18-year-old factory worker this month have sparked national outrage.
   In polls and on social media there is widespread support for tougher punishments including castration and the death penalty for perpetrators, particularly when children are the victims.',
   current_timestamp,
   to_date('20160405','YYYYMMDD'));

INSERT INTO NEWS(NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE) VALUES
  (11,
   'US nuclear force ''''still uses floppy disks',
   'The US nuclear weapons force still uses a 1970s-era computer system and floppy disks, a government report has revealed.',
   'The Government Accountability Office said the Pentagon was one of several departments where "legacy systems" urgently needed to be replaced.
   The report said taxpayers spent $61bn (£41bn) a year on maintaining ageing technologies.
   It said that was three times more than the investment on modern IT systems.
   The report said that the Department of Defence systems that co-ordinated intercontinental ballistic missiles, nuclear bombers and tanker support aircraft "runs on an IBM Series-1 Computer - a 1970s computing system - and uses eight-inch floppy disks".
   "This system remains in use because, in short, it still works," Pentagon spokeswoman Lt Col Valerie Henderson told the AFP news agency.
   "However, to address obsolescence concerns, the floppy drives are scheduled to be replaced with secure digital devices by the end of 2017."
   She added: "Modernisation across the entire Nuclear Command, Control, and Communications enterprise remains ongoing."',
   current_timestamp,
   to_date('20160405','YYYYMMDD'));


INSERT INTO NEWS(NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE) VALUES
  (12,
   'Jose Mourinho: Image rights negotiations hold up Manchester United deal',
   'Image rights negotiations are delaying Jose Mourinho''s appointment as Manchester United manager.',
   'It has emerged that Chelsea still own his name as a trademark and could demand a six-figure sum from United before any deal is concluded.
   However, the issue will not scupper his appointment at Old Trafford.
   Mourinho is expected to be named as Louis van Gaal''s replacement after the Dutchman, 64, was sacked on Monday despite winning the FA Cup.
   The deal to bring Mourinho to Old Trafford are largely complete, but discussions between his agent, Jorge Mendes, and United officials will now enter a third day.
   On Wednesday, former Chelsea and Real Madrid boss Mourinho, 53, told waiting reporters at his home in London that he was going to Portugal.
   Chelsea registered ''Jose Mourinho'' and his signature as a European trademark in 2005, which means they can use it to sell merchandise such as toiletries, technology, clothing and jewellery.
   Sports lawyer Carol Couse told BBC Sport it was "really unusual" for an individual not to own the trademark to their own name.',
   current_timestamp,
   to_date('20160405','YYYYMMDD'));

INSERT INTO NEWS(NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE) VALUES
  (13,
   'Lewis Hamilton: ''No tension'' with Nico Rosberg after Spanish GP crash',
   'Triple world champion Lewis Hamilton says he feels no tension towards Mercedes team-mate Nico Rosberg despite their crash in Spain.',
   '"It''s fine," said the Briton. "In the past, there would have been tension, but there was just pure respect."
   Rosberg leads the championship with 100 points after winning four of the first five races, with Hamilton 43 points adrift of the German in third place.
   The sixth race of the season takes place in Monaco on Sunday.
   Hamilton and Rosberg crashed out of the last race in Barcelona after colliding with each other on the first lap.
   They have clashed several times in their Formula 1 careers, but the reigning world champion insists there is no lingering bad feeling on this occasion.
   Asked why it was different now, he said: "It is always good to discuss things. As a team we did in Barcelona and then at the factory individually.
   "Then Nico and I spoke just now. I didn''t feel we had to, because there is no issue. We just move on. There is nothing you can do about the past.',
   current_timestamp,
   to_date('20160405','YYYYMMDD'));

INSERT INTO NEWS(NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE) VALUES
  (14,
   'New York shooting: Gunfire at Irving Plaza leaves one person dead',
   'One person has been killed and three others wounded in a shooting at a music venue in New York.',
   'Police said the shooting happened at Irving Plaza, near Union Square, Manhattan, where hip-hop artist TI was due to perform on Wednesday evening.
   No-one has been arrested and the motive for the shooting is unclear.
   Witnesses reported panic inside the concert hall when the shots rang out. Police have sealed off the area around the venue.
   The victims were three men, one of whom died, and a woman, the New York Times reported.
   TI, whose real name is Clifford Joseph Harris Jr, was not on stage at the time. His representatives have not yet commented on the incident.',
   current_timestamp,
   to_date('20160405','YYYYMMDD'));

INSERT INTO NEWS(NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE) VALUES
  (15,
   'The Democrats'' election nightmare',
   'Some Democrats have a nightmare that takes them back to Florida 16 years ago, and the time of the ''hanging chads''.',
   'It was the presidential election decided in that state by 537 votes after weeks of counting, amid arguments over the ragged fragments of ballots not punched free in the voting machines. Those pesky chads.
   The villain of the nightmare is the old consumer and green crusader Ralph Nader.
   He persisted in his third party campaign through to November, impervious to Democrat accusations of selfish egocentricity, and got nearly 100,000 votes in Florida.
   With less than one per cent of votes for Nader, cast overwhelmingly by liberal-left voters, Al Gore would have won the state for the Democrats, and vote in the state-by-state electoral college.
   President George W Bush would never have been.
   The figure who hovers in this dream as a white-haired ghost is of course, Bernie Sanders.
   Might he be the spoiler for Hillary Clinton which gives Donald Trump the White House?
   The fear of senior Democrats is not that he makes a Nader-style independent run - it would make no sense at all - but simply that he poisons the well, and has the same effect in the end.
   He''s not giving up, although mathematically his chance of the nomination has gone.
   He tells his huge rallies that the only way to defeat Trump is to defeat Clinton, and many of his followers believe him. I walked with a few hundred of them through San Diego.',
   current_timestamp,
   to_date('20160405','YYYYMMDD'));


INSERT INTO NEWS(NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE) VALUES
  (16,
   'Xiaomi Mi Drone poses price challenge to DJI',
   'Chinese smartphone maker Xiaomi has announced its first drone, pricing it significantly lower than a comparable model by the market leader DJI.',
   'The Mi Drone can stay airborne for nearly half an hour and will be sold with a choice of stabilised cameras.
   The move gives Xiaomi the chance to target a fast-growing market, at a time when it has failed to meet its own sales targets.
   One expert said the firm now had to win over potential buyers'' trust.
   "The feature set between the Mi Drone and DJI''s Phantom 3 is almost identical - they can both be made to return home and circle around a point of interest - but Xiaomi''s product is so competitively priced you have to wonder if it can make much profit," Engadget''s Chinese editor-in-chief Richard Lai told the BBC.
   "So, the new drone will probably appeal to beginners. But experienced fliers want reliability and a brand with experience, and DJI has spent years refining its technology.
   "It will take Xiaomi some time to prove itself as we still don''t know how reliable its drones are, the quality of its video footage or how well its software will work."
   Xiaomi plans to sell the Mi Drone with a 1080p high-definition camera with a 1km (0.6 miles) range for 2,499 yuan (£260), and a version with a higher-resolution 4K camera and 2km range for 2,999 yuan (£310).
   By contrast, DJI - another Chinese firm - sells the Phantom 3 4K with a range of 1.2km for 4,999 yuan (£520).
   Xiaomi vice president Hugo Barra said that drones were "typically a product for rich people", but that his company wanted to sell them to a wider audience.',
   current_timestamp,
   to_date('20160405','YYYYMMDD'));



INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (1, 'The most underrated band around right now.', current_timestamp);
INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (1, 'Im glad that the bands recent drama was just overblown press junk.', current_timestamp);
INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (1, 'The music they make heavy and melodic is like the way life beats you down. ', current_timestamp);
INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (1, 'ooooooo', current_timestamp);
INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (1, 'Its beautiful and painful moment to moment.', current_timestamp);

INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (2, 'Cool', current_timestamp);
INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (2, 'Amazing', current_timestamp);
INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (2, 'Victoria the best', current_timestamp);
INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (2, 'Really good game', current_timestamp);
INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (2, 'Unbelievable win', current_timestamp);

INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (3, 'ITS SO MUCH MORE MANEUVERABLE THAN A LONGBOARD', current_timestamp);
INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (3, 'Amazing', current_timestamp);
INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (3, 'Really, its a lot of fun to ride', current_timestamp);
INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (3, 'Cooool!', current_timestamp);
INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (3, 'Blink Board seems downright cheap', current_timestamp);

INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (4, 'Goo Goo Dolls!!! Love them.', current_timestamp);
INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (4, 'News song is amazing', current_timestamp);
INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (4, 'Best group', current_timestamp);
INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (4, 'Love new song.', current_timestamp);
INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (4, 'The best group for me!', current_timestamp);
INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (4, 'Cool', current_timestamp);

INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (5, 'Very interesting.', current_timestamp);
INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (5, 'Step in future.', current_timestamp);
INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (5, 'Amazing', current_timestamp);
INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (5, 'Good news', current_timestamp);
INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (5, 'Good algorithm', current_timestamp);

INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (6, 'Good ', current_timestamp);

INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (7, 'Creag fsd  sdf s ', current_timestamp);

INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (8, 'SJfsdkj sdjf kd fsjk sd', current_timestamp);

INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (9, 'Very good', current_timestamp);

INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (10, 'Bad news for all of us', current_timestamp);

INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (11, 'Say something new, please', current_timestamp);

INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (12, 'Here i am', current_timestamp);

INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (13, 'This is all of me', current_timestamp);

INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (14, 'I''m standing alone', current_timestamp);

INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (15, 'Something new for me', current_timestamp);

INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (16, 'Oh, their products are amazing', current_timestamp);


INSERT INTO TAGS(TAG_ID, TAG_NAME) VALUES (1, 'sport');
INSERT INTO TAGS(TAG_ID, TAG_NAME) VALUES (2, 'music');
INSERT INTO TAGS(TAG_ID, TAG_NAME) VALUES (3, 'interview');
INSERT INTO TAGS(TAG_ID, TAG_NAME) VALUES (4, 'technology');
INSERT INTO TAGS(TAG_ID, TAG_NAME) VALUES (5, 'science');
INSERT INTO TAGS(TAG_ID, TAG_NAME) VALUES (6, 'politics');

INSERT INTO news_tags VALUES (1,2);
INSERT INTO news_tags VALUES (1,3);
INSERT INTO news_tags VALUES (2,1);
INSERT INTO news_tags VALUES (3,1);
INSERT INTO news_tags VALUES (3,4);
INSERT INTO news_tags VALUES (4,2);
INSERT INTO news_tags VALUES (5,5);
INSERT INTO news_tags VALUES (6,6);
INSERT INTO news_tags VALUES (7,6);
INSERT INTO news_tags VALUES (7,3);
INSERT INTO news_tags VALUES (8,5);
INSERT INTO news_tags VALUES (8,6);
INSERT INTO news_tags VALUES (9,6);
INSERT INTO news_tags VALUES (10,6);
INSERT INTO news_tags VALUES (11,4);
INSERT INTO news_tags VALUES (11,6);
INSERT INTO news_tags VALUES (12,1);
INSERT INTO news_tags VALUES (12,3);
INSERT INTO news_tags VALUES (13,1);
INSERT INTO news_tags VALUES (13,3);
INSERT INTO news_tags VALUES (14,2);
INSERT INTO news_tags VALUES (14,6);
INSERT INTO news_tags VALUES (15,6);
INSERT INTO news_tags VALUES (15,3);
INSERT INTO news_tags VALUES (16,4);
INSERT INTO news_tags VALUES (16,5);


INSERT INTO AUTHORS(ATR_ID, ATR_NAME, ATR_EXPIRED) VALUES (1, 'Svetlana', current_timestamp);
INSERT INTO AUTHORS(ATR_ID, ATR_NAME, ATR_EXPIRED) VALUES (2, 'Igor', current_timestamp);
INSERT INTO AUTHORS(ATR_ID, ATR_NAME, ATR_EXPIRED) VALUES (3, 'Sam', current_timestamp);
INSERT INTO AUTHORS(ATR_ID, ATR_NAME, ATR_EXPIRED) VALUES (4, 'Ben', current_timestamp);
INSERT INTO AUTHORS(ATR_ID, ATR_NAME, ATR_EXPIRED) VALUES (5, 'James', current_timestamp);


INSERT INTO news_authors VALUES (1,1);
INSERT INTO news_authors VALUES (2,2);
INSERT INTO news_authors VALUES (3,3);
INSERT INTO news_authors VALUES (4,4);
INSERT INTO news_authors VALUES (5,5);
INSERT INTO news_authors VALUES (6,5);
INSERT INTO news_authors VALUES (7,2);
INSERT INTO news_authors VALUES (8,3);
INSERT INTO news_authors VALUES (9,4);
INSERT INTO news_authors VALUES (10,5);
INSERT INTO news_authors VALUES (11,1);
INSERT INTO news_authors VALUES (12,2);
INSERT INTO news_authors VALUES (13,3);
INSERT INTO news_authors VALUES (14,5);
INSERT INTO news_authors VALUES (15,5);
INSERT INTO news_authors VALUES (16,5);



