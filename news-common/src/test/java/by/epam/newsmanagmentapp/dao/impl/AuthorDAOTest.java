package by.epam.newsmanagmentapp.dao.impl;

import by.epam.newsmanagmentapp.dao.AuthorDAO;
import by.epam.newsmanagmentapp.domain.bean.Author;
import by.epam.newsmanagmentapp.exceptions.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static junit.framework.TestCase.assertEquals;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-application-context.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
@DatabaseSetup(value = "classpath:by/epam/newsmanagmentapp/dao/AuthorDAOTest.xml", type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = "classpath:by/epam/newsmanagmentapp/dao/AuthorDAOTest.xml", type = DatabaseOperation.DELETE_ALL)
public class AuthorDAOTest {

    @Autowired
    private AuthorDAO authorDAO;

    @BeforeClass
    public static void setLocale() {
        Locale.setDefault(Locale.ENGLISH);
    }

    @Test
    public void testFindAll() throws DAOException {
        final int size = 4;
        List<Author> authorList = authorDAO.findAll();
        assertEquals(size, authorList.size());
    }

    @Test
    public void testInsert() throws DAOException {
        final String authorName = "TestAuthor";

        Author author = new Author();
        author.setExpired(new Timestamp(new Date().getTime()));
        author.setAuthorName(authorName);

        Long authorId = authorDAO.insert(author);
        Author actualAuthor = authorDAO.getByKey(authorId);
        author.setAuthorId(authorId);

        assertEquals(author, actualAuthor);
    }

    @Test
    public void testDelete() throws DAOException {
        final Long id = 3L;
        final int size = 3;
        authorDAO.delete(id);
        List<Author> authorList = authorDAO.findAll();
        assertEquals(size, authorList.size());
    }

    @Test
    public void testGetByKey() throws DAOException {
        final Long id = 1L;
        Author author = authorDAO.getByKey(id);
        assertEquals(id, author.getAuthorId());
    }

    @Test
    public void testUpdate() throws DAOException {
        final String EXPIRED_STRING = "2016-05-18 08:58:03.628";
        final Long id = 1L;
        final Timestamp newTimestamp = Timestamp.valueOf(EXPIRED_STRING);
        Author author = authorDAO.getByKey(id);
        author.setExpired(newTimestamp);
        authorDAO.update(author);
        author = authorDAO.getByKey(id);
        assertEquals(newTimestamp, author.getExpired());
    }

    @Test
    public void testCheckAuthor() throws DAOException {
        final String AUTHOR_NAME = "Siarhei";
        boolean result = authorDAO.checkAuthor(AUTHOR_NAME);
        assertEquals(true, result);
    }

    @Test
    public void testGetAuthorByName() throws DAOException {
        final String AUTHOR_NAME = "Siarhei";
        Author author = authorDAO.getAuthorByName(AUTHOR_NAME);
        assertEquals(AUTHOR_NAME, author.getAuthorName());
    }

    @Test
    public void testGetNotExpiredAuthors() throws DAOException {
        final int size = 4;
        List<Author> authors = authorDAO.getNotExpiredAuthors();
        assertEquals(size, authors.size());
    }

    @Test
    public void testGetAuthorByNewsId() throws DAOException {
        final Long newsId = 1L;
        final Long authorId = 1L;
        Author author = authorDAO.getAuthorByNewsId(newsId);
        assertEquals(authorId, author.getAuthorId());
    }
}
