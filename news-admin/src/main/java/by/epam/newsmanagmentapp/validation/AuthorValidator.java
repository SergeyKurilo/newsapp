package by.epam.newsmanagmentapp.validation;

import by.epam.newsmanagmentapp.domain.bean.Author;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class AuthorValidator implements Validator {

  @Override
  public boolean supports(Class<?> aClass) {
    return Author.class.equals(aClass);
  }

  @Override
  public void validate(Object o, Errors errors) {
    Author author = (Author) o;

    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "authorName", "NotEmpty.author.authorName", "Author name is required!");
  }
}
