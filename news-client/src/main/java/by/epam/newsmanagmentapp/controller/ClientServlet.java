package by.epam.newsmanagmentapp.controller;

import by.epam.newsmanagmentapp.controller.commands.CommandException;
import by.epam.newsmanagmentapp.controller.commands.ICommand;
import by.epam.newsmanagmentapp.utils.Action;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "news-client", urlPatterns = "/ClientController")
public class ClientServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(ClientServlet.class);
    private WebApplicationContext webApplicationContext;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
        webApplicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(this.getServletContext());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }


    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        final String COMMAND_NAME_PARAM = "command";
        final String DEFAULT_COMMAND = "loadAllNews";
        final String ACTION_PARAM = "action";
        String path;

        String commandName = request.getParameter(COMMAND_NAME_PARAM);
        if (commandName == null) {
            commandName = DEFAULT_COMMAND;
        }

        ICommand command = (ICommand) webApplicationContext.getBean(commandName);

        if (command != null) {
            try {
                path = command.execute(request);
            } catch (CommandException e) {
                LOGGER.error("Exception occured: ", e);
                request.setAttribute(ACTION_PARAM, Action.REDIRECT);
                path = "/error";
            }
        } else {
            request.setAttribute(ACTION_PARAM, Action.REDIRECT);
            path = "/error";
        }

        Action action = (Action) request.getAttribute(ACTION_PARAM);
        switch (action) {
            case FORWARD:
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(path);
                dispatcher.forward(request, response);
                break;
            case REDIRECT:
                response.sendRedirect(request.getContextPath() + path);
                break;
        }

    }
}
