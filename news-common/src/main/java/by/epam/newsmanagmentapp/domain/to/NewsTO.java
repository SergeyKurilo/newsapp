package by.epam.newsmanagmentapp.domain.to;


import by.epam.newsmanagmentapp.domain.bean.Author;
import by.epam.newsmanagmentapp.domain.bean.Comment;
import by.epam.newsmanagmentapp.domain.bean.News;
import by.epam.newsmanagmentapp.domain.bean.Tag;

import java.util.List;

/**
 * Class to carry multiple data elements. Contains news and comments, tags, author related to this news.
 */
public class NewsTO {
    private News news;
    private List<Comment> comments;
    private List<Tag> tags;
    private Author author;

    public NewsTO() {
    }

    public NewsTO(News news, List<Comment> comments, List<Tag> tags, Author author) {
        this.news = news;
        this.comments = comments;
        this.tags = tags;
        this.author = author;
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NewsTO newsTO = (NewsTO) o;

        if (news != null ? !news.equals(newsTO.news) : newsTO.news != null) return false;
        if (comments != null ? !comments.equals(newsTO.comments) : newsTO.comments != null) return false;
        if (tags != null ? !tags.equals(newsTO.tags) : newsTO.tags != null) return false;
        return author != null ? author.equals(newsTO.author) : newsTO.author == null;

    }

    @Override
    public int hashCode() {
        int result = news != null ? news.hashCode() : 0;
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("NewsTO{");
        sb.append("news=").append(news);
        sb.append(", comments=").append(comments);
        sb.append(", tags=").append(tags);
        sb.append(", author=").append(author);
        sb.append('}');
        return sb.toString();
    }
}
