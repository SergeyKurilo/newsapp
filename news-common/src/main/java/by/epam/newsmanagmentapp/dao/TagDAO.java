package by.epam.newsmanagmentapp.dao;

import by.epam.newsmanagmentapp.domain.bean.Tag;
import by.epam.newsmanagmentapp.exceptions.DAOException;

import java.util.List;

/**
 * Describes specific operations which can be performed with {@link Tag} objects in the database.
 */
public interface TagDAO extends GenericDAO<Tag> {
    /**
     * Loads all {@link Tag} records from database with specified news identifier.
     *
     * @param newsId identifier of news.
     * @return List of ${@link Tag} objects from database.
     * @throws DAOException if there were errors during reading records from database.
     */
    List<Tag> getTagsByNewsId(Long newsId) throws DAOException;

}
