package by.epam.newsmanagmentapp.controller.commands.impl;

import by.epam.newsmanagmentapp.controller.commands.CommandException;
import by.epam.newsmanagmentapp.controller.commands.ICommand;
import by.epam.newsmanagmentapp.domain.bean.Comment;
import by.epam.newsmanagmentapp.exceptions.ServiceException;
import by.epam.newsmanagmentapp.service.CommentService;
import by.epam.newsmanagmentapp.utils.Action;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Component
public class AddComment implements ICommand {

  @Autowired
  private CommentService commentService;

  private static final String COMMENT_TEXT = "commentText";
  private static final String NEWS_ID = "newsId";
  private static final String ACTION_PARAM = "action";
  private static final String PATH = "/ClientController?command=singleNews&id=";

  @Override
  public String execute(HttpServletRequest request) throws CommandException {
    Long newsId = Long.valueOf(request.getParameter(NEWS_ID));
    String commentText = request.getParameter(COMMENT_TEXT);
    Comment comment = new Comment();
    comment.setNewsId(newsId);
    comment.setCommentText(commentText);
    comment.setCreationDate(Timestamp.valueOf(LocalDateTime.now()));

    try {
      commentService.insert(comment);
    } catch (ServiceException e) {
      throw new CommandException(e);
    }

    request.setAttribute(ACTION_PARAM, Action.REDIRECT);
    return PATH + newsId;
  }
}
