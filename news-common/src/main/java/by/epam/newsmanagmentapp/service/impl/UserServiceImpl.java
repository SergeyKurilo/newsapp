
package by.epam.newsmanagmentapp.service.impl;

import by.epam.newsmanagmentapp.dao.UserDAO;
import by.epam.newsmanagmentapp.domain.bean.User;
import by.epam.newsmanagmentapp.exceptions.DAOException;
import by.epam.newsmanagmentapp.exceptions.ServiceException;
import by.epam.newsmanagmentapp.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Implementation of {@link UserService}.
 */
@Component
@Transactional
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LogManager.getLogger(UserServiceImpl.class);

    @Autowired
    private UserDAO userDAO;

    /**
     * Implementation of {@link UserService#findAll()} method.
     */
    @Override
    public List<User> findAll() throws ServiceException {
        try {
            return userDAO.findAll();
        } catch (DAOException e) {
            LOGGER.error("Load all authors service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link UserService#insert(Object)} method.
     */
    @Override
    public Long insert(User user) throws ServiceException {
        try {
            return userDAO.insert(user);
        } catch (DAOException e) {
            LOGGER.error("Create user node service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link UserService#delete(Long)} method.
     */
    @Override
    public void delete(Long id) throws ServiceException {
        try {
            userDAO.delete(id);
        } catch (DAOException e) {
            LOGGER.error("Removing user service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link UserService#getByKey(Long)} method.
     */
    @Override
    public User getByKey(Long id) throws ServiceException {
        try {
            return userDAO.getByKey(id);
        } catch (DAOException e) {
            LOGGER.error("Find user node by ID " + id + " service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link UserService#update(Object)} method.
     */
    @Override
    public void update(User user) throws ServiceException {
        try {
            userDAO.update(user);
        } catch (DAOException e) {
            LOGGER.error("Update user node service error");
            throw new ServiceException(e);
        }
    }

    @Override
    public User getUserInfo(String userName) throws ServiceException {
        try {
            return userDAO.getUserInfo(userName);
        } catch (DAOException e) {
            LOGGER.error("Find user node by user name " + userName + " service error");
            throw new ServiceException(e);
        }
    }
}

