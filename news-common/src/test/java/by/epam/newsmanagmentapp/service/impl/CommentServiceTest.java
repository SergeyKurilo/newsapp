package by.epam.newsmanagmentapp.service.impl;

import by.epam.newsmanagmentapp.dao.CommentDAO;
import by.epam.newsmanagmentapp.domain.bean.Comment;
import by.epam.newsmanagmentapp.exceptions.DAOException;
import by.epam.newsmanagmentapp.exceptions.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {
    @InjectMocks
    private CommentServiceImpl commentService;

    @Mock
    private CommentDAO commentDAO;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateComment() throws ServiceException, DAOException {
        Comment comment = new Comment();
        when(commentDAO.insert(comment)).thenReturn(1L);
        Long tempId = commentService.insert(comment);
        assertNotNull(tempId);
        verify(commentDAO).insert(comment);
    }

    @Test(expected = ServiceException.class)
    public void testCreateCommentExceptionSituation() throws ServiceException, DAOException {
        Comment comment = new Comment();
        when(commentDAO.insert(comment)).thenThrow(DAOException.class);
        Long tempId = commentService.insert(comment);
    }

    @Test
    public void testUpdateComment() throws ServiceException, DAOException {
        Comment comment = new Comment();
        commentService.update(comment);
        verify(commentDAO, times(1)).update(comment);
    }

    @Test
    public void testLoadById() throws ServiceException, DAOException {
        Comment comment = new Comment();
        Long commentId = 1L;
        when(commentDAO.getByKey(commentId)).thenReturn(comment);
        Comment commentRole = commentService.getByKey(commentId);
        assertEquals(commentRole, comment);
        verify(commentDAO).getByKey(commentId);
    }


    @Test
    public void testDelete() throws ServiceException, DAOException {
        Long commentId = 1L;
        commentService.delete(commentId);
        verify(commentDAO).delete(commentId);
    }

    @Test
    public void testDeleteByNewsId() throws ServiceException, DAOException {
        Long newsId = 1L;
        commentService.deleteByNewsId(newsId);
        verify(commentDAO).deleteByNewsId(newsId);
    }

    @Test
    public void testFindAll() throws ServiceException, DAOException {
        when(commentDAO.findAll()).thenReturn(new ArrayList<>());
        List<Comment> result = commentService.findAll();
        assertNotNull(result);
        verify(commentDAO).findAll();
    }

    @Test
    public void testGetCommentsByNewsId() throws ServiceException, DAOException {
        Long newsId = 1L;
        when(commentDAO.getCommentsByNewsId(newsId)).thenReturn(new ArrayList<>());
        List<Comment> result = commentService.getCommentsByNewsId(newsId);
        assertNotNull(result);
        verify(commentDAO).getCommentsByNewsId(newsId);
    }
}
