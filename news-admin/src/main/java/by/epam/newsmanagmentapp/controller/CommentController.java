package by.epam.newsmanagmentapp.controller;

import by.epam.newsmanagmentapp.domain.bean.Comment;
import by.epam.newsmanagmentapp.exceptions.ServiceException;
import by.epam.newsmanagmentapp.service.CommentService;
import by.epam.newsmanagmentapp.utils.ErrorsAttributeBuilder;
import by.epam.newsmanagmentapp.validation.CommentValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Controller
public class CommentController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private CommentValidator commentValidator;

    private static final String REDIRECT_PREFIX = "redirect:";
    private static final String NEWS_MAPPING = "/news/";
    private static final String ADD_COMMENT_MAPPING = "/addcomment";
    private static final String DELETE_COMMENT_MAPPING = "/deletecomment";
    private static final String COMMENT_MODEL_ATTRIBUTE = "comment";
    private static final String ERRORS_MODEL_ATTRIBUTE = "errors";

    @InitBinder(COMMENT_MODEL_ATTRIBUTE)
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(commentValidator);
    }

    @RequestMapping(value = DELETE_COMMENT_MAPPING, method = RequestMethod.POST)
    public ModelAndView deleteComment(@RequestParam Long newsId, Long commentId) throws ServiceException {
        ModelAndView modelAndView = new ModelAndView();
        commentService.delete(commentId);
        modelAndView.setViewName(REDIRECT_PREFIX + NEWS_MAPPING + newsId);
        return modelAndView;
    }

    @RequestMapping(value = ADD_COMMENT_MAPPING, method = RequestMethod.POST)
    public RedirectView addComment(@ModelAttribute(COMMENT_MODEL_ATTRIBUTE) @Validated Comment comment, BindingResult bindingResult, RedirectAttributes redirectAttributes) throws ServiceException {
        Long newsId = comment.getNewsId();
        RedirectView redirectView = new RedirectView(NEWS_MAPPING + newsId);
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute(COMMENT_MODEL_ATTRIBUTE, comment);
            redirectAttributes.addFlashAttribute(ERRORS_MODEL_ATTRIBUTE, ErrorsAttributeBuilder.getErrors(bindingResult));
            return redirectView;
        }
        Timestamp timestamp = Timestamp.valueOf(LocalDateTime.now());
        comment.setCreationDate(timestamp);
        commentService.insert(comment);
        return redirectView;
    }

}
