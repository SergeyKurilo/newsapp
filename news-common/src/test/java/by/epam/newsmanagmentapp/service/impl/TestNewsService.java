package by.epam.newsmanagmentapp.service.impl;

import by.epam.newsmanagmentapp.dao.NewsDAO;
import by.epam.newsmanagmentapp.domain.bean.News;
import by.epam.newsmanagmentapp.domain.bean.SearchCriteria;
import by.epam.newsmanagmentapp.exceptions.DAOException;
import by.epam.newsmanagmentapp.exceptions.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TestNewsService {

    @InjectMocks
    private NewsServiceImpl newsService;

    @Mock
    private NewsDAO newsDAO;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateNews() throws ServiceException, DAOException {
        News news = new News();
        when(newsDAO.insert(news)).thenReturn(1L);
        Long tempId = newsService.insert(news);
        assertNotNull(tempId);
        verify(newsDAO).insert(news);
    }

    @Test(expected = ServiceException.class)
    public void testCreateNewsExceptionSituation() throws ServiceException, DAOException {
        News news = new News();
        when(newsDAO.insert(news)).thenThrow(DAOException.class);
        Long tempId = newsService.insert(news);
    }

    @Test
    public void testUpdateNews() throws ServiceException, DAOException {
        News news = new News();
        newsService.update(news);
        verify(newsDAO, times(1)).update(news);
    }

    @Test
    public void testDeleteNews() throws ServiceException, DAOException {
        Long newsId = 1L;
        newsService.delete(newsId);
        verify(newsDAO).delete(newsId);
    }


    @Test
    public void testFindAllNewses() throws ServiceException, DAOException {
        when(newsDAO.findAll()).thenReturn(new ArrayList<>());
        List<News> result = newsService.findAll();
        assertNotNull(result);
        verify(newsDAO).findAll();
    }

    @Test
    public void findBySearchCriteria() throws ServiceException, DAOException {
        SearchCriteria searchCriteria = new SearchCriteria();
        when(newsDAO.findBySearchCriteria(searchCriteria)).thenReturn(new ArrayList<>());
        List<News> result = newsService.findBySearchCriteria(searchCriteria);
        assertNotNull(result);
        verify(newsDAO).findBySearchCriteria(searchCriteria);
    }

    @Test
    public void createNewsAuthorLink() throws ServiceException, DAOException {
        Long newsId = 1L;
        Long authorId = 1L;
        newsService.createNewsAuthorLink(newsId, authorId);
        verify(newsDAO).createNewsAuthorLink(newsId, authorId);
    }

    @Test
    public void deleteNewsAuthorLink() throws ServiceException, DAOException {
        Long newsId = 1L;
        newsService.deleteNewsAuthorLink(newsId);
        verify(newsDAO).deleteNewsAuthorLink(newsId);
    }

    @Test
    public void createNewsTagsLinks() throws ServiceException, DAOException {
        Long newsId = 1L;
        Long tagIdOne = 1L;
        Long tagIdTwo = 2L;
        newsService.createNewsTagsLinks(newsId, tagIdOne, tagIdTwo);
        verify(newsDAO).createNewsTagsLinks(newsId, tagIdOne, tagIdTwo);

    }

    @Test
    public void deleteNewsTagLink() throws ServiceException, DAOException {
        Long newsId = 1L;
        newsService.deleteNewsTagLink(newsId);
        verify(newsDAO).deleteNewsTagLink(newsId);
    }

    @Test
    public void countNumberOfNews() throws ServiceException, DAOException {
        Long count = 10L;
        when(newsDAO.getNumberOfNews()).thenReturn(10L);
        Long tempCount = newsService.countNumberOfNews();
        assertEquals(count, tempCount);
        verify(newsDAO).getNumberOfNews();
    }
}
