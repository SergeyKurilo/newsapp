package by.epam.newsmanagmentapp.controller;

import by.epam.newsmanagmentapp.domain.bean.Author;
import by.epam.newsmanagmentapp.exceptions.ServiceException;
import by.epam.newsmanagmentapp.service.AuthorService;
import by.epam.newsmanagmentapp.utils.ErrorsAttributeBuilder;
import by.epam.newsmanagmentapp.validation.AuthorValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Controller
public class AuthorController {

  @Autowired
  private AuthorService authorService;

  @Autowired
  private AuthorValidator authorValidator;

  private static final String UPDATE_AUTHOR_MAPPING = "/updateauthor";
  private static final String AUTHORS_MAPPING = "/authors";
  private static final String ADD_AUTHOR_MAPPING = "/addauthor";
  private static final String EXPIRE_AUTHOR_MAPPING = "/expireauthor";
  private static final String AUTHOR_ADD_MODEL_ATTRIBUTE = "addAuthor";
  private static final String AUTHOR_EDIT_MODEL_ATTRIBUTE = "editAuthor";
  private static final String AUTHORS_PAGE = "authors";
  private static final String AUTHORS_ATTRIBUTE = "authors";
  private static final String REDIRECT_PREFIX = "redirect:";
  private static final String ERRORS_MODEL_ATTRIBUTE = "errors";
  private static final String AUTHOR_ID_ERROR_ATTRIBUTE = "authorIdError";

  @InitBinder({AUTHOR_ADD_MODEL_ATTRIBUTE, AUTHOR_EDIT_MODEL_ATTRIBUTE})
  protected void initBinder(WebDataBinder binder) {
    binder.setValidator(authorValidator);
  }

  @RequestMapping(value = AUTHORS_MAPPING, method = RequestMethod.GET)
  public ModelAndView getAllAuthors(final RedirectAttributes redirectAttributes) throws ServiceException {
    ModelAndView modelAndView = new ModelAndView(AUTHORS_PAGE);
    modelAndView.addObject(AUTHORS_ATTRIBUTE, authorService.getNotExpiredAuthors());
    if (redirectAttributes.getFlashAttributes().isEmpty()) {
      modelAndView.addObject(AUTHOR_ADD_MODEL_ATTRIBUTE, new Author());
      modelAndView.addObject(AUTHOR_EDIT_MODEL_ATTRIBUTE, new Author());
    }
    return modelAndView;
  }

  @RequestMapping(value = ADD_AUTHOR_MAPPING, method = RequestMethod.POST)
  public ModelAndView addAuthor(@ModelAttribute(AUTHOR_ADD_MODEL_ATTRIBUTE) @Validated Author author, BindingResult bindingResult, final RedirectAttributes redirectAttributes) throws ServiceException {
    ModelAndView modelAndView = new ModelAndView(REDIRECT_PREFIX + AUTHORS_MAPPING);
    if (bindingResult.hasErrors()) {
      redirectAttributes.addFlashAttribute(ERRORS_MODEL_ATTRIBUTE, ErrorsAttributeBuilder.getErrors(bindingResult));
      redirectAttributes.addFlashAttribute(AUTHOR_ADD_MODEL_ATTRIBUTE, author);
      return modelAndView;
    }
    Timestamp timestamp = Timestamp.valueOf(LocalDateTime.now());
    author.setExpired(timestamp);
    authorService.insert(author);
    return modelAndView;
  }

  @RequestMapping(value = UPDATE_AUTHOR_MAPPING, method = RequestMethod.POST)
  public ModelAndView updateAuthor(@ModelAttribute(AUTHOR_EDIT_MODEL_ATTRIBUTE) @Validated Author author, BindingResult bindingResult, final RedirectAttributes redirectAttributes) throws ServiceException {
    ModelAndView modelAndView = new ModelAndView(REDIRECT_PREFIX + AUTHORS_MAPPING);
    if (bindingResult.hasErrors()) {
      redirectAttributes.addFlashAttribute(ERRORS_MODEL_ATTRIBUTE, ErrorsAttributeBuilder.getErrors(bindingResult));
      redirectAttributes.addFlashAttribute(AUTHOR_ID_ERROR_ATTRIBUTE, author.getAuthorId());
      return modelAndView;
    }
    Timestamp timestamp = Timestamp.valueOf(LocalDateTime.now());
    author.setExpired(timestamp);
    authorService.update(author);
    return modelAndView;
  }

  @RequestMapping(value = EXPIRE_AUTHOR_MAPPING, method = RequestMethod.POST)
  public ModelAndView expireAuthor(@ModelAttribute(AUTHOR_ADD_MODEL_ATTRIBUTE) Author author) throws ServiceException {
    ModelAndView modelAndView = new ModelAndView();
    authorService.update(author);
    modelAndView.setViewName(REDIRECT_PREFIX + AUTHORS_MAPPING);
    return modelAndView;
  }
}
