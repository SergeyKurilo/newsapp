package by.epam.newsmanagmentapp.service.impl;

import by.epam.newsmanagmentapp.dao.NewsDAO;
import by.epam.newsmanagmentapp.domain.bean.News;
import by.epam.newsmanagmentapp.domain.bean.SearchCriteria;
import by.epam.newsmanagmentapp.domain.to.NextAndPreviousNewsIdTO;
import by.epam.newsmanagmentapp.exceptions.DAOException;
import by.epam.newsmanagmentapp.exceptions.ServiceException;
import by.epam.newsmanagmentapp.service.NewsService;
import by.epam.newsmanagmentapp.util.PaginationUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Implementation of {@link NewsService}.
 */
@Component
@Transactional
public class NewsServiceImpl implements NewsService {

    private static final Logger LOGGER = LogManager.getLogger(NewsServiceImpl.class);

    @Autowired
    private NewsDAO newsDAO;

    /**
     * Implementation of {@link NewsService#findBySearchCriteria(SearchCriteria)} method.
     */
    @Override
    public List<News> findBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
        try {
            return newsDAO.findBySearchCriteria(searchCriteria);
        } catch (DAOException e) {
            LOGGER.error("Load all news with search criteria service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link NewsService#createNewsAuthorLink(Long, Long)} method.
     */
    @Override
    public void createNewsAuthorLink(Long newsId, Long authorId) throws ServiceException {
        try {
            newsDAO.createNewsAuthorLink(newsId, authorId);
        } catch (DAOException e) {
            LOGGER.error("Create link with author and news service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link NewsService#deleteNewsAuthorLink(Long)} method.
     */
    @Override
    public void deleteNewsAuthorLink(Long newsId) throws ServiceException {
        try {
            newsDAO.deleteNewsAuthorLink(newsId);
        } catch (DAOException e) {
            LOGGER.error("Delete news author node service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link NewsService#createNewsTagsLinks(Long, Long...)} method.
     */
    @Override
    public void createNewsTagsLinks(Long newsId, Long... tags) throws ServiceException {
        try {
            newsDAO.createNewsTagsLinks(newsId, tags);
        } catch (DAOException e) {
            LOGGER.error("Create link with author and tags service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link NewsService#deleteNewsTagLink(Long)} method.
     */
    @Override
    public void deleteNewsTagLink(Long newsId) throws ServiceException {
        try {
            newsDAO.deleteNewsTagLink(newsId);
        } catch (DAOException e) {
            LOGGER.error("Delete news tag nodes service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link NewsService#countNumberOfNews()} method.
     */
    @Override
    public Long countNumberOfNews() throws ServiceException {
        try {
            return newsDAO.getNumberOfNews();
        } catch (DAOException e) {
            LOGGER.error("Count number of news service error");
            throw new ServiceException(e);
        }
    }

    @Override
    public Long countNumberOfNewsWithSearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
        try {
            return newsDAO.getNumberOfNewsWithSearchCriteria(searchCriteria);
        } catch (DAOException e) {
            LOGGER.error("Count number of news service error");
            throw new ServiceException(e);
        }
    }

    @Override
    public List<News> loadPage(Long numberOfPage) throws ServiceException {
        Long from = PaginationUtil.getLeftSelectBound(numberOfPage);
        Long to = PaginationUtil.getRightSelectBound(numberOfPage);
        try {
            return newsDAO.loadPage(from, to);
        } catch (DAOException e) {
            LOGGER.error("Load all news service error");
            throw new ServiceException(e);
        }
    }

    @Override
    public List<News> loadPageWithSearchCriteria(SearchCriteria searchCriteria, Long numberOfPage) throws ServiceException {
        Long from = PaginationUtil.getLeftSelectBound(numberOfPage);
        Long to = PaginationUtil.getRightSelectBound(numberOfPage);
        try {
            return newsDAO.loadPageWithSearchCriteria(searchCriteria, from, to);
        } catch (DAOException e) {
            LOGGER.error("Load all news service error");
            throw new ServiceException(e);
        }
    }

    @Override
    public Long numberOfPages() throws ServiceException {
        Long numberOfNews = countNumberOfNews();
        Long numberOfPages = PaginationUtil.numberOfPages(numberOfNews);
        return numberOfPages;
    }

    @Override
    public Long numberOfPagesWithCriteria(SearchCriteria searchCriteria) throws ServiceException {
        Long numberOfNews = countNumberOfNewsWithSearchCriteria(searchCriteria);
        return PaginationUtil.numberOfPages(numberOfNews);
    }

    @Override
    public NextAndPreviousNewsIdTO getNextAndPreviousNewsId(Long newsId) throws ServiceException {
        try {
            return newsDAO.getNextAndPreviousNewsId(newsId);
        } catch (DAOException e) {
            LOGGER.error("Get next and previous news id service error");
            throw new ServiceException(e);
        }
    }

    @Override
    public NextAndPreviousNewsIdTO getNextAndPreviousNewsIdWithSearchCriteria(SearchCriteria searchCriteria, Long newsId) throws ServiceException {
        try {
            return newsDAO.getNextAndPreviousNewsIdWithSearchCriteria(searchCriteria, newsId);
        } catch (DAOException e) {
            LOGGER.error("Get next and previous news id service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link NewsService#findAll()} method.
     */
    @Override
    public List<News> findAll() throws ServiceException {
        try {
            return newsDAO.findAll();
        } catch (DAOException e) {
            LOGGER.error("Load all news service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link NewsService#insert(Object)} method.
     */
    @Override
    public Long insert(News news) throws ServiceException {
        try {
            return newsDAO.insert(news);
        } catch (DAOException e) {
            LOGGER.error("Create news node service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link NewsService#delete(Long)} method.
     */
    @Override
    public void delete(Long id) throws ServiceException {
        try {
            newsDAO.delete(id);
        } catch (DAOException e) {
            LOGGER.error("Removing news service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link NewsService#getByKey(Long)} method.
     */
    @Override
    public News getByKey(Long id) throws ServiceException {
        try {
            return newsDAO.getByKey(id);
        } catch (DAOException e) {
            LOGGER.error("Find news node by ID service error");
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link NewsService#update(Object)} method.
     */
    @Override
    public void update(News news) throws ServiceException {
        try {
            newsDAO.update(news);
        } catch (DAOException e) {
            LOGGER.error("Update news node service error");
            throw new ServiceException(e);
        }
    }
}

