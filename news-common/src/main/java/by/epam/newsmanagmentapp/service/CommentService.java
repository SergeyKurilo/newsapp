package by.epam.newsmanagmentapp.service;

import by.epam.newsmanagmentapp.domain.bean.Comment;
import by.epam.newsmanagmentapp.exceptions.ServiceException;

import java.util.List;

/**
 * Provides specific operations which can be performed with {@link Comment} objects.
 */
public interface CommentService extends GenericService<Comment> {
    /**
     * Delete {@link Comment} object with specified news identifier.
     *
     * @param id identifier of news.
     * @throws ServiceException if ${@link by.epam.newsmanagmentapp.exceptions.DAOException} was thrown.
     */
    void deleteByNewsId(Long id) throws ServiceException;

    /**
     * Loads {@link Comment} objects with specified news identifier.
     *
     * @param id identifier of news.
     * @return List of ${@link Comment} objects.
     * @throws ServiceException if ${@link by.epam.newsmanagmentapp.exceptions.DAOException} was thrown.
     */
    List<Comment> getCommentsByNewsId(Long id) throws ServiceException;
}
