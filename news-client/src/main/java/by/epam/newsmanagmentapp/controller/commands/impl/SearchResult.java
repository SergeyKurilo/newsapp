package by.epam.newsmanagmentapp.controller.commands.impl;

import by.epam.newsmanagmentapp.controller.commands.CommandException;
import by.epam.newsmanagmentapp.controller.commands.ICommand;
import by.epam.newsmanagmentapp.domain.bean.Author;
import by.epam.newsmanagmentapp.domain.bean.SearchCriteria;
import by.epam.newsmanagmentapp.domain.bean.Tag;
import by.epam.newsmanagmentapp.domain.to.NewsTO;
import by.epam.newsmanagmentapp.exceptions.ServiceException;
import by.epam.newsmanagmentapp.service.AuthorService;
import by.epam.newsmanagmentapp.service.ComplexNewsService;
import by.epam.newsmanagmentapp.service.NewsService;
import by.epam.newsmanagmentapp.service.TagService;
import by.epam.newsmanagmentapp.utils.Action;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Component
public class SearchResult implements ICommand {

  @Autowired
  private AuthorService authorService;

  @Autowired
  private TagService tagService;

  @Autowired
  private ComplexNewsService complexNewsService;

  @Autowired
  private NewsService newsService;

  private static final String PAGE_NUMBER_VARIABLE = "page";
  private static final String TAGS_LIST = "tagsList";
  private static final String NEWS_TO = "newsTOs";
  private static final String NUMBER_OF_PAGES = "numberOfPages";
  private static final String AUTHORS_LIST = "authorsList";
  private static final String SEARCH_CRITERIA = "searchCriteria";
  private static final String ACTION_PARAM = "action";

  @Override
  public String execute(HttpServletRequest request) throws CommandException {
    HttpSession session = request.getSession(false);
    String[] tagsId = request.getParameterValues("tagId");
    String authorId = request.getParameter("authorId");
    SearchCriteria searchCriteria;
    String path = "/allNews";
    Long numberOfPage = 1L;

    if (tagsId == null && authorId == null) {
      searchCriteria = (SearchCriteria) session.getAttribute(SEARCH_CRITERIA);
      if (searchCriteria == null) {
        path = "/error";
        request.setAttribute(ACTION_PARAM, Action.REDIRECT);
        return path;
      }
    } else {
      session.removeAttribute(SEARCH_CRITERIA);
      List<Long> tags = createTagList(tagsId);
      searchCriteria = new SearchCriteria();
      searchCriteria.setAuthorId(Long.valueOf(authorId));
      searchCriteria.setTagsId(tags);
      session.setAttribute(SEARCH_CRITERIA, searchCriteria);
      request.setAttribute(ACTION_PARAM, Action.FORWARD);
    }

    if (request.getParameter(PAGE_NUMBER_VARIABLE) != null) {
      numberOfPage = Long.valueOf(request.getParameter(PAGE_NUMBER_VARIABLE));
    }

    try {
      fillInRequest(request, searchCriteria, numberOfPage);
    } catch (ServiceException e) {
      throw new CommandException(e);
    }
    session.setAttribute(SEARCH_CRITERIA, searchCriteria);
    return path;
  }


  private List<Long> createTagList(String[] tags) {
    List<Long> tagsId = null;
    if (tags != null) {
      tagsId = new ArrayList<>();
      for (String id : tags) {
        tagsId.add(Long.valueOf(id));
      }
    }
    return tagsId;
  }


  private void fillInRequest(HttpServletRequest request, SearchCriteria searchCriteria, Long numberOfPage) throws ServiceException {
    Long numberOfPages = newsService.numberOfPagesWithCriteria(searchCriteria);
    List<Tag> tags = tagService.findAll();
    List<Author> authors = authorService.getNotExpiredAuthors();
    List<NewsTO> newsTOs = complexNewsService.loadPageWithCriteria(searchCriteria, numberOfPage);
    request.setAttribute(NEWS_TO, newsTOs);
    request.setAttribute(AUTHORS_LIST, authors);
    request.setAttribute(TAGS_LIST, tags);
    request.setAttribute(NUMBER_OF_PAGES, numberOfPages);
    request.setAttribute(ACTION_PARAM, Action.FORWARD);
  }


}
