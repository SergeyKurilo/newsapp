package by.epam.newsmanagmentapp.dao.impl;

import by.epam.newsmanagmentapp.dao.NewsDAO;
import by.epam.newsmanagmentapp.domain.bean.News;
import by.epam.newsmanagmentapp.domain.bean.SearchCriteria;
import by.epam.newsmanagmentapp.exceptions.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-application-context.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
@DatabaseSetup(value = "classpath:by/epam/newsmanagmentapp/dao/NewsDAOTest.xml", type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = "classpath:by/epam/newsmanagmentapp/dao/NewsDAOTest.xml", type = DatabaseOperation.DELETE_ALL)
public class NewsDAOTest {

    @Autowired
    private NewsDAO newsDAO;

    @BeforeClass
    public static void setLocale() {
        Locale.setDefault(Locale.ENGLISH);
    }

    @Test
    public void testFindAll() throws DAOException {
        final int size = 3;
        List<News> newses = newsDAO.findAll();
        assertEquals(size, newses.size());

    }

    @Test
    public void testInsert() throws DAOException {
        final String title = "NewTitle";
        final String shortText = "ShortText";
        final String fullText = "FullText";
        final String creation = "2016-05-18 08:58:03.628";
        News news = new News();
        news.setTitle(title);
        news.setShortText(shortText);
        news.setFullText(fullText);
        news.setCreationDate(Timestamp.valueOf(creation));
        news.setModificationDate(Date.valueOf(LocalDate.now()));
        Long newsId = newsDAO.insert(news);
        News actualNews = newsDAO.getByKey(newsId);
        assertEquals(newsId, actualNews.getNewsId());
    }

    @Test
    public void testDelete() throws DAOException {
        final Long id = 2L;
        final int size = 2;
        newsDAO.delete(id);
        List<News> newses = newsDAO.findAll();
        assertEquals(size, newses.size());
    }

    @Test
    public void testGetByKey() throws DAOException {
        final Long id = 1L;
        News news = newsDAO.getByKey(id);
        assertEquals(id, news.getNewsId());
    }

    @Test
    public void testUpdate() throws DAOException {
        final String TITLE = "Hello";
        final Long id = 1L;
        News news = newsDAO.getByKey(id);
        news.setTitle(TITLE);
        newsDAO.update(news);
        news = newsDAO.getByKey(id);
        assertEquals(TITLE, news.getTitle());
    }


    @Test
    public void testFindBySearchCriteria() throws DAOException {
        final Long id = 1L;
        final int size = 1;
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setAuthorId(id);
        List<Long> tagsId = new ArrayList<>();
        tagsId.add(id);
        searchCriteria.setTagsId(tagsId);
        List<News> newsList = newsDAO.findBySearchCriteria(searchCriteria);
        assertEquals(size,newsList.size());
    }

    @Test
    public void testCreateNewsAuthorLink() throws DAOException {
        final Long newsId = 1L;
        final Long authorId = 1L;
        newsDAO.createNewsAuthorLink(newsId,authorId);
    }

    @Test
    public void testDeleteNewsAuthorLink() throws DAOException {
        final Long newsId = 1L;
        newsDAO.deleteNewsAuthorLink(newsId);
    }

    @Test
    public void testCreateNewsTagsLinks() throws DAOException {
        final Long [] tags = {1L, 2L};
        final Long newsId = 1L;
        newsDAO.createNewsTagsLinks(newsId,tags);
    }

    @Test
    public void testDeleteNewsTagLink() throws DAOException {
        final Long newsId = 1L;
        newsDAO.deleteNewsTagLink(newsId);
    }

    @Test
    public void testCountNumberOfNews() throws DAOException {
        final Long size = 3L;
        Long count = newsDAO.getNumberOfNews();
        assertEquals(size, count);
    }

}
