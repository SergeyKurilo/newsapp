package by.epam.newsmanagmentapp.service;

import by.epam.newsmanagmentapp.exceptions.ServiceException;

import java.util.List;

/**
 * Provides basic operations with {@link Entity} objects.
 *
 * @param <Entity> Generic type of entity object.
 */
public interface GenericService<Entity> {
    /**
     * Find all ${@link Entity} objects.
     *
     * @return List of ${@link Entity} objects.
     * @throws ServiceException if ${@link by.epam.newsmanagmentapp.exceptions.DAOException} was thrown.
     */
    List<Entity> findAll() throws ServiceException;

    /**
     * Insert {@link Entity} object in database.
     *
     * @param entity ${@link Entity} object which is need to be inserted in database.
     * @return Identifier of inserted object.
     * @throws ServiceException if ${@link by.epam.newsmanagmentapp.exceptions.DAOException} was thrown.
     */
    Long insert(Entity entity) throws ServiceException;

    /**
     * Delete object with identifier specified as parameter.
     *
     * @param id Identifier of object which is must be deleted.
     * @throws ServiceException if ${@link by.epam.newsmanagmentapp.exceptions.DAOException} was thrown.
     */
    void delete(Long id) throws ServiceException;

    /**
     * Return {@link Entity} object with specified identifier.
     *
     * @param id Identifier of object which is must be returned.
     * @return ${@link Entity} object.
     * @throws ServiceException if ${@link by.epam.newsmanagmentapp.exceptions.DAOException} or
     *                          ${@link by.epam.newsmanagmentapp.exceptions.NoSuchEntityException} was thrown.
     */
    Entity getByKey(Long id) throws ServiceException;

    /**
     * Updates {@link Entity} object.
     *
     * @param entity {@link Entity} object which is need to be updated.
     * @throws ServiceException if ${@link by.epam.newsmanagmentapp.exceptions.DAOException} was thrown.
     */
    void update(Entity entity) throws ServiceException;
}
