package by.epam.newsmanagmentapp.validation;

import by.epam.newsmanagmentapp.domain.bean.Tag;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class TagValidator implements Validator {
  @Override
  public boolean supports(Class<?> aClass) {
    return Tag.class.equals(aClass);
  }

  @Override
  public void validate(Object o, Errors errors) {
    Tag tag = (Tag) o;

    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "tagName", "NotEmpty.tag.tagName", "Tag name is required!");
  }
}
