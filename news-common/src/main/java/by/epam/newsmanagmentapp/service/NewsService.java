package by.epam.newsmanagmentapp.service;

import by.epam.newsmanagmentapp.domain.bean.News;
import by.epam.newsmanagmentapp.domain.bean.SearchCriteria;
import by.epam.newsmanagmentapp.domain.to.NextAndPreviousNewsIdTO;
import by.epam.newsmanagmentapp.exceptions.ServiceException;

import java.util.List;

/**
 * Provides specific operations which can be performed with {@link News} objects.
 */
public interface NewsService extends GenericService<News> {
    /**
     * Load all ${@link News} objects corresponding to criteria specified in passed ${@link SearchCriteria} object.
     *
     * @param searchCriteria ${@link SearchCriteria} object.
     * @return List of ${@link News} objects.
     * @throws ServiceException if ${@link by.epam.newsmanagmentapp.exceptions.DAOException} was thrown.
     */
    List<News> findBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException;

    /**
     * Create wire between news and author.
     *
     * @param newsId   identifier of news.
     * @param authorId identifier of author.
     * @throws ServiceException if ${@link by.epam.newsmanagmentapp.exceptions.DAOException} was thrown.
     */
    void createNewsAuthorLink(Long newsId, Long authorId) throws ServiceException;

    /**
     * Delete wire between news and author.
     *
     * @param newsId identifier of news.
     * @throws ServiceException if ${@link by.epam.newsmanagmentapp.exceptions.DAOException} was thrown.
     */
    void deleteNewsAuthorLink(Long newsId) throws ServiceException;

    /**
     * Create wire(s) between news and tag(s).
     *
     * @param newsId identifier of news.
     * @param tags   identifiers of tags.
     * @throws ServiceException if ${@link by.epam.newsmanagmentapp.exceptions.DAOException} was thrown.
     */
    void createNewsTagsLinks(Long newsId, Long... tags) throws ServiceException;

    /**
     * Delete wire(s) between news and tag(s).
     *
     * @param newsId identifier of news.
     * @throws ServiceException if ${@link by.epam.newsmanagmentapp.exceptions.DAOException} was thrown.
     */
    void deleteNewsTagLink(Long newsId) throws ServiceException;

    /**
     * Count number of news.
     *
     * @return number of news.
     * @throws ServiceException if ${@link by.epam.newsmanagmentapp.exceptions.DAOException} was thrown.
     */
    Long countNumberOfNews() throws ServiceException;

    Long countNumberOfNewsWithSearchCriteria(SearchCriteria searchCriteria) throws ServiceException;

    List<News> loadPage(Long numberOfPage) throws ServiceException;

    List<News> loadPageWithSearchCriteria(SearchCriteria searchCriteria, Long numberOfPage) throws ServiceException;

    Long numberOfPages() throws ServiceException;

    Long numberOfPagesWithCriteria(SearchCriteria searchCriteria) throws ServiceException;

    public NextAndPreviousNewsIdTO getNextAndPreviousNewsId(Long newsId) throws ServiceException;

    public NextAndPreviousNewsIdTO getNextAndPreviousNewsIdWithSearchCriteria(SearchCriteria searchCriteria, Long newsId) throws ServiceException;
}

