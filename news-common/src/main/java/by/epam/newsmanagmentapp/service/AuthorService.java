package by.epam.newsmanagmentapp.service;

import by.epam.newsmanagmentapp.domain.bean.Author;
import by.epam.newsmanagmentapp.exceptions.ServiceException;

import java.util.List;

/**
 * Provides specific operations which can be performed with {@link Author} objects.
 */
public interface AuthorService extends GenericService<Author> {
    /**
     * Loads non-expired authors.
     *
     * @return list of non-expired authors.
     * @throws ServiceException if ${@link by.epam.newsmanagmentapp.exceptions.DAOException} was thrown.
     */
    List<Author> getNotExpiredAuthors() throws ServiceException;

    /**
     * Loads {@link Author} object by news identifier.
     *
     * @param newsId identifier of news.
     * @return {@link Author} object with required newsId.
     * @throws ServiceException if ${@link by.epam.newsmanagmentapp.exceptions.DAOException} or
     *      ${@link by.epam.newsmanagmentapp.exceptions.NoSuchEntityException} was thrown.
     */
    Author getAuthorByNewsId(Long newsId) throws ServiceException;
}
