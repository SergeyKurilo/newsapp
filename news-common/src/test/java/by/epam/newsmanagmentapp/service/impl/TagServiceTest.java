package by.epam.newsmanagmentapp.service.impl;

import by.epam.newsmanagmentapp.dao.TagDAO;
import by.epam.newsmanagmentapp.domain.bean.Tag;
import by.epam.newsmanagmentapp.exceptions.DAOException;
import by.epam.newsmanagmentapp.exceptions.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {

    @InjectMocks
    private TagServiceImpl tagService;

    @Mock
    private TagDAO tagDAO;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateTag() throws ServiceException, DAOException {
        Tag tag = new Tag();
        when(tagDAO.insert(tag)).thenReturn(1L);
        Long tempId = tagService.insert(tag);
        assertNotNull(tempId);
        verify(tagDAO).insert(tag);
    }

    @Test(expected = ServiceException.class)
    public void testCreateTagExceptionSituation() throws ServiceException, DAOException {
        Tag tag = new Tag();
        when(tagDAO.insert(tag)).thenThrow(DAOException.class);
        Long tempId = tagService.insert(tag);
    }

    @Test
    public void testUpdateTag() throws ServiceException, DAOException {
        Tag tag = new Tag();
        tagService.update(tag);
        verify(tagDAO, times(1)).update(tag);
    }

    @Test
    public void testGetTagById() throws ServiceException, DAOException {
        Tag tag = new Tag();
        Long authorId = 1L;
        when(tagDAO.getByKey(authorId)).thenReturn(tag);
        Tag tempTag = tagService.getByKey(authorId);
        assertEquals(tempTag, tag);
        verify(tagDAO).getByKey(authorId);
    }


    @Test
    public void testDeleteTag() throws ServiceException, DAOException {
        Long tagId = 1L;
        tagService.delete(tagId);
        verify(tagDAO).delete(tagId);
    }

    @Test
    public void testGetTagsByNewsId() throws ServiceException, DAOException {
        Long newsId = 1L;
        when(tagDAO.getTagsByNewsId(newsId)).thenReturn(new ArrayList<>());
        List<Tag> tagList = tagService.getTagsByNewsId(newsId);
        assertNotNull(tagList);
        verify(tagDAO).getTagsByNewsId(newsId);
    }

    @Test
    public void testFindAllTags() throws ServiceException, DAOException {
        when(tagDAO.findAll()).thenReturn(new ArrayList<>());
        List<Tag> result = tagService.findAll();
        assertNotNull(result);
        verify(tagDAO).findAll();
    }
}
