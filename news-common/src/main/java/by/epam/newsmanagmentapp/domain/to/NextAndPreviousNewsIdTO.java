package by.epam.newsmanagmentapp.domain.to;

/**
 * Created by Siarhei on 25.05.2016.
 */
public class NextAndPreviousNewsIdTO {
  private Long nextId;
  private Long previousId;

  public NextAndPreviousNewsIdTO() {
  }

  public NextAndPreviousNewsIdTO(Long nextId, Long previousId) {
    this.nextId = nextId;
    this.previousId = previousId;
  }

  public Long getNextId() {
    return nextId;
  }

  public void setNextId(Long nextId) {
    this.nextId = nextId;
  }

  public Long getPreviousId() {
    return previousId;
  }

  public void setPreviousId(Long previousId) {
    this.previousId = previousId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    NextAndPreviousNewsIdTO that = (NextAndPreviousNewsIdTO) o;

    if (nextId != null ? !nextId.equals(that.nextId) : that.nextId != null) return false;
    return previousId != null ? previousId.equals(that.previousId) : that.previousId == null;

  }

  @Override
  public int hashCode() {
    int result = nextId != null ? nextId.hashCode() : 0;
    result = 31 * result + (previousId != null ? previousId.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("NextAndPreviousNewsIdTO{");
    sb.append("nextId=").append(nextId);
    sb.append(", previousId=").append(previousId);
    sb.append('}');
    return sb.toString();
  }
}
