<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<spring:url value="tagName" var="tagName"/>
<spring:url value="${pageContext.request.contextPath}/updatetag" var="updateTag"/>
<div class="col-sm-10">
  <c:forEach items="${tags}" var="tag">
    <form:form class="form-inline" role="form" method="post" modelAttribute="tag">
      <div class="form-group">
        <label>Tag</label>
        <form:input path="tagId" type="hidden" value="${tag.tagId}"/>
        <form:input id="tag${tag.tagId}" class="form-control" value="${tag.tagName}" path="${tagName}"
                    disabled="true"/>
        <c:if test="${tagIdError eq tag.tagId}">
          <c:forEach items="${errors}" var="error">
            ${error}
          </c:forEach>
        </c:if>
      </div>

      <button id="update${tag.tagId}" class="btn btn-primary hidden" formaction="${updateTag}">Update
      </button>
      <button id="delete${tag.tagId}" class="btn btn-danger hidden"
              formaction="${pageContext.request.contextPath}/deletetag/${tag.tagId}">Delete
      </button>
      <button id="cancel${tag.tagId}" class="btn btn-danger hidden" type="button" onclick="hideTagButtons(this)">
        Cancel
      </button>
      <button id="edit${tag.tagId}" class="btn btn-primary" type="button" onclick="showTagButtons(this)">Edit
      </button>
    </form:form>

  </c:forEach>
  <form:form class="form-inline" role="form" action="${pageContext.request.contextPath}/addtag" method="post"
             modelAttribute="addTag">
    <div class="form-group">
      <label>Add tag</label>
      <form:input class="form-control" path="${tagName}"/>
      <c:forEach items="${errors}" var="error">
        ${error}
      </c:forEach>
      <button class="btn btn-primary tag-action-button">Add tag</button>
    </div>
  </form:form>
</div>
