package by.epam.newsmanagmentapp.dao.impl;

import by.epam.newsmanagmentapp.dao.TagDAO;
import by.epam.newsmanagmentapp.domain.bean.Tag;
import by.epam.newsmanagmentapp.exceptions.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-application-context.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
@DatabaseSetup(value = "classpath:by/epam/newsmanagmentapp/dao/TagDAOTest.xml", type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = "classpath:by/epam/newsmanagmentapp/dao/TagDAOTest.xml", type = DatabaseOperation.DELETE_ALL)
public class TagDAOTest {

    @Autowired
    private TagDAO tagDAO;

    @BeforeClass
    public static void setLocale() {
        Locale.setDefault(Locale.ENGLISH);
    }

    @Test
    public void testFindAll() throws DAOException {
        final int size = 3;
        List<Tag> tags = tagDAO.findAll();
        assertEquals(size, tags.size());
    }

    @Test
    public void testInsert() throws DAOException {
        final String tagName = "New tag";
        Tag tag = new Tag();
        tag.setTagName(tagName);
        Long tagId = tagDAO.insert(tag);
        Tag actualTag = tagDAO.getByKey(tagId);
        assertEquals(tagId, actualTag.getTagId());
    }

    @Test
    public void testDelete() throws DAOException {
        final Long id = 3L;
        final int size = 2;
        tagDAO.delete(id);
        List<Tag> tags = tagDAO.findAll();
        assertEquals(size, tags.size());
    }

    @Test
    public void testGetByKey() throws DAOException {
        final Long id = 1L;
        Tag tag = tagDAO.getByKey(id);
        assertEquals(id, tag.getTagId());
    }

    @Test
    public void testUpdate() throws DAOException {
        final String TAG_NAME = "tag";
        final Long ID = 1L;
        Tag tag = tagDAO.getByKey(ID);
        tag.setTagName(TAG_NAME);
        tagDAO.update(tag);
        Tag actual = tagDAO.getByKey(ID);
        assertEquals(tag, actual);
    }

}
