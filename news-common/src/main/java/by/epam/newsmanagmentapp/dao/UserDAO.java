package by.epam.newsmanagmentapp.dao;

import by.epam.newsmanagmentapp.domain.bean.User;
import by.epam.newsmanagmentapp.exceptions.DAOException;

/**
 * Describes specific operations which can be performed with {@link User} objects in the database.
 */
public interface UserDAO extends GenericDAO<User> {
  User getUserInfo(String userLogin) throws DAOException;
}
