package by.epam.newsmanagmentapp.controller.commands.impl;

import by.epam.newsmanagmentapp.controller.commands.CommandException;
import by.epam.newsmanagmentapp.controller.commands.ICommand;
import by.epam.newsmanagmentapp.domain.bean.SearchCriteria;
import by.epam.newsmanagmentapp.domain.to.NewsTO;
import by.epam.newsmanagmentapp.domain.to.NextAndPreviousNewsIdTO;
import by.epam.newsmanagmentapp.exceptions.ServiceException;
import by.epam.newsmanagmentapp.service.ComplexNewsService;
import by.epam.newsmanagmentapp.service.NewsService;
import by.epam.newsmanagmentapp.utils.Action;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Component
public class SingleNews implements ICommand {

  @Autowired
  private NewsService newsService;

  @Autowired
  private ComplexNewsService complexNewsService;

  private static final String NEXT_ID = "nextId";
  private static final String PREVIOUS_ID = "previousId";
  private static final String NEWS_ID_VARIABLE = "id";
  private static final String SINGLE_NEWS_TO = "newsTO";
  private static final String SEARCH_CRITERIA = "searchCriteria";
  private static final String ACTION_PARAM = "action";
  private static final String PATH = "/singleNews";

  @Override
  public String execute(HttpServletRequest request) throws CommandException {
    HttpSession session = request.getSession();
    SearchCriteria searchCriteria;
    NewsTO newsTO;
    Long newsId = null;
    NextAndPreviousNewsIdTO nextAndPreviousNewsIdTO;

    if (request.getParameter(NEWS_ID_VARIABLE) != null) {
      newsId = Long.valueOf(request.getParameter(NEWS_ID_VARIABLE));
    }
    try {
      if (session.getAttribute(SEARCH_CRITERIA) != null) {
        searchCriteria = (SearchCriteria) session.getAttribute(SEARCH_CRITERIA);
        nextAndPreviousNewsIdTO = newsService.getNextAndPreviousNewsIdWithSearchCriteria(searchCriteria, newsId);
      } else {
        nextAndPreviousNewsIdTO = newsService.getNextAndPreviousNewsId(newsId);
      }
      newsTO = complexNewsService.loadNewsTO(newsId);
    } catch (ServiceException e) {
      throw new CommandException(e);
    }

    if (nextAndPreviousNewsIdTO != null) {
      request.setAttribute(NEXT_ID, nextAndPreviousNewsIdTO.getNextId());
      request.setAttribute(PREVIOUS_ID, nextAndPreviousNewsIdTO.getPreviousId());
    }
    request.setAttribute(SINGLE_NEWS_TO, newsTO);
    request.setAttribute(ACTION_PARAM, Action.FORWARD);
    return PATH;
  }
}
