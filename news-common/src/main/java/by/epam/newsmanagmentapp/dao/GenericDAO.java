package by.epam.newsmanagmentapp.dao;

import by.epam.newsmanagmentapp.exceptions.DAOException;

import java.util.List;

/**
 * Basic interface which provides C.R.U.D. operations with {@link Entity} objects.
 *
 * @param <Entity> Generic type of object.
 */
public interface GenericDAO<Entity> {

    /**
     * Loads all {@link Entity} records from database.
     *
     * @return List of ${@link Entity} objects from database.
     * @throws DAOException if there were errors during reading records from database.
     */
    List<Entity> findAll() throws DAOException;

    /**
     * Create record into database with values specified in input {@link Entity} object.
     *
     * @param entity ${@link Entity} object which is need to be created in database.
     * @return Identifier of inserted record.
     * @throws DAOException if there were error during inserting data into database.
     */
    Long insert(Entity entity) throws DAOException;

    /**
     * Delete database record with identifier specified as parameter.
     *
     * @param id Identifier of record which is must be deleted.
     * @throws DAOException if there were errors during deleting record.
     */
    void delete(Long id) throws DAOException;

    /**
     * Return object of {@link Entity} appropriate record in database with identifier specified as parameter.
     *
     * @param id Identifier of record which is must be returned.
     * @return ${@link Entity} object from database.
     * @throws DAOException          if there were errors during reading records from database.
     */
    Entity getByKey(Long id) throws DAOException;

    /**
     * Updates record in database with values specified in input {@link Entity} object.
     *
     * @param entity {@link Entity} object which is need to be updated in database.
     * @throws DAOException if there were errors during updating entity record in database.
     */
    void update(Entity entity) throws DAOException;
}
