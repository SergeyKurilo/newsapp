package by.epam.newsmanagmentapp.service.impl;

import by.epam.newsmanagmentapp.dao.CommentDAO;
import by.epam.newsmanagmentapp.domain.bean.Comment;
import by.epam.newsmanagmentapp.exceptions.DAOException;
import by.epam.newsmanagmentapp.exceptions.ServiceException;
import by.epam.newsmanagmentapp.service.CommentService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Implementation of {@link CommentService}.
 */
@Component
@Transactional
public class CommentServiceImpl implements CommentService {

  private static final Logger LOGGER = LogManager.getLogger(CommentServiceImpl.class);

  @Autowired
  private CommentDAO commentDAO;

  /**
   * Implementation of {@link CommentService#deleteByNewsId(Long)} method.
   */
  @Override
  public void deleteByNewsId(Long id) throws ServiceException {
    try {
      commentDAO.deleteByNewsId(id);
    } catch (DAOException e) {
      LOGGER.error("Delete comment node by news ID: " + id + " service error");
      throw new ServiceException(e);
    }
  }

  /**
   * Implementation of {@link CommentService#getCommentsByNewsId(Long)} method.
   */
  @Override
  public List<Comment> getCommentsByNewsId(Long id) throws ServiceException {
    try {
      return commentDAO.getCommentsByNewsId(id);
    } catch (DAOException e) {
      LOGGER.error("Load comments by news ID: " + id + " service error");
      throw new ServiceException(e);
    }
  }

  /**
   * Implementation of {@link CommentService#findAll()} method.
   */
  @Override
  public List<Comment> findAll() throws ServiceException {
    try {
      return commentDAO.findAll();
    } catch (DAOException e) {
      LOGGER.error("Load all comments service error");
      throw new ServiceException(e);
    }
  }

  /**
   * Implementation of {@link CommentService#insert(Object)} method.
   */
  @Override
  public Long insert(Comment comment) throws ServiceException {
    try {
      return commentDAO.insert(comment);
    } catch (DAOException e) {
      LOGGER.error("Create comment node service error");
      throw new ServiceException(e);
    }
  }

  /**
   * Implementation of {@link CommentService#delete(Long)} method.
   */
  @Override
  public void delete(Long id) throws ServiceException {
    try {
      commentDAO.delete(id);
    } catch (DAOException e) {
      LOGGER.error("Removing comment service error");
      throw new ServiceException(e);
    }
  }

  /**
   * Implementation of {@link CommentService#getByKey(Long)} method.
   */
  @Override
  public Comment getByKey(Long id) throws ServiceException {
    try {
      return commentDAO.getByKey(id);
    } catch (DAOException e) {
      LOGGER.error("Find comment node by ID: " + id + " service error");
      throw new ServiceException(e);
    }
  }

  /**
   * Implementation of {@link CommentService#update(Object)} method.
   */
  @Override
  public void update(Comment comment) throws ServiceException {
    try {
      commentDAO.update(comment);
    } catch (DAOException e) {
      LOGGER.error("Update comment node service error");
      throw new ServiceException(e);
    }
  }
}

