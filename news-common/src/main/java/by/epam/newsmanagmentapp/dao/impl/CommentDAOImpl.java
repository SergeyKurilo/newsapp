package by.epam.newsmanagmentapp.dao.impl;

import by.epam.newsmanagmentapp.dao.CommentDAO;
import by.epam.newsmanagmentapp.dao.util.DBUtil;
import by.epam.newsmanagmentapp.domain.bean.Comment;
import by.epam.newsmanagmentapp.exceptions.DAOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of {@link CommentDAO} using JDBC
 */
@Component
public class CommentDAOImpl implements CommentDAO {

    @Autowired
    private DataSource dataSource;

    private static final String COMMENT_ID = "CMT_ID";
    private static final String NEWS_ID = "CMT_NWS_ID";
    private static final String COMMENT_TEXT = "CMT_TEXT";
    private static final String COMMENT_CREATION_DATE = "CMT_CREATION_DATE";
    private static final String SQL_SELECT_FROM_COMMENTS = "SELECT CMT_ID, CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE FROM COMMENTS";
    private static final String SQL_INSERT_COMMENT = "INSERT INTO COMMENTS(CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE) VALUES (?, ?, ?)";
    private static final String SQL_DELETE_COMMENT_BY_COMMENT_ID = "DELETE FROM COMMENTS WHERE CMT_ID=?";
    private static final String SQL_SELECT_COMMENT_BY_COMMENT_ID = "SELECT CMT_ID, CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE FROM  COMMENTS WHERE CMT_ID=?";
    private static final String SQL_UPDATE_COMMENT = "UPDATE COMMENTS SET  CMT_TEXT=? WHERE CMT_ID = ?";
    private static final String SQL_DELETE_COMMENT_BY_NEWS_ID = "DELETE FROM COMMENTS WHERE CMT_NWS_ID=?";
    private static final String SQL_SELECT_COMMENTS_BY_NEWS_ID = "SELECT CMT_ID, CMT_NWS_ID, CMT_TEXT, CMT_CREATION_DATE FROM COMMENTS WHERE CMT_NWS_ID=?";


    private CommentDAOImpl() {
    }

    /**
     * Implementation of {@link CommentDAO#findAll()} method using simple JDBC.
     */
    @Override
    public List<Comment> findAll() throws DAOException {
        List<Comment> comments = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_FROM_COMMENTS);
            rs = preparedStatement.executeQuery();

            while (rs.next()) {
                Comment comment = new Comment();
                comment.setCommentId(rs.getLong(COMMENT_ID));
                comment.setNewsId(rs.getLong(NEWS_ID));
                comment.setCommentText(rs.getString(COMMENT_TEXT));
                comment.setCreationDate(rs.getTimestamp(COMMENT_CREATION_DATE));
                comments.add(comment);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
        return comments;
    }

    /**
     * Implementation of {@link CommentDAO#insert(Object)} method using simple JDBC.
     */
    @Override
    public Long insert(Comment comment) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        Long id = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_INSERT_COMMENT, new String[]{COMMENT_ID});
            preparedStatement.setLong(1, comment.getNewsId());
            preparedStatement.setString(2, comment.getCommentText());
            preparedStatement.setTimestamp(3, comment.getCreationDate());
            preparedStatement.executeUpdate();

            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    id = generatedKeys.getLong(1);
                } else {
                    throw new DAOException("Creating comment failed, no ID obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, null);
        }
        return id;
    }

    /**
     * Implementation of {@link CommentDAO#delete(Long)} method using simple JDBC.
     */
    @Override
    public void delete(Long id) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_DELETE_COMMENT_BY_COMMENT_ID);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
    }

    /**
     * Implementation of {@link CommentDAO#getByKey(Long)} method using simple JDBC.
     */
    @Override
    public Comment getByKey(Long id) throws DAOException {
        Comment comment = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_COMMENT_BY_COMMENT_ID);
            preparedStatement.setLong(1, id);
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                comment = new Comment();
                comment.setCommentId(rs.getLong(COMMENT_ID));
                comment.setNewsId(rs.getLong(NEWS_ID));
                comment.setCommentText(rs.getString(COMMENT_TEXT));
                comment.setCreationDate(rs.getTimestamp(COMMENT_CREATION_DATE));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
        return comment;
    }

    /**
     * Implementation of {@link CommentDAO#update(Object)} method using simple JDBC.
     */
    @Override
    public void update(Comment comment) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_UPDATE_COMMENT);
            preparedStatement.setString(1, comment.getCommentText());
            preparedStatement.setLong(2, comment.getCommentId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
    }

    /**
     * Implementation of {@link CommentDAO#deleteByNewsId(Long)} method using simple JDBC.
     */
    @Override
    public void deleteByNewsId(Long id) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_DELETE_COMMENT_BY_NEWS_ID);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
    }

    /**
     * Implementation of {@link CommentDAO#getCommentsByNewsId(Long)} method using simple JDBC.
     */
    @Override
    public List<Comment> getCommentsByNewsId(Long id) throws DAOException {
        List<Comment> comments = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_COMMENTS_BY_NEWS_ID);
            preparedStatement.setLong(1, id);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Comment comment = new Comment();
                comment.setCommentId(rs.getLong(COMMENT_ID));
                comment.setNewsId(rs.getLong(NEWS_ID));
                comment.setCommentText(rs.getString(COMMENT_TEXT));
                comment.setCreationDate(rs.getTimestamp(COMMENT_CREATION_DATE));
                comments.add(comment);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
        return comments;
    }

}
