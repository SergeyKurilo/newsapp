package by.epam.newsmanagmentapp.controller;

import by.epam.newsmanagmentapp.domain.bean.Author;
import by.epam.newsmanagmentapp.domain.bean.Comment;
import by.epam.newsmanagmentapp.domain.bean.SearchCriteria;
import by.epam.newsmanagmentapp.domain.bean.Tag;
import by.epam.newsmanagmentapp.domain.to.AddNewsTO;
import by.epam.newsmanagmentapp.domain.to.NewsTO;
import by.epam.newsmanagmentapp.domain.to.NewsToDeleteTO;
import by.epam.newsmanagmentapp.domain.to.NextAndPreviousNewsIdTO;
import by.epam.newsmanagmentapp.exceptions.ServiceException;
import by.epam.newsmanagmentapp.service.AuthorService;
import by.epam.newsmanagmentapp.service.ComplexNewsService;
import by.epam.newsmanagmentapp.service.NewsService;
import by.epam.newsmanagmentapp.service.TagService;
import by.epam.newsmanagmentapp.validation.NewsValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class NewsController {

    @Autowired
    private ComplexNewsService complexNewsService;

    @Autowired
    private NewsService newsService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private TagService tagService;

    @Autowired
    private NewsValidator newsFormValidator;

    private static final String ALL_NEWS_MAPPING = "/allnews";
    private static final String PAGE_NUMBER_VARIABLE = "page";
    private static final String NEWS_MAPPING = "/news/{id}";
    private static final String NEWS_ID_VARIABLE = "id";
    private static final String SEARCH_RESULT_MAPPING = "/searchresult";
    private static final String SEARCH_CRITERIA_VARIABLE = "searchCriteria";
    private static final String ADD_NEWS_MAPPING = "/addnews";
    private static final String ADD_NEWS_MODEL = "addNewsTO";
    private static final String DELETE_NEWS_TO_ATTRIBUTE = "newsToDelete";
    private static final String DELETE_NEWS_MAPPING = "/deleteNews";
    private static final String EDIT_NEWS_MAPPING = "/editNews/{id}";
    private static final String ADD_VIEW_NAME = "addnews";
    private static final String NEWS_VIEW_NAME = "news";
    private static final String ALL_NEWS_VIEW_NAME = "allnews";
    private static final String RESET_MAPPING = "/deleteCriteria";
    private static final String TAGS_LIST = "tagsList";
    private static final String NEWS_TO = "newsTOs";
    private static final String SINGLE_NEWS_TO = "newsTO";
    private static final String NEWS_PREFIX = "/news/";
    private static final String AUTHORS_LIST = "authorsList";
    private static final String ADD_COMMENT_MODEL = "comment";
    private static final String EDIT_VIEW_NAME = "editNews";
    private static final String NUMBER_OF_PAGES = "numberOfPages";
    private static final String SEARCH_CRITERIA = "searchCriteria";
    private static final String REDIRECT_PREFIX = "redirect:";
    private static final String URL_PAGE_ATTRIBUTE = "?page=";
    private static final String NEXT_ID = "nextId";
    private static final String PREVIOUS_ID = "previousId";
    private static final String ALL_NEWS_ON_FIRST_PAGE_MAPPING = "/allnews?page=1";

    @InitBinder(ADD_NEWS_MODEL)
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(newsFormValidator);
    }

    @RequestMapping(value = ALL_NEWS_MAPPING, method = RequestMethod.GET)
    public ModelAndView getNewsForPage(ModelAndView modelAndView, @RequestParam(PAGE_NUMBER_VARIABLE) Long numberOfPage, HttpSession session) throws ServiceException {
        modelAndView.setViewName(ALL_NEWS_VIEW_NAME);
        if (numberOfPage == null) {
            numberOfPage = 1L;
        }
        if (session.getAttribute(SEARCH_CRITERIA) != null) {
            return new ModelAndView(REDIRECT_PREFIX + SEARCH_RESULT_MAPPING + URL_PAGE_ATTRIBUTE + numberOfPage);
        }
        List<Tag> tags = tagService.findAll();
        List<Author> authors = authorService.getNotExpiredAuthors();
        List<NewsTO> newsTOs = complexNewsService.loadPage(numberOfPage);
        Long numberOfPages = newsService.numberOfPages();
        fillModelAndView(modelAndView, newsTOs, authors, tags, numberOfPages);
        return modelAndView;
    }

    @RequestMapping(value = ADD_NEWS_MAPPING, method = RequestMethod.GET)
    public ModelAndView getNewsForm() throws ServiceException {
        ModelAndView modelAndView = new ModelAndView(ADD_VIEW_NAME);
        modelAndView.addObject(TAGS_LIST, tagService.findAll());
        modelAndView.addObject(AUTHORS_LIST, authorService.getNotExpiredAuthors());
        modelAndView.addObject(ADD_NEWS_MODEL, new AddNewsTO());
        return modelAndView;
    }

    @RequestMapping(value = NEWS_MAPPING, method = RequestMethod.GET)
    public ModelAndView getSingleNews(@PathVariable(NEWS_ID_VARIABLE) Long newsId, HttpSession session) throws ServiceException {
        ModelAndView modelAndView = new ModelAndView(NEWS_VIEW_NAME);
        Comment comment = new Comment();
        comment.setNewsId(newsId);
        modelAndView.addObject(ADD_COMMENT_MODEL, comment);
        NextAndPreviousNewsIdTO nextAndPreviousNewsIdTO;
        SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute(SEARCH_CRITERIA);
        if (searchCriteria != null) {
            nextAndPreviousNewsIdTO = newsService.getNextAndPreviousNewsIdWithSearchCriteria(searchCriteria, newsId);
        } else {
            nextAndPreviousNewsIdTO = newsService.getNextAndPreviousNewsId(newsId);
        }
        NewsTO newsTO = complexNewsService.loadNewsTO(newsId);
        if (nextAndPreviousNewsIdTO != null) {
            modelAndView.addObject(NEXT_ID, nextAndPreviousNewsIdTO.getNextId());
            modelAndView.addObject(PREVIOUS_ID, nextAndPreviousNewsIdTO.getPreviousId());
        }
        modelAndView.addObject(SINGLE_NEWS_TO, newsTO);
        return modelAndView;
    }

    @RequestMapping(value = ADD_NEWS_MAPPING, method = RequestMethod.POST)
    public ModelAndView addNews(@ModelAttribute(ADD_NEWS_MODEL) @Validated AddNewsTO newsTO, BindingResult result, HttpSession session) throws ServiceException {
        ModelAndView modelAndView = new ModelAndView();
        if (result.hasErrors()) {
            modelAndView.setViewName(ADD_VIEW_NAME);
            modelAndView.addObject(TAGS_LIST, tagService.findAll());
            modelAndView.addObject(AUTHORS_LIST, authorService.getNotExpiredAuthors());
            modelAndView.addObject(ADD_NEWS_MODEL, newsTO);
            return modelAndView;
        }
        Long newsId = fillAndSaveNews(newsTO);
        session.removeAttribute(SEARCH_CRITERIA_VARIABLE);
        String REDIRECT_URL = REDIRECT_PREFIX + NEWS_PREFIX + newsId;
        modelAndView.setViewName(REDIRECT_URL);
        return modelAndView;
    }


    @RequestMapping(value = EDIT_NEWS_MAPPING, method = RequestMethod.GET)
    public ModelAndView editNewsPage(@PathVariable(NEWS_ID_VARIABLE) Long id) throws ServiceException {
        ModelAndView modelAndView = new ModelAndView(EDIT_VIEW_NAME);
        NewsTO newsTO = complexNewsService.loadNewsTO(id);
        modelAndView.addObject(ADD_NEWS_MODEL, fillInEditNews(newsTO));
        modelAndView.addObject(TAGS_LIST, tagService.findAll());
        modelAndView.addObject(AUTHORS_LIST, authorService.getNotExpiredAuthors());
        return modelAndView;

    }

    @RequestMapping(value = EDIT_NEWS_MAPPING, method = RequestMethod.POST)
    public ModelAndView editNews(@ModelAttribute(ADD_NEWS_MODEL) @Validated AddNewsTO addNewsTO, BindingResult bindingResult, HttpSession session) throws ServiceException {
        ModelAndView modelAndView = new ModelAndView();
        Long newsId = addNewsTO.getNews().getNewsId();
        if (bindingResult.hasErrors()) {
            return editNewsPage(addNewsTO.getNews().getNewsId());
        }
        fillAndUpdateNews(addNewsTO);
        session.removeAttribute(SEARCH_CRITERIA_VARIABLE);
        String REDIRECT_URL = REDIRECT_PREFIX + NEWS_PREFIX + newsId;
        modelAndView.setViewName(REDIRECT_URL);
        return modelAndView;
    }

    @RequestMapping(value = SEARCH_RESULT_MAPPING, method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView newsesWithSearchCriteria(@ModelAttribute(SEARCH_CRITERIA) SearchCriteria searchCriteria, @RequestParam(PAGE_NUMBER_VARIABLE) Long numberOfPage, HttpSession session) throws ServiceException {
        ModelAndView modelAndView = new ModelAndView(ALL_NEWS_VIEW_NAME);
        List<NewsTO> newsTOs;
        Long numberOfPages;
        if (session.getAttribute(SEARCH_CRITERIA) != null) {
            searchCriteria = (SearchCriteria) session.getAttribute(SEARCH_CRITERIA);
        }
        newsTOs = complexNewsService.loadPageWithCriteria(searchCriteria, numberOfPage);
        numberOfPages = newsService.numberOfPagesWithCriteria(searchCriteria);
        modelAndView.addObject(DELETE_NEWS_TO_ATTRIBUTE, new NewsToDeleteTO());
        modelAndView.addObject(TAGS_LIST, tagService.findAll());
        modelAndView.addObject(AUTHORS_LIST, authorService.getNotExpiredAuthors());
        session.setAttribute(SEARCH_CRITERIA, searchCriteria);
        modelAndView.addObject(NEWS_TO, newsTOs);
        modelAndView.addObject(NUMBER_OF_PAGES, numberOfPages);
        return modelAndView;
    }

    @RequestMapping(value = DELETE_NEWS_MAPPING, method = RequestMethod.POST)
    public String deleteNewses(@ModelAttribute(DELETE_NEWS_TO_ATTRIBUTE) NewsToDeleteTO newsTO) throws ServiceException {
        for (Long newsId : newsTO.getNewsIds()) {
            complexNewsService.deleteNewsWithAllLinks(newsId);
        }
        return REDIRECT_PREFIX + ALL_NEWS_ON_FIRST_PAGE_MAPPING;
    }


    @RequestMapping(value = RESET_MAPPING, method = RequestMethod.POST)
    public ModelAndView resetCriteria(final HttpSession session) {
        session.removeAttribute(SEARCH_CRITERIA_VARIABLE);
        ModelAndView modelAndView = new ModelAndView();
        String path = REDIRECT_PREFIX + ALL_NEWS_ON_FIRST_PAGE_MAPPING;
        modelAndView.setViewName(path);
        return modelAndView;
    }

    private void fillModelAndView(ModelAndView modelAndView, List<NewsTO> news, List<Author> authors,
                                  List<Tag> tags, Long numberOfPages) {
        modelAndView.addObject(SEARCH_CRITERIA, new SearchCriteria());
        modelAndView.addObject(DELETE_NEWS_TO_ATTRIBUTE, new NewsToDeleteTO());
        modelAndView.addObject(NEWS_TO, news);
        modelAndView.addObject(AUTHORS_LIST, authors);
        modelAndView.addObject(TAGS_LIST, tags);
        modelAndView.addObject(NUMBER_OF_PAGES, numberOfPages);
    }

    private Long fillAndSaveNews(AddNewsTO addNewsTO) throws ServiceException {
        Timestamp timestamp = Timestamp.valueOf(LocalDateTime.now());
        addNewsTO.getNews().setCreationDate(timestamp);
        addNewsTO.getNews().setModificationDate(Date.valueOf(LocalDate.now()));
        Long newsId = complexNewsService.createNewsWithAuthorAndTags(addNewsTO.getNews(), addNewsTO.getAuthorId(), addNewsTO.getTags());
        return newsId;
    }

    private void fillAndUpdateNews(AddNewsTO addNewsTO) throws ServiceException {
        addNewsTO.getNews().setModificationDate(Date.valueOf(LocalDate.now()));
        newsService.update(addNewsTO.getNews());
        newsService.deleteNewsAuthorLink(addNewsTO.getNews().getNewsId());
        newsService.deleteNewsTagLink(addNewsTO.getNews().getNewsId());
        Long author = addNewsTO.getAuthorId();
        Long[] tags = addNewsTO.getTags();
        newsService.createNewsAuthorLink(addNewsTO.getNews().getNewsId(), author);
        newsService.createNewsTagsLinks(addNewsTO.getNews().getNewsId(), tags);
    }

    private AddNewsTO fillInEditNews(NewsTO newsTO) {
        AddNewsTO addNewsTO = new AddNewsTO();
        addNewsTO.setNews(newsTO.getNews());
        addNewsTO.setAuthorId(newsTO.getAuthor().getAuthorId());
        List<Long> tagsId = newsTO.getTags().stream().map(Tag::getTagId).collect(Collectors.toList());
        Long[] tagsIdArray = tagsId.toArray(new Long[tagsId.size()]);
        addNewsTO.setTags(tagsIdArray);
        return addNewsTO;
    }

}
