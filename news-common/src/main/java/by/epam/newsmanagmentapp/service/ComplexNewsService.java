package by.epam.newsmanagmentapp.service;

import by.epam.newsmanagmentapp.domain.bean.News;
import by.epam.newsmanagmentapp.domain.bean.SearchCriteria;
import by.epam.newsmanagmentapp.domain.to.NewsTO;
import by.epam.newsmanagmentapp.exceptions.ServiceException;

import java.util.List;

/**
 * Provides specific operations with news,new's authors,new's comments,and new's tags.
 */
public interface ComplexNewsService {
    /**
     * Create new news with author and tags.
     *
     * @param news     ${@link News} object.
     * @param authorId identifier of author.
     * @param tags     tags identifiers.
     * @return identifier of news.
     * @throws ServiceException if ${@link by.epam.newsmanagmentapp.exceptions.DAOException} was thrown.
     */
    Long createNewsWithAuthorAndTags(News news, Long authorId, Long... tags) throws ServiceException;

    /**
     * Delete news with comments and news wires with tags and author.
     *
     * @param newsId identifier of news.
     * @throws ServiceException if ${@link by.epam.newsmanagmentapp.exceptions.ServiceException} was caused.
     */
    void deleteNewsWithAllLinks(Long newsId) throws ServiceException;

    /**
     * Load single ${@link NewsTO} object with specified news identifier.
     *
     * @param newsId identifier of news.
     * @return {@link NewsTO} object.
     * @throws ServiceException if ${@link by.epam.newsmanagmentapp.exceptions.ServiceException} was caused.
     */
    NewsTO loadNewsTO(Long newsId) throws ServiceException;

    /**
     * Load all ${@link NewsTO} objects.
     *
     * @return List of ${@link NewsTO} objects.
     * @throws ServiceException if ${@link by.epam.newsmanagmentapp.exceptions.ServiceException} was caused.
     */
    List<NewsTO> loadAllNewsTO() throws ServiceException;

    /**
     * Load all ${@link NewsTO} objects corresponding to criteria specified in passed ${@link SearchCriteria} object.
     *
     * @param criteria ${@link SearchCriteria} object.
     * @return List of ${@link NewsTO} objects.
     * @throws ServiceException if ${@link by.epam.newsmanagmentapp.exceptions.ServiceException} was caused.
     */
    List<NewsTO> findBySearchCriteria(SearchCriteria criteria) throws ServiceException;


    /**
     * Load all ${@link NewsTO} objects.
     *
     * @return List of ${@link NewsTO} objects.
     * @throws ServiceException if ${@link by.epam.newsmanagmentapp.exceptions.ServiceException} was caused.
     */
    List<NewsTO> loadPage(Long numberOfPage) throws ServiceException;

    List<NewsTO> loadPageWithCriteria(SearchCriteria searchCriteria, Long numberOfPage) throws ServiceException;
}
