package by.epam.newsmanagmentapp.util;

import by.epam.newsmanagmentapp.exceptions.ServiceException;
import org.springframework.stereotype.Component;

public class PaginationUtil {

    private static final int NUMBER_OF_NEWS_PER_PAGE = 3;

    public static Long numberOfPages(Long numberOfNews) throws ServiceException {
        if (numberOfNews % NUMBER_OF_NEWS_PER_PAGE > 0) {
            return numberOfNews / NUMBER_OF_NEWS_PER_PAGE + 1;
        } else {
            return numberOfNews / NUMBER_OF_NEWS_PER_PAGE;
        }
    }

    public static Long getLeftSelectBound(Long numberOfPage) throws ServiceException {
        Long from = (numberOfPage - 1) * NUMBER_OF_NEWS_PER_PAGE + 1;
        return from;
    }

    public static Long getRightSelectBound(Long numberOfPage) throws ServiceException {
        Long to = NUMBER_OF_NEWS_PER_PAGE * numberOfPage;
        return to;
    }
}

