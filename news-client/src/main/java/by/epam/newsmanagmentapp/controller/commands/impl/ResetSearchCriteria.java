package by.epam.newsmanagmentapp.controller.commands.impl;

import by.epam.newsmanagmentapp.controller.commands.ICommand;
import by.epam.newsmanagmentapp.utils.Action;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Component
public class ResetSearchCriteria implements ICommand {

  private static final String TARGET = "/ClientController?page=1";
  private static final String CRITERIA = "searchCriteria";
  private static final String ACTION_PARAM = "action";

  @Override
  public String execute(HttpServletRequest request) {
    HttpSession session = request.getSession();
    session.removeAttribute(CRITERIA);
    request.setAttribute(ACTION_PARAM, Action.REDIRECT);
    return TARGET;
  }
}
