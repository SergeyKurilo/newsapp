<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="col-sm-10">
  <spring:url value="${pageContext.request.contextPath}/addnews" var="action"/>
  <c:if test="${not empty addNewsTO.news}">
    <spring:url value="${pageContext.request.contextPath}/editNews/${addNewsTO.news.newsId}" var="action"/>
  </c:if>
  <form:form class="form-horizontal" method="post"
             modelAttribute="addNewsTO" action="${action}">

    <form:hidden path="news.newsId"/>

    <label class="col-sm-2 control-label">Title</label>
    <div class="col-sm-10" id="form-news">
      <form:input path="news.title" type="text" class="form-control" placeholder="Title"/>
      <form:errors path="news.title" class="control-label"/>
    </div>

    <label class="col-sm-2 control-label">Brief</label>
    <div class="col-sm-10" id="form-news">
      <form:textarea path="news.shortText" type="text" class="form-control"
                     id="title" placeholder="Brief" rows="3" cols="70"/>
      <form:errors path="news.shortText" class="control-label"/>
    </div>

    <label class="col-sm-2 control-label">Content</label>
    <div class="col-sm-10" id="form-news">
      <form:textarea path="news.fullText" type="text" class="form-control"
                     id="title" placeholder="Content" rows="5" cols="70"/>
      <form:errors path="news.fullText" class="control-label"/>
    </div>

    <label class="col-sm-2 control-label">Tags</label>
    <div class="col-sm-10" id="form-news">
      <form:select path="tags" multiple="multiple">
        <form:options items="${tagsList}" itemValue="tagId" itemLabel="tagName"/>
      </form:select>
    </div>
    <form:errors path="tags" class="control-label"/>

    <label class="col-sm-2 control-label">Authors</label>
    <div class="col-sm-10" id="form-news">
      <form:select path="authorId">
        <form:option value="" label="Select author"/>
        <form:options items="${authorsList}" itemValue="authorId" itemLabel="authorName"/>
      </form:select>
      <form:errors path="authorId" class="control-label"/>
    </div>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <c:choose>
          <c:when test="${addNewsTO.news.newsId eq null}">
            <button type="submit" class="btn-lg btn-primary pull-left">Add news
            </button>
          </c:when>
          <c:otherwise>
            <button type="submit" class="btn-lg btn-primary pull-left">Update news
            </button>
          </c:otherwise>
        </c:choose>
      </div>
    </div>
  </form:form>

</div>

