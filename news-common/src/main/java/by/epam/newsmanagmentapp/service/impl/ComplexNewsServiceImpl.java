package by.epam.newsmanagmentapp.service.impl;

import by.epam.newsmanagmentapp.domain.bean.*;
import by.epam.newsmanagmentapp.domain.to.NewsTO;
import by.epam.newsmanagmentapp.exceptions.ServiceException;
import by.epam.newsmanagmentapp.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of {@link ComplexNewsService}.
 */
@Component
@Transactional
public class ComplexNewsServiceImpl implements ComplexNewsService {

  @Autowired
  private NewsService newsService;

  @Autowired
  private CommentService commentService;

  @Autowired
  private TagService tagService;

  @Autowired
  private AuthorService authorService;

  /**
   * Implementation of {@link ComplexNewsService#createNewsWithAuthorAndTags(News, Long, Long...)} method.
   */
  @Override
  public Long createNewsWithAuthorAndTags(News news, Long authorId, Long... tags) throws ServiceException {
    Long newsId = newsService.insert(news);
    newsService.createNewsAuthorLink(newsId, authorId);
    newsService.createNewsTagsLinks(newsId, tags);
    return newsId;
  }

  /**
   * Implementation of {@link ComplexNewsService#deleteNewsWithAllLinks(Long)} method.
   */
  @Override
  public void deleteNewsWithAllLinks(Long newsId) throws ServiceException {
    newsService.deleteNewsAuthorLink(newsId);
    newsService.deleteNewsTagLink(newsId);
    commentService.deleteByNewsId(newsId);
    newsService.delete(newsId);
  }

  /**
   * Implementation of {@link ComplexNewsService#loadAllNewsTO()} method.
   */
  @Override
  public NewsTO loadNewsTO(Long newsId) throws ServiceException {
    News news = newsService.getByKey(newsId);
    NewsTO newsTO = new NewsTO();
    newsTO.setNews(news);
    newsTO.setTags(tagService.getTagsByNewsId(newsId));
    Author author = authorService.getAuthorByNewsId(newsId);
    newsTO.setAuthor(author);
    newsTO.setComments(commentService.getCommentsByNewsId(newsId));
    return newsTO;
  }

  /**
   * Implementation of {@link ComplexNewsService#loadAllNewsTO()} method.
   */
  @Override
  public List<NewsTO> loadAllNewsTO() throws ServiceException {
    List<NewsTO> newsTOList = new ArrayList<>();
    List<News> newsList = newsService.findAll();
    for (News news : newsList) {
      NewsTO newsTO = loadNewsTO(news.getNewsId());
      newsTOList.add(newsTO);
    }
    return newsTOList;
  }

  /**
   * Implementation of {@link ComplexNewsService#findBySearchCriteria(SearchCriteria)} method.
   */
  @Override
  public List<NewsTO> findBySearchCriteria(SearchCriteria criteria) throws ServiceException {
    List<NewsTO> newsTOList = new ArrayList<>();
    List<News> newsList = newsService.findBySearchCriteria(criteria);
    for (News news : newsList) {
      newsTOList.add(loadNewsTO(news.getNewsId()));
    }
    return newsTOList;
  }

  @Override
  public List<NewsTO> loadPage(Long numberOfPage) throws ServiceException {
    List<NewsTO> newsTOList = new ArrayList<>();
    List<News> newsList = newsService.loadPage(numberOfPage);
    for (News news : newsList) {
      NewsTO newsTO = loadNewsTO(news.getNewsId());
      newsTOList.add(newsTO);
    }
    return newsTOList;
  }

  @Override
  public List<NewsTO> loadPageWithCriteria(SearchCriteria searchCriteria, Long numberOfPage) throws ServiceException {
    List<NewsTO> newsTOList = new ArrayList<>();
    List<News> newsList = newsService.loadPageWithSearchCriteria(searchCriteria, numberOfPage);
    for (News news : newsList) {
      NewsTO newsTO = loadNewsTO(news.getNewsId());
      newsTOList.add(newsTO);
    }
    return newsTOList;
  }
}
