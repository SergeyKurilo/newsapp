package by.epam.newsmanagmentapp.utils;

import by.epam.newsmanagmentapp.domain.to.UserTO;
import by.epam.newsmanagmentapp.exceptions.ServiceException;
import by.epam.newsmanagmentapp.service.AuthenticationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Collections;

public class UserDetailsServiceImpl implements UserDetailsService {

  private static final Logger LOGGER = LogManager.getLogger(UserDetailsServiceImpl.class);

  private final AuthenticationService authenticationService;

  public UserDetailsServiceImpl(AuthenticationService authenticationService) {
    this.authenticationService = authenticationService;
  }

  @Override
  public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
    UserDetails userDetails = null;
    try {
      UserTO userTO = authenticationService.getUserInfo(s);
      GrantedAuthority authority = new SimpleGrantedAuthority(userTO.getRole().getRoleName());
      userDetails = new User(userTO.getUser().getUserName(),
              userTO.getUser().getPassword(), Collections.singletonList(authority));
    } catch (ServiceException e) {
      LOGGER.error("Authentication service error", e);
    }
    return userDetails;
  }
}
