package by.epam.newsmanagmentapp.dao.impl;

import by.epam.newsmanagmentapp.dao.AuthorDAO;
import by.epam.newsmanagmentapp.dao.util.DBUtil;
import by.epam.newsmanagmentapp.domain.bean.Author;
import by.epam.newsmanagmentapp.exceptions.DAOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of {@link AuthorDAO} using JDBC
 */
@Component
public class AuthorDAOImpl implements AuthorDAO {

    @Autowired
    DataSource dataSource;

    private static final String AUTHOR_ID = "ATR_ID";
    private static final String AUTHOR_NAME = "ATR_NAME";
    private static final String AUTHOR_EXPIRED = "ATR_EXPIRED";

    private static final String SQL_SELECT_ALL_AUTHORS = "SELECT ATR_ID, ATR_NAME, ATR_EXPIRED FROM AUTHORS";
    private static final String SQL_INSERT_AUTHOR = "INSERT INTO AUTHORS(ATR_NAME, ATR_EXPIRED) VALUES (?, ?)";
    private static final String SQL_DELETE_AUTHOR_BY_ID = "DELETE FROM AUTHORS WHERE ATR_ID=?";
    private static final String SQL_SELECT_AUTHOR_BY_AUTHOR_ID = "SELECT ATR_ID, ATR_NAME, ATR_EXPIRED FROM AUTHORS WHERE ATR_ID=?";
    private static final String SQL_UPDATE_AUTHOR = "UPDATE AUTHORS SET ATR_NAME=?, ATR_EXPIRED=? WHERE ATR_ID = ?";
    private static final String SQL_SELECT_AUTHOR_BY_AUTHOR_NAME = "SELECT ATR_ID, ATR_NAME, ATR_EXPIRED FROM AUTHORS WHERE ATR_NAME=?";
    private static final String SQL_SELECT_NOT_EXPIRED_AUTHORS = "SELECT ATR_ID, ATR_NAME, ATR_EXPIRED FROM AUTHORS WHERE ATR_EXPIRED IS NOT NULL";
    private static final String SQL_SELECT_AUTHOR_BY_NEWS_ID = "SELECT ATR_ID, ATR_NAME, ATR_EXPIRED FROM AUTHORS JOIN NEWS_AUTHORS ON AUTHORS.ATR_ID = NEWS_AUTHORS.NAT_ATR_ID WHERE NEWS_AUTHORS.NAT_NEWS_ID=?";

    private AuthorDAOImpl() {
    }

    /**
     * Implementation of {@link AuthorDAO#findAll()} method using simple JDBC.
     */
    @Override
    public List<Author> findAll() throws DAOException {
        List<Author> authors = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_AUTHORS);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Author author = new Author();
                author.setAuthorId(rs.getLong(AUTHOR_ID));
                author.setExpired(rs.getTimestamp(AUTHOR_EXPIRED));
                author.setAuthorName(rs.getString(AUTHOR_NAME));
                authors.add(author);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
        return authors;
    }

    /**
     * Implementation of {@link AuthorDAO#insert(Object)} method using simple JDBC.
     */
    @Override
    public Long insert(Author author) throws DAOException {

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        Long id = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_INSERT_AUTHOR, new String[]{AUTHOR_ID});
            preparedStatement.setString(1, author.getAuthorName());
            preparedStatement.setTimestamp(2, author.getExpired());
            preparedStatement.executeUpdate();

            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    id = generatedKeys.getLong(1);
                } else {
                    throw new DAOException("Creating comment failed, no ID obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement);
        }
        return id;
    }

    /**
     * Implementation of {@link AuthorDAO#delete(Long)} method using simple JDBC.
     */
    @Override
    public void delete(Long id) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_DELETE_AUTHOR_BY_ID);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement);
        }
    }

    /**
     * Implementation of {@link AuthorDAO#getByKey(Long)} method using simple JDBC.
     */
    @Override
    public Author getByKey(Long id) throws DAOException {
        Author author = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_AUTHOR_BY_AUTHOR_ID);
            preparedStatement.setLong(1, id);
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                author = new Author();
                author.setAuthorId(rs.getLong(AUTHOR_ID));
                author.setAuthorName(rs.getString(AUTHOR_NAME));
                author.setExpired(rs.getTimestamp(AUTHOR_EXPIRED));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
        return author;
    }

    /**
     * Implementation of {@link AuthorDAO#update(Object)} method using simple JDBC.
     */
    @Override
    public void update(Author author) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_UPDATE_AUTHOR);
            preparedStatement.setString(1, author.getAuthorName());
            preparedStatement.setTimestamp(2, author.getExpired());
            preparedStatement.setLong(3, author.getAuthorId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement);
        }
    }

    /**
     * Implementation of {@link AuthorDAO#checkAuthor(String)} method using simple JDBC.
     */
    @Override
    public boolean checkAuthor(String authorName) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_AUTHOR_BY_AUTHOR_NAME);
            preparedStatement.setString(1, authorName);
            rs = preparedStatement.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
    }

    /**
     * Implementation of {@link AuthorDAO#getAuthorByName(String)} method using simple JDBC.
     */
    @Override
    public Author getAuthorByName(String authorName) throws DAOException {
        Author author = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_AUTHOR_BY_AUTHOR_NAME);
            preparedStatement.setString(1, authorName);
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                author = new Author();
                author.setAuthorId(rs.getLong(AUTHOR_ID));
                author.setAuthorName(rs.getString(AUTHOR_NAME));
                author.setExpired(rs.getTimestamp(AUTHOR_EXPIRED));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
        return author;
    }

    /**
     * Implementation of {@link AuthorDAO#getNotExpiredAuthors()} method using simple JDBC.
     */
    @Override
    public List<Author> getNotExpiredAuthors() throws DAOException {
        List<Author> authors = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_NOT_EXPIRED_AUTHORS);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Author author = new Author();
                author.setAuthorId(rs.getLong(AUTHOR_ID));
                author.setAuthorName(rs.getString(AUTHOR_NAME));
                author.setExpired(rs.getTimestamp(AUTHOR_EXPIRED));
                authors.add(author);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
        return authors;
    }

    /**
     * Implementation of {@link AuthorDAO#getAuthorByNewsId(Long)} method using simple JDBC.
     */
    @Override
    public Author getAuthorByNewsId(Long newsId) throws DAOException {
        Author author = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_AUTHOR_BY_NEWS_ID);
            preparedStatement.setLong(1, newsId);
            rs = preparedStatement.executeQuery();
            author = new Author();
            author.setAuthorId(rs.getLong(1));
            author.setAuthorName(rs.getString(2));
            author.setExpired(rs.getTimestamp(3));
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
        return author;
    }
}
