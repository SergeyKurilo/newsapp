package by.epam.newsmanagmentapp.exceptions;

/**
 * Wrapper for ${@link java.sql.SQLException}.
 */
public class DAOException extends Exception {

    public DAOException() {
        super();
    }

    public DAOException(String message) {
        super(message);
    }

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

    public DAOException(Throwable cause) {
        super(cause);
    }
}
