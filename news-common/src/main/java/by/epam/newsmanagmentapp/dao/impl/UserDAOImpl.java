package by.epam.newsmanagmentapp.dao.impl;

import by.epam.newsmanagmentapp.dao.UserDAO;
import by.epam.newsmanagmentapp.dao.util.DBUtil;
import by.epam.newsmanagmentapp.domain.bean.User;
import by.epam.newsmanagmentapp.exceptions.DAOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of {@link UserDAO} using JDBC
 */
@Component
public class UserDAOImpl implements UserDAO {

    @Autowired
    DataSource dataSource;

    private static final String USER_ID = "USR_ID";
    private static final String USER_ROLE_ID = "USR_ROLE_ID";
    private static final String USER_NAME = "USR_NAME";
    private static final String USER_LOGIN = "USR_LOGIN";
    private static final String USER_PASSWORD = "USR_PASSWORD";
    private static final String SQL_SELECT_FROM_USERS = "SELECT USR_ID, USR_ROLE_ID, USR_NAME, USR_LOGIN, USR_PASSWORD FROM USERS";
    private static final String SQL_INSERT_USER = "INSERT INTO USERS(USR_ROLE_ID, USR_NAME, USR_LOGIN, USR_PASSWORD) VALUES (?, ?, ?, ?)";
    private static final String SQL_DELETE_USER_BY_USER_ID = "DELETE FROM USERS WHERE USR_ID=?";
    private static final String SQL_SELECT_USER_BY_USER_ID = "SELECT USR_ID, USR_ROLE_ID, USR_NAME, USR_LOGIN, USR_PASSWORD FROM USERS WHERE USR_ID=?";
    private static final String SQL_UPDATE_USER = "UPDATE USERS SET USR_ROLE_ID=?, USR_NAME=?, USR_LOGIN=?, USR_PASSWORD=? WHERE USR_ID = ?";
    private static final String SQL_SELECT_USER_BY_LOGIN = "SELECT USR_ID, USR_ROLE_ID, USR_NAME, USR_LOGIN, USR_PASSWORD FROM USERS WHERE USR_LOGIN=?";

    private UserDAOImpl() {
    }

    /**
     * Implementation of {@link UserDAO#findAll()} method using simple JDBC.
     */
    @Override
    public List<User> findAll() throws DAOException {
        List<User> users = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_FROM_USERS);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                User user = new User();
                user.setUserId(rs.getLong(USER_ID));
                user.setRoleId(rs.getLong(USER_ROLE_ID));
                user.setUserName(rs.getString(USER_NAME));
                user.setLogin(rs.getString(USER_LOGIN));
                user.setPassword(rs.getString(USER_PASSWORD));
                users.add(user);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
        return users;
    }

    /**
     * Implementation of {@link UserDAO#insert(Object)} method using simple JDBC.
     */
    @Override
    public Long insert(User user) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        Long id = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_INSERT_USER, new String[]{USER_ID});
            preparedStatement.setLong(1, user.getRoleId());
            preparedStatement.setString(2, user.getUserName());
            preparedStatement.setString(3, user.getLogin());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.executeUpdate();

            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    id = generatedKeys.getLong(1);
                } else {
                    throw new DAOException("Creating user failed, no ID obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement);
        }
        return id;
    }

    /**
     * Implementation of {@link UserDAO#delete(Long)} method using simple JDBC.
     */
    @Override
    public void delete(Long id) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_DELETE_USER_BY_USER_ID);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement);
        }
    }

    /**
     * Implementation of {@link UserDAO#getByKey(Long)} method using simple JDBC.
     */
    @Override
    public User getByKey(Long id) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        User user = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_USER_BY_USER_ID);
            preparedStatement.setLong(1, id);
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                user = buildUserFromResultSet(rs);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
        return user;
    }

    /**
     * Implementation of {@link UserDAO#update(Object)} method using simple JDBC.
     */
    @Override
    public void update(User user) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_UPDATE_USER);
            preparedStatement.setLong(1, user.getRoleId());
            preparedStatement.setString(2, user.getUserName());
            preparedStatement.setString(3, user.getLogin());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setLong(5, user.getUserId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement);
        }
    }

    @Override
    public User getUserInfo(String userLogin) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        User user = null;
        try {
            connection = DataSourceUtils.doGetConnection(dataSource);
            preparedStatement = connection.prepareStatement(SQL_SELECT_USER_BY_LOGIN);
            preparedStatement.setString(1, userLogin);
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                user = buildUserFromResultSet(rs);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBUtil.closeConnection(connection, dataSource, preparedStatement, rs);
        }
        return user;
    }

    private User buildUserFromResultSet(ResultSet rs) throws SQLException {
        User user = new User();
        user.setUserId(rs.getLong(USER_ID));
        user.setRoleId(rs.getLong(USER_ROLE_ID));
        user.setUserName(rs.getString(USER_NAME));
        user.setLogin(rs.getString(USER_LOGIN));
        user.setPassword(rs.getString(USER_PASSWORD));
        return user;
    }
}
