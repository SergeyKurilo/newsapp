<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>All News</title>

  <link href="${pageContext.request.contextPath}/resources/styles/app.css" rel="stylesheet"/>
  <link href="${pageContext.request.contextPath}/resources/styles/bootstrap.css" rel="stylesheet"/>
  <script src="<c:url value="${pageContext.request.contextPath}/resources/scripts/jquery.js"/>"></script>


</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="${pageContext.request.contextPath}/ClientController?page=1">News App</a>
    </div>
  </div>
</nav>
<div class="col-sm-12">
  <form class="form" method="POST"
        action="${pageContext.request.contextPath}/ClientController?command=searchResult&page=1">
    <div class="col-md-5">
      <select class="form-control" name="authorId">
        <option disabled value>Select author</option>
        <c:forEach items="${authorsList}" var="author">
          <option value="${author.authorId}">${author.authorName}</option>
        </c:forEach>
      </select>
    </div>
    <div class="col-md-5">
      <select class="form-control" multiple="multiple" name="tagId">
        <option disabled value>Select tags</option>
        <c:forEach items="${tagsList}" var="tag">
          <option value="${tag.tagId}">${tag.tagName}</option>
        </c:forEach>
      </select>
    </div>
    <input class="btn btn-info" type="submit" value="Filter"/>
    <input class="btn btn-info" type="submit" value="Reset"
           formaction="${pageContext.request.contextPath}/ClientController?command=resetSearchCriteria"/>
  </form>
  <c:forEach items="${newsTOs}" var="newsTO">
    <div class="grid news_desc">
      <h3><a href="${pageContext.request.contextPath}/ClientController?command=singleNews&id=${newsTO.news.newsId}"
             title="More">${newsTO.news.title} </a></h3>
      <checkbox path="newsIds" value="${newsTO.news.newsId}"/>
      <h4>Posted on <fmt:formatDate value="${newsTO.news.creationDate}" pattern="MM/dd/yyyy"/> by
        <span><a>${newsTO.author.authorName}</a></span></h4>
      <p>${newsTO.news.shortText}<a
              href="${pageContext.request.contextPath}/ClientController?command=singleNews&id=${newsTO.news.newsId}"
              title="More">[....]</a>
      </p>
    </div>
  </c:forEach>
  <div class="pagination-custom">
    <ul class="pagination">
      <c:forEach begin="1" end="${numberOfPages}" step="1" var="page">
        <li><a href="${pageContext.request.contextPath}/ClientController?page=${page}">${page}</a></li>
      </c:forEach>
    </ul>
  </div>
</div>
<div class="container">
  <p class="foot">Siarhei Kuryla 2016 EPAM Systems</p>
</div>
</body>
</html>