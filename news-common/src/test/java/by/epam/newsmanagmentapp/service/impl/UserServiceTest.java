package by.epam.newsmanagmentapp.service.impl;

import by.epam.newsmanagmentapp.dao.UserDAO;
import by.epam.newsmanagmentapp.domain.bean.User;
import by.epam.newsmanagmentapp.exceptions.DAOException;
import by.epam.newsmanagmentapp.exceptions.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserDAO userDAO;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetByKey() throws ServiceException, DAOException {
        User user = new User();
        when(userDAO.getByKey(1L)).thenReturn(user);
        User testUser = userService.getByKey(1L);
        assertEquals(testUser, user);
        verify(userDAO).getByKey(1L);

    }

    @Test
    public void testFindAll() throws ServiceException, DAOException {
        when(userDAO.findAll()).thenReturn(new ArrayList<>());
        List<User> result = userService.findAll();
        assertNotNull(result);
        verify(userDAO).findAll();
    }

    @Test
    public void testCreateUser() throws ServiceException, DAOException {
        User user = new User();
        when(userDAO.insert(any(User.class))).thenReturn(1L);
        Long id = userService.insert(user);
        assertNotNull(id);
    }

    @Test(expected = ServiceException.class)
    public void testCreateUserExceptionSituation() throws ServiceException, DAOException {
        User user = new User();
        when(userDAO.insert(user)).thenThrow(DAOException.class);
        userService.insert(user);
    }

    @Test
    public void testUpdate() throws ServiceException, DAOException {
        User user = new User();
        userService.update(user);
        verify(userDAO, times(1)).update(user);
    }
}
