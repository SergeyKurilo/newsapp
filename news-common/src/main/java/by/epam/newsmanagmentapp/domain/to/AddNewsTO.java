package by.epam.newsmanagmentapp.domain.to;

import by.epam.newsmanagmentapp.domain.bean.News;

import java.util.Arrays;
import java.util.List;

public class AddNewsTO {
    private News news;
    private Long[] tags;
    private Long authorId;

    public AddNewsTO() {

    }

    public AddNewsTO(News news, Long[] tags, Long authorId) {
        this.news = news;
        this.tags = tags;
        this.authorId = authorId;
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public Long[] getTags() {
        return tags;
    }

    public void setTags(Long[] tags) {
        this.tags = tags;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AddNewsTO addNewsTO = (AddNewsTO) o;

        if (news != null ? !news.equals(addNewsTO.news) : addNewsTO.news != null) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(tags, addNewsTO.tags)) return false;
        return authorId != null ? authorId.equals(addNewsTO.authorId) : addNewsTO.authorId == null;

    }

    @Override
    public int hashCode() {
        int result = news != null ? news.hashCode() : 0;
        result = 31 * result + Arrays.hashCode(tags);
        result = 31 * result + (authorId != null ? authorId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AddNewsTO{");
        sb.append("news=").append(news);
        sb.append(", tags=").append(Arrays.toString(tags));
        sb.append(", authorId=").append(authorId);
        sb.append('}');
        return sb.toString();
    }
}
